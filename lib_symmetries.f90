MODULE symmetries
!
!  Purpose:
!    Space-group symmetry stuff, my first attempt ...
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    19/07/15     S. Barthel	Original code
!

   IMPLICIT NONE
   SAVE

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units

   TYPE :: single_space_group

      INTEGER :: n_symmetry_operators                   !number of symmetry operators in the space group
      CHARACTER(len=16) :: name                         !name of the space group (Hermann–Mauguin)
      CHARACTER(len=16) :: crystal_system               !corresponding crystal system
      CHARACTER(len=16) :: ass_point_group              !associated point group (Schoenfliess)
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: matrix     !matrix defining coordinates transformation BEFORE applying the translation
      REAL, ALLOCATABLE, DIMENSION(:,:) :: transl       !vector defining the translation vector to be applied AFTER the transformation

   END TYPE single_space_group

   TYPE (single_space_group), DIMENSION(230) :: space_groups

CONTAINS

   !=================================================================================================================================

   FUNCTION inv_T_spd(inv_T)
      !
      !  Purpose:
      !    Calculate the spd-orbital transformation matrix (VASP order) into new local coordinate system
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    24/06/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      REAL, INTENT(IN), DIMENSION(3,3) :: inv_T   !Inverse transformation matrix T^{-1} (basis vectors in rows)

      REAL, DIMENSION(9,9) :: inv_T_spd       !Inverse transformation matrix of spd-orbitals into new local coordinate system

      !-------------------------------------------------------------------------------------------------------------------------------

      !Setup complete transformation matrix spd 9x9: s py pz px dxy dyz dz2 dxz dx2
      inv_T_spd = 0.

      !-------------------------------------------------------------------------------------------------------------------------------
      !s-orbital
      !-------------------------------------------------------------------------------------------------------------------------------
      !s'
      inv_T_spd(1,1) = 1.

      !-------------------------------------------------------------------------------------------------------------------------------
      !p-orbitals (05.01.2016: Graphene, the pz-orbital appears to be not ordered correctly ?)
      !-------------------------------------------------------------------------------------------------------------------------------
!     !py'
!     inv_T_spd(2,2:4) = (/ inv_T(2,1), inv_T(2,2), inv_T(2,3) /)
!     !pz'
!     inv_T_spd(3,2:4) = (/ inv_T(3,1), inv_T(3,2), inv_T(3,3) /)
!     !px'
!     inv_T_spd(4,2:4) = (/ inv_T(1,1), inv_T(1,2), inv_T(1,3) /)

      !with this ordering we get the unit matrix (which appears to be correct)
      !but i have to think about this to exclude a systematic error ... did i consider the orbital ordering of vasp in all equations ?

!     !py'
!     inv_T_spd(2,2:4) = (/ inv_T(1,1), inv_T(1,2), inv_T(1,3) /)
!     !pz'
!     inv_T_spd(3,2:4) = (/ inv_T(2,1), inv_T(2,2), inv_T(2,3) /)
!     !px'
!     inv_T_spd(4,2:4) = (/ inv_T(3,1), inv_T(3,2), inv_T(3,3) /)


      !this is what i guess should be right ? (y,z,x) keep unit-vector definition but reorder orbitals/mind vasp orbital ordering ?

      !py'
      inv_T_spd(2,2:4) = (/ inv_T(2,2), inv_T(2,3), inv_T(2,1) /)
      !pz'
      inv_T_spd(3,2:4) = (/ inv_T(3,2), inv_T(3,3), inv_T(3,1) /)
      !px'
      inv_T_spd(4,2:4) = (/ inv_T(1,2), inv_T(1,3), inv_T(1,1) /)

      !-------------------------------------------------------------------------------------------------------------------------------
      !d-orbitals
      !-------------------------------------------------------------------------------------------------------------------------------
      !dx'y'
      inv_T_spd(5,5) = inv_T(1,1)*inv_T(2,2)+inv_T(1,2)*inv_T(2,1)
      inv_T_spd(5,6) = inv_T(1,2)*inv_T(2,3)+inv_T(1,3)*inv_T(2,2)
      inv_T_spd(5,7) = inv_T(1,3)*inv_T(2,3)*SQRT(3.)
      inv_T_spd(5,8) = inv_T(1,1)*inv_T(2,3)+inv_T(1,3)*inv_T(2,1)
      inv_T_spd(5,9) = inv_T(1,1)*inv_T(2,1)-inv_T(1,2)*inv_T(2,2)
      !dy'z'
      inv_T_spd(6,5) = inv_T(2,1)*inv_T(3,2)+inv_T(2,2)*inv_T(3,1)
      inv_T_spd(6,6) = inv_T(2,2)*inv_T(3,3)+inv_T(2,3)*inv_T(3,2)
      inv_T_spd(6,7) = inv_T(2,3)*inv_T(3,3)*SQRT(3.)
      inv_T_spd(6,8) = inv_T(2,1)*inv_T(3,3)+inv_T(2,3)*inv_T(3,1)
      inv_T_spd(6,9) = inv_T(2,1)*inv_T(3,1)-inv_T(2,2)*inv_T(3,2)

      !dz'2
      inv_T_spd(7,5) = 2*( -inv_T(1,1)*inv_T(1,2)-inv_T(2,1)*inv_T(2,2)+2*inv_T(3,1)*inv_T(3,2) ) / ( 2*SQRT(3.) )	!ok
      inv_T_spd(7,6) = 2*( -inv_T(1,2)*inv_T(1,3)-inv_T(2,2)*inv_T(2,3)+2*inv_T(3,2)*inv_T(3,3) ) / ( 2*SQRT(3.) )	!ok
      inv_T_spd(7,7) =   ( -inv_T(1,3)*inv_T(1,3)-inv_T(2,3)*inv_T(2,3)+2*inv_T(3,3)*inv_T(3,3) ) / ( 2. )		!ok
      inv_T_spd(7,8) = 2*( -inv_T(1,1)*inv_T(1,3)-inv_T(2,1)*inv_T(2,3)+2*inv_T(3,1)*inv_T(3,3) ) / ( 2*SQRT(3.) )	!ok
      inv_T_spd(7,9) = ( (-inv_T(1,1)*inv_T(1,1)-inv_T(2,1)*inv_T(2,1)+2*inv_T(3,1)*inv_T(3,1)) &
         - (-inv_T(2,2)*inv_T(2,2)-inv_T(1,2)*inv_T(1,2)+2*inv_T(3,2)*inv_T(3,2)) )   / ( 2*SQRT(3.) )

      !dx'z'
      inv_T_spd(8,5) = inv_T(1,1)*inv_T(3,2) + inv_T(1,2)*inv_T(3,1)
      inv_T_spd(8,6) = inv_T(1,2)*inv_T(3,3) + inv_T(1,3)*inv_T(3,2)
      inv_T_spd(8,7) = inv_T(1,3)*inv_T(3,3) * SQRT(3.)
      inv_T_spd(8,8) = inv_T(1,1)*inv_T(3,3) + inv_T(1,3)*inv_T(3,1)
      inv_T_spd(8,9) = inv_T(1,1)*inv_T(3,1) - inv_T(1,2)*inv_T(3,2)

      !dx'2
      inv_T_spd(9,5) = 2*( inv_T(1,1)*inv_T(1,2) - inv_T(2,1)*inv_T(2,2) ) / ( 2. )
      inv_T_spd(9,6) = 2*( inv_T(1,2)*inv_T(1,3) - inv_T(2,2)*inv_T(2,3) ) / ( 2. )
      inv_T_spd(9,7) = ( inv_T(1,3)*inv_T(1,3) - inv_T(2,3)*inv_T(2,3) ) * SQRT(3.) / ( 2. )
      inv_T_spd(9,8) = 2*( inv_T(1,1)*inv_T(1,3) - inv_T(2,1)*inv_T(2,3) ) / ( 2. )
      inv_T_spd(9,9) = ( (inv_T(1,1)*inv_T(1,1)-inv_T(2,1)*inv_T(2,1))-(inv_T(1,2)*inv_T(1,2)-inv_T(2,2)*inv_T(2,2)) ) / ( 2. )

   END FUNCTION inv_T_spd

END MODULE symmetries
