MODULE timings
!
!  Purpose:
!    Collection of routines to assist with timings
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    21/05/14     S. Barthel	Original code
!

   IMPLICIT NONE

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units
   REAL :: mtime,dmtime,timesum						!time measurement for OpenMP
   INTEGER :: counter_time

CONTAINS

   !=============================================================================================================================

   SUBROUTINE progressbar(j,t)
      !
      !  Purpose:
      !    Display a progressbar including an estimated time for a task to finish
      !    example: CALL progressbar( NINT(100*REAL(kpoint)/n_kpoints),(n_kpoints-kpoint)*dmtime)
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    03/03/14     S. Barthel	Original code
      !

      USE parallel

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      INTEGER, INTENT(IN) :: j
      INTEGER :: k,l
      REAL,INTENT(IN) :: t
      CHARACTER(LEN=57) :: bar

      bar="   ???% |                    | Time left: ?????????? sec."

      WRITE(UNIT=bar(4:6),FMT="(I3)") j
      WRITE(UNIT=bar(43:52),FMT="(F8.2)") -t

      l=NINT(REAL(j)/5)

      DO k=1,l
         bar(9+k:9+k+1)="=|"
      ENDDO

      ! print the progress bar.
      WRITE(UNIT=6,FMT="(A1,A1,A57)") '+',CHAR(13), bar
      RETURN
   END SUBROUTINE progressbar

   !=============================================================================================================================

END MODULE timings
