MODULE greens_functions
   !
   !  Purpose:
   !    Class implementing Greens-functions
   !  Record of revisions:
   !      Date       Programmer    Description of change
   !      ====       ==========    =====================
   !    27/04/15     S. Barthel    Original code
   !    31/08/15     S. Barthel    +subroutine "gf_iw_SET_SIGMA_ZERO"
   !    15/09/15     S. Barthel    +several subroutines including serial versions
   !

   USE constants
   USE math
   USE parallel

   IMPLICIT NONE

   ! Data dictionary: declare constants

   ! Data dictionary: type definitions

   !===========================================================================================================================

   TYPE :: gf_iw

      PRIVATE                         !no access granted to member variables

      !Matsubara frequency mesh
      REAL :: beta = 0.0D0                    !inverse temperature
      INTEGER :: n_iw = 0                     !number of matsubara frequencies
      COMPLEX, ALLOCATABLE, DIMENSION(:) :: mesh          !matsubara frequency mesh

      !Degrees of freedom
      INTEGER :: n_spins = 0
      INTEGER :: n_kpoints = 0
      INTEGER :: n_orbitals = 0

      !Constructors

      REAL, ALLOCATABLE, DIMENSION(:) :: k_weight         !Weight for each k-point
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:) :: H0        !Single-particle hamiltonian, orbital,orbital_prime,kpoint
      REAL :: Ne = 0.0D0                      !total number of electrons
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: Sigma       !Self-energy (k-independent), orbital,orbital_prime,spin,iw
      REAL :: mu = 0.0D0                      !chemical potential

      !COMPLEX, ALLOCATABLE, DIMENSION(:,:,:) :: EW       !Eigenenergies of H0+Sigma, if frequency independent
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: EW           !Eigenenergies of H0+Sigma, if frequency independent
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: EV      !Eigenvectors of H0+Sigma, if frequency independent
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: EW0          !Eigenenergies of H0, if frequency independent
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: EV0     !Eigenvectors of H0, if frequency independent

      !G(z)
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: data        !orbital,orbital_prime,spin,iw

      !-tail-begin-------------------------------------------------------------------------------------------------------------------
      !We adopt the strategy outlined in the Phd-Thesis of E.Gull, Appendix B, p-136-141

      LOGICAL :: use_gtail_fit = .FALSE.              !disable analytical tail calculation and fit instead

      !high frequency tail of G(z), coefficients of polynom in 1/iw: g0 + g1/iw + g2/(iw)^2 + g3/(iw)^3 + rest
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: gtail       !tail coefficients

      !order 6 used in tail fitting, provides good agreement to analytics up to order 3
      INTEGER :: gtail_order_fit = 6

      !order 2 results in real diagonal elements of reduced single-particle density matrix up to machine precision
      INTEGER :: gtail_order_sum = 2
      !order 1: fail safe, debugging
      !order 2: chemical potential agrees with TRIQS ~ meV or better, TRIQS: realpart of largest iw of sigma ...
      !order 3: will affect trivial sum, but has no contribution on analytical correction, ?

      !order 3 used in Matsubara sum, needs testing if s1/iw is reasonable, because we fit ... only reasonable if have measured tail?
      !might still be a bug in order 3 calculation .... who knows
      !INTEGER :: gtail_order_sum = 3

      !high frequency tail of self-energy, coefficients of polynom in 1/iw: s0 + s1/iw + rest
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: stail       !tail coefficients
      INTEGER :: stail_order = 1                  !order used in tail fitting

      INTEGER :: fit_start
      INTEGER :: n_fit
      !REAL :: w_max

      ! REAL :: tol = 1.0D-12                   !stopping criterion for mu-search
      REAL :: tol = 1.0D-5                   !stopping criterion for mu-search

!      HF   : mu=    5.96122260055460, SET
!      HF   : mu=    5.96122776752008, SET, SUM extraploated
!      HF   : mu=    5.96122210271133, FIT
!      HF   : mu=    5.96122695656754, FIT, SUM extrapolated
!      W2DYN: mu=5.961224
!      TRIQS: mu=5.961223

      !-tail-end----------------------------------------------------------------------------------------------------------------------

      !Reduced single-particle density-matrix
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:) :: density       !orbital,orbital_prime,spin
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:) :: density_k_eig     !orbital,orbital_prime,spin
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: density_k       !orbital,orbital_prime,spin


      !-------------------------------------------------------------------------------------------------------------------------------

      !Allow subroutine calls only if prequisites are met

      LOGICAL :: is_ALLOCATED = .FALSE.

      LOGICAL :: has_MESH = .FALSE.
      LOGICAL :: has_H0 = .FALSE.
      LOGICAL :: has_SIGMA = .FALSE.
      LOGICAL :: has_NE = .FALSE.
      LOGICAL :: has_MU = .FALSE.

      LOGICAL :: has_DATA = .FALSE.

      LOGICAL :: has_DENSITY = .FALSE.

      LOGICAL :: is_CONSISTENT = .FALSE.

      !Dev
      LOGICAL :: has_EIGEN = .FALSE.
      LOGICAL :: has_EIGEN0 = .FALSE.

      LOGICAL :: has_GTAIL = .FALSE.
      LOGICAL :: has_STAIL = .FALSE.

   END TYPE Gf_iw

   !=================================================================================================================================

CONTAINS

   SUBROUTINE gf_iw_ALLOCATE( giw, n_iw, n_spins, n_kpoints, n_orbitals )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer  Description of change
      !      ====       ==========  =====================
      !    18/05/15     S. Barthel  Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      INTEGER, INTENT(IN) :: n_iw
      INTEGER, INTENT(IN) :: n_spins
      INTEGER, INTENT(IN) :: n_kpoints
      INTEGER, INTENT(IN) :: n_orbitals
      INTEGER :: allocerror         !error codes for allocation

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( .NOT. giw%is_ALLOCATED ) THEN

         giw%n_iw = n_iw
         giw%n_spins = n_spins
         giw%n_kpoints = n_kpoints
         giw%n_orbitals = n_orbitals

         ALLOCATE ( giw%mesh(n_iw), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%mesh" could not be allocated'
            STOP
         ELSE
            giw%mesh = CMPLX(0.,0.)
         ENDIF

         ALLOCATE ( giw%k_weight(giw%n_kpoints), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "giw%k_weight" could not be allocated'
            STOP
         ELSE
            giw%k_weight = 0.
         ENDIF

         ALLOCATE ( giw%H0(giw%n_orbitals, giw%n_orbitals, giw%n_kpoints), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "giw%H0" could not be allocated'
            STOP
         ELSE
            giw%H0 = CMPLX(0.,0.)
         ENDIF

         ALLOCATE ( giw%Sigma(giw%n_orbitals, giw%n_orbitals, giw%n_spins, giw%n_iw), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "giw%S" could not be allocated'
            STOP
         ELSE
            giw%Sigma = CMPLX(0.,0.)
         ENDIF

         !DEV:
!   ALLOCATE ( giw%EV_R(giw%n_orbitals,giw%n_orbitals,giw%n_spins,giw%n_kpoints), &
!     giw%EV_R(giw%n_orbitals,giw%n_orbitals,giw%n_spins,giw%n_kpoints), STAT=allocerror )
!   IF ( allocerror > 0 ) THEN
!     WRITE(*,*) 'ERROR: Memory for "giw%EV" could not be allocated'
!     STOP
!   ELSE
!     giw%EV_R = CMPLX(0.,0.)
!     giw%EV_L = CMPLX(0.,0.)
!   ENDIF

         ALLOCATE ( giw%EW(giw%n_orbitals,giw%n_spins,giw%n_kpoints), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%EW" could not be allocated'
            STOP
         ELSE
            giw%EW = 0.
         ENDIF

         ALLOCATE ( giw%EV(giw%n_orbitals,giw%n_orbitals,giw%n_spins,giw%n_kpoints), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%EV" could not be allocated'
            STOP
         ELSE
            giw%EV = 0.
         ENDIF

         ALLOCATE ( giw%EW0(giw%n_orbitals,giw%n_spins,giw%n_kpoints), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%EW" could not be allocated'
            STOP
         ELSE
            giw%EW0 = 0.
         ENDIF

         ALLOCATE ( giw%EV0(giw%n_orbitals,giw%n_orbitals,giw%n_spins,giw%n_kpoints), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%EV" could not be allocated'
            STOP
         ELSE
            giw%EV0 = 0.
         ENDIF
         !--

         ALLOCATE ( giw%data(giw%n_orbitals, giw%n_orbitals, giw%n_spins, giw%n_iw), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%data" could not be allocated'
            STOP
         ELSE
            giw%data = CMPLX(0.,0.)
         ENDIF

         !ALLOCATE ( giw%tail(giw%n_orbitals, giw%n_orbitals, giw%n_spins, giw%tail_order), STAT=allocerror )

         ALLOCATE ( giw%gtail(giw%n_orbitals, giw%n_orbitals, giw%n_spins, 0:giw%gtail_order_fit), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%gtail" could not be allocated'
            STOP
         ELSE
            giw%gtail = CMPLX(0.,0.)
         ENDIF

         ALLOCATE ( giw%stail(giw%n_orbitals, giw%n_orbitals, giw%n_spins, 0:giw%stail_order), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%stail" could not be allocated'
            STOP
         ELSE
            giw%stail = CMPLX(0.,0.)
         ENDIF

         ALLOCATE ( giw%density(giw%n_orbitals, giw%n_orbitals, giw%n_spins), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%density" could not be allocated'
            STOP
         ELSE
            giw%density = CMPLX(0.,0.)
         ENDIF

         ALLOCATE ( giw%density_k(giw%n_orbitals, giw%n_orbitals, giw%n_spins,giw%n_kpoints), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%density" could not be allocated'
            STOP
         ELSE
            giw%density_k = CMPLX(0.,0.)
         ENDIF

         ALLOCATE ( giw%density_k_eig(giw%n_orbitals, giw%n_spins,giw%n_kpoints), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%density" could not be allocated'
            STOP
         ELSE
            giw%density_k_eig = CMPLX(0.,0.)
         ENDIF

         giw%is_ALLOCATED = .TRUE.

         IF ( rank_0 ) THEN
            WRITE(6,*) 'INFO: gtail_order_sum: ',  giw%gtail_order_sum
            WRITE(6,*) 'INFO: stail_order: ',  giw%stail_order
         ENDIF

      ELSE

         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" already called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP

      ENDIF

   END SUBROUTINE gf_iw_ALLOCATE

   !===============================================================================================================================

   SUBROUTINE gf_iw_DEALLOCATE( giw )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer  Description of change
      !      ====       ==========  =====================
      !    18/05/15     S. Barthel  Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      INTEGER :: allocerror         !error codes for allocation

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( giw%is_ALLOCATED ) THEN

         giw%n_iw = 0
         giw%n_spins = 0
         giw%n_kpoints = 0
         giw%n_orbitals = 0

         DEALLOCATE ( giw%mesh, STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%mesh" could not be deallocated'
            STOP
         ENDIF

         DEALLOCATE ( giw%k_weight, STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "giw%k_weight" could not be deallocated'
            STOP
         ENDIF

         DEALLOCATE ( giw%H0, STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "giw%H0" could not be deallocated'
            STOP
         ENDIF

         DEALLOCATE ( giw%Sigma, STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "giw%Sigma" could not be deallocated'
            STOP
         ENDIF

         DEALLOCATE ( giw%EW, STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%EW" could not be deallocated'
            STOP
         ENDIF

         DEALLOCATE ( giw%data, STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%data" could not be deallocated'
            STOP
         ENDIF

         DEALLOCATE ( giw%gtail, STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%gtail" could not be deallocated'
            STOP
         ENDIF

         DEALLOCATE ( giw%stail, STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%stail" could not be deallocated'
            STOP
         ENDIF

         DEALLOCATE ( giw%density, STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(*,*) 'ERROR: Memory for "giw%density" could not be deallocated'
            STOP
         ENDIF

         giw%is_ALLOCATED = .FALSE.

      ELSE

         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_DEALLOCATE()" already/cannot be called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP

      ENDIF

   END SUBROUTINE gf_iw_DEALLOCATE

   !===============================================================================================================================

   SUBROUTINE gf_iw_SET_MESH( giw, beta, fit_start )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      REAL, INTENT(IN) :: beta
      INTEGER, INTENT(IN) :: fit_start
      INTEGER :: iw

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( giw%is_ALLOCATED ) THEN

         !init
         giw%has_MESH = .TRUE.
         giw%beta = beta
         DO iw=1,giw%n_iw
            giw%mesh(iw) = CMPLX(0.,PI*(2*(iw-giw%n_iw/2-1)+1)/giw%beta)
         ENDDO

         !giw%w_max = w_max

!         giw%fit_start = INT( (giw%beta*giw%w_max/(2*PI)-0.5) )    !index of positive branch only ...
!         giw%n_fit = (giw%n_iw/2 - giw%fit_start)
!         giw%fit_start = giw%fit_start + giw%n_iw/2

         !giw%fit_start = INT( (giw%w_max*giw%beta)/(2*PI)+0.5 ) + giw%n_iw/2    !on positive branch
         giw%fit_start = fit_start   !on positive branch
         giw%n_fit = giw%n_iw - giw%fit_start + 1

         !apparently this fitting window does not work for self-energies, lets just hard-code
!         giw%n_fit = 100
!         giw%fit_start = giw%n_iw-giw%n_fit

         !debug: show matsubara indices used for fitting
         IF ( rank_0 ) THEN
            WRITE(6,*) 'iw-fit: (index of first mats freq for fit, number of freqs for fit) ', giw%fit_start, giw%n_fit
            !WRITE(6,*) 'giw-fit: ', giw%mesh(giw%fit_start), giw%mesh(giw%fit_start+giw%n_fit-1), giw%mesh(giw%n_iw)
         ENDIF
         IF ( giw%n_fit < 1) THEN
            IF ( rank_0 ) THEN
               WRITE(6,*) 'Number of matsubara frequencies used for fit is negative. Increase number of Matsubaras... Exiting!'
            END IF
            CALL EXIT()
         ENDIF
         !reset
         giw%has_DATA = .FALSE.
         giw%data = CMPLX(0.,0.)

         giw%has_DENSITY = .FALSE.
         giw%density = CMPLX(0.,0.)

         giw%is_CONSISTENT = .FALSE.

         !stail, the fit might be different ? but a mesh-reinit is not possible at the moment, thus not relevant atm.
         giw%has_STAIL = .FALSE.
         giw%stail = CMPLX(0.,0.)

         !gtail, but a mesh-reinit is not possible at the moment, thus not relevant atm.
         giw%has_GTAIL = .FALSE.
         giw%gtail = CMPLX(0.,0.)

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_SET_MESH


   SUBROUTINE gf_iw_SET_MESH_T0( giw, beta, fit_start )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      REAL, INTENT(IN) :: beta
      INTEGER, INTENT(IN) :: fit_start
      INTEGER :: iw

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( giw%is_ALLOCATED ) THEN

         !init
         giw%has_MESH = .TRUE.
         giw%beta = beta
         DO iw=1,giw%n_iw
            giw%mesh(iw) = CMPLX(0.,PI*(2*(iw-giw%n_iw/2-1)+1)/giw%beta)
         ENDDO
         !reset
         giw%has_DATA = .FALSE.
         giw%data = CMPLX(0.,0.)

         giw%has_DENSITY = .FALSE.
         giw%density = CMPLX(0.,0.)

         giw%is_CONSISTENT = .FALSE.

         !stail, the fit might be different ? but a mesh-reinit is not possible at the moment, thus not relevant atm.
         giw%has_STAIL = .FALSE.
         giw%stail = CMPLX(0.,0.)

         !gtail, but a mesh-reinit is not possible at the moment, thus not relevant atm.
         giw%has_GTAIL = .FALSE.
         giw%gtail = CMPLX(0.,0.)

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_SET_MESH_T0

   !===============================================================================================================================

   SUBROUTINE gf_iw_SET_H0( giw, H0, k_weight )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      COMPLEX, INTENT(IN), DIMENSION(giw%n_orbitals,giw%n_orbitals,giw%n_kpoints) :: H0
      REAL, INTENT(IN), DIMENSION(giw%n_kpoints) :: k_weight

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( giw%is_ALLOCATED ) THEN

         !init
         giw%has_H0 = .TRUE.
         giw%H0 = H0
         giw%k_weight = k_weight

!         IF ( rank_0 ) THEN
!     WRITE(6,*) SIZE(H0,1), SIZE(H0,2), SIZE(H0,3)
!     WRITE(6,*) SIZE(giw%H0,1), SIZE(giw%H0,2),SIZE(giw%H0,3)
!         ENDIF

         !reset
         giw%has_DATA = .FALSE.
         giw%data = CMPLX(0.,0.)

         giw%has_DENSITY = .FALSE.
         giw%density = CMPLX(0.,0.)

         giw%is_CONSISTENT = .FALSE.

         giw%has_EIGEN = .FALSE.

         !gtail
         giw%has_GTAIL = .FALSE.
         giw%gtail = CMPLX(0.,0.)

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_SET_H0

   !===============================================================================================================================

   SUBROUTINE gf_iw_SET_SIGMA( giw, Sigma )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      COMPLEX, INTENT(IN), DIMENSION(giw%n_orbitals, giw%n_orbitals, giw%n_spins, giw%n_iw) :: Sigma

      !------------------------------------------------------------------------------------giw%density-----------------------------------------

      IF ( giw%is_ALLOCATED ) THEN
         IF ( giw%has_MESH ) THEN

            !init
            giw%has_SIGMA = .TRUE.
            giw%Sigma = Sigma

            !stail, automatic tail fit
            giw%has_STAIL = .FALSE.
            giw%stail = CMPLX(0.,0.)
            CALL gf_iw_FIT_STAIL( giw )

            !reset
            giw%has_DATA = .FALSE.
            giw%data = CMPLX(0.,0.)

            giw%has_DENSITY = .FALSE.
            giw%density = CMPLX(0.,0.)

            giw%is_CONSISTENT = .FALSE.

            giw%has_EIGEN = .FALSE.

            !gtail
            giw%has_GTAIL = .FALSE.
            giw%gtail = CMPLX(0.,0.)

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'STOP: "gf_iw_SET_MESH()" not called.'
            ENDIF
            CALL MPI_FINALIZE(i_error)
            STOP
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_SET_SIGMA


   !===============================================================================================================================

   SUBROUTINE gf_iw_SET_SIGMA_T0( giw, Sigma )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      COMPLEX, INTENT(IN), DIMENSION(giw%n_orbitals, giw%n_orbitals, giw%n_spins, giw%n_iw) :: Sigma

      !------------------------------------------------------------------------------------giw%density-----------------------------------------

      IF ( giw%is_ALLOCATED ) THEN
         IF ( giw%has_MESH ) THEN

            !init
            giw%has_SIGMA = .TRUE.
            giw%Sigma = Sigma

            !stail, automatic tail fit
            giw%has_STAIL = .FALSE.
            giw%stail = CMPLX(0.,0.)
            !CALL gf_iw_FIT_STAIL( giw )

            !reset
            giw%has_DATA = .FALSE.
            giw%data = CMPLX(0.,0.)

            giw%has_DENSITY = .FALSE.
            giw%density = CMPLX(0.,0.)

            giw%is_CONSISTENT = .FALSE.

            giw%has_EIGEN = .FALSE.

            !gtail
            giw%has_GTAIL = .FALSE.
            giw%gtail = CMPLX(0.,0.)

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'STOP: "gf_iw_SET_MESH()" not called.'
            ENDIF
            CALL MPI_FINALIZE(i_error)
            STOP
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_SET_SIGMA_T0

   SUBROUTINE gf_iw_FIT_STAIL( giw )
      !
      !  Purpose:
      !    Fit the asymptotic tail of the self-energy
      !
      !  Record of revisions:
      !      Date       Programmer  Description of change
      !      ====       ==========  =====================
      !    01/09/15     S. Barthel  Original code
      !
      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      COMPLEX, DIMENSION(giw%n_fit) :: inverse_iw               !Inverse positive Matsubara frequencies
      COMPLEX, DIMENSION(giw%n_fit) :: tail_data                !Complex data to fit

      INTEGER :: spin, orbital, orbital_, iw, i                 !loop indices
      INTEGER :: dim_work_opt                           !Optimal work array length

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( giw%has_MESH .AND. giw%has_SIGMA ) THEN
         IF ( .NOT. giw%has_STAIL ) THEN

            giw%stail = CMPLX(0.,0.)

            DO iw=1,giw%n_fit
               inverse_iw(iw) = 1./giw%mesh(giw%fit_start+iw-1)
            ENDDO

            DO spin=1,giw%n_spins
               DO orbital_=1,giw%n_orbitals
                  DO orbital=1,giw%n_orbitals

                     !prepare data used for fitting
                     DO iw=1,giw%n_fit
                        tail_data(iw) = giw%Sigma(orbital, orbital_, spin, giw%fit_start+iw-1)
                     ENDDO

                     dim_work_opt = -1   !optimal work array size

                     !fit of order (dim_p-1)+1, ugly
                     CALL complex_least_square_fit( giw%n_fit, tail_data, inverse_iw, giw%stail_order+1, dim_work_opt )

                     CALL complex_least_square_fit( giw%n_fit, tail_data, inverse_iw, giw%stail_order+1, dim_work_opt )

                     !indices are shifted in fit routine, ugly
                     giw%stail(orbital, orbital_, spin,:) = tail_data(1:giw%stail_order+1)

                  ENDDO
               ENDDO
            ENDDO

            giw%has_STAIL = .TRUE.

            !IF ( rank_0 ) THEN
            !  WRITE(6,*) 'DEBUG: "gf_iw_FIT_STAIL" done. Maybe set element to zero if small enough?'
            !  WRITE(6,*) 'p=0: ', giw%stail(1,1,1,0)
            !  WRITE(6,*) 'p=1: ', giw%stail(1,1,1,1)
            !ENDIF

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_FIT_STAIL" already called.'
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_SET_MESH" and/or "gf_iw_SET_SIGMA" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_FIT_STAIL

   !===============================================================================================================================

   SUBROUTINE gf_iw_SET_SIGMA_ZERO( giw )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      !------------------------------------------------------------------------------------giw%density-----------------------------------------

      IF ( giw%is_ALLOCATED ) THEN

         !init
         giw%has_SIGMA = .TRUE.
         giw%Sigma = CMPLX(0.,0.)

         !stail, automatic tail fit
         giw%has_STAIL = .TRUE.
         giw%stail = CMPLX(0.,0.)
         !CALL gf_iw_FIT_STAIL(giw)  !no need to fit zeros ...

         !reset
         giw%has_DATA = .FALSE.
         giw%data = CMPLX(0.,0.)

         giw%has_DENSITY = .FALSE.
         giw%density = CMPLX(0.,0.)

         giw%is_CONSISTENT = .FALSE.

         giw%has_EIGEN = .FALSE.

         !gtail
         giw%has_GTAIL = .FALSE.
         giw%gtail = CMPLX(0.,0.)

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_SET_SIGMA_ZERO

   !===============================================================================================================================

   SUBROUTINE gf_iw_SET_NE( giw, Ne )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      REAL, INTENT(IN) :: Ne

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( giw%is_ALLOCATED ) THEN

         !init
         giw%has_NE = .TRUE.
         giw%Ne = Ne

         !reset
         giw%has_DATA = .FALSE.
         giw%data = CMPLX(0.,0.)

         giw%has_DENSITY = .FALSE.
         giw%density = CMPLX(0.,0.)

         giw%is_CONSISTENT = .FALSE.

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_SET_NE

   !===============================================================================================================================

   SUBROUTINE gf_iw_SET_MU( giw, mu )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      REAL, INTENT(IN) :: mu

      !-----------------------------------------------------------------------------------------------------------------------------
      IF ( giw%is_ALLOCATED ) THEN

         !init
         giw%has_mu = .TRUE.
         giw%mu = mu

         !reset
         giw%has_DATA = .FALSE.
         giw%data = CMPLX(0.,0.)

         giw%has_DENSITY = .FALSE.
         giw%density = CMPLX(0.,0.)

         giw%is_CONSISTENT = .FALSE.

         !gtail
         giw%has_GTAIL = .FALSE.
         giw%gtail = CMPLX(0.,0.)

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_SET_MU

   !===============================================================================================================================

   SUBROUTINE gf_iw_CALC_G( giw )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      INTEGER :: spin, kpoint, iw, orbital
      COMPLEX, DIMENSION(giw%n_orbitals,giw%n_orbitals) :: g_tmp

      !=============================================================================================================================

      IF ( giw%is_ALLOCATED ) THEN
         IF ( .NOT. giw%has_DATA ) THEN
            !IF ( giw%has_MESH .AND. giw%has_H0 .AND. giw%has_SIGMA .AND. giw%has_NE .AND. giw%has_MU ) THEN
            IF ( giw%has_MESH .AND. giw%has_H0 .AND. giw%has_SIGMA .AND. giw%has_MU ) THEN

               !Update with chemical potential ...
               giw%data = CMPLX(0.,0.)

               !MPI: only k-loop
               CALL parallel_range(1, giw%n_kpoints, nprocs, myrank, ista, iend)
               DO ilin=ista,iend
                  DO iw=1,giw%n_iw
                  ! WRITE(6,*) 'debug 15: iwn', iw
                     DO spin=1,giw%n_spins

!                 !MPI: superindex loop over kpoints and matsubara frequencies
!                 CALL parallel_range(1, giw%n_spins*giw%n_iw*giw%n_kpoints, nprocs, myrank, ista, iend)
!                 DO ilin=ista,iend
!
!         CALL parallel_lin2sub_triple(ilin, giw%n_spins, giw%n_iw, giw%n_kpoints, spin, iw, kpoint)
!         g_tmp = - giw%H0(:,:,kpoint) - giw%Sigma(:,:,spin,iw)

                        g_tmp = - giw%H0(:,:,ilin) - giw%Sigma(:,:,spin,iw)

                        DO orbital=1,giw%n_orbitals
                           g_tmp(orbital,orbital) = g_tmp(orbital,orbital) + giw%mesh(iw) + giw%mu
                        ENDDO
                        CALL complex_matrix_inverse(giw%n_orbitals,g_tmp)
                        giw%data(:,:,spin,iw) = giw%data(:,:,spin,iw) + g_tmp*giw%k_weight(ilin)  !summation over kpoints

!       ENDDO

                     ENDDO
                  ENDDO

               ENDDO

               !MPI
               srcnt = (giw%n_spins)*(giw%n_iw)*(giw%n_orbitals**2)
               CALL MPI_ALLREDUCE(MPI_IN_PLACE, giw%data, srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)

               !giw%data = giw%data / giw%n_kpoints                        !normalization wrt. kpoints

               giw%data = giw%data / SUM( giw%k_weight )

               giw%has_DATA = .TRUE.

               !gtail, automatic tail calculation
               IF ( giw%use_gtail_fit ) THEN
                  CALL gf_iw_FIT_GTAIL( giw )
               ELSE
                  CALL gf_iw_SET_GTAIL( giw )
               ENDIF

            ELSE
               IF ( rank_0 ) THEN
                  WRITE(6,*) 'STOP: "gf_iw_CALC_G()" cannot be called.'
               ENDIF
               CALL MPI_FINALIZE(i_error)
               STOP
            ENDIF
         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_CALC_G()" already called.'
            ENDIF
         ENDIF
      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_CALC_G

   !===============================================================================================================================

   SUBROUTINE gf_iw_CALC_G_serial( giw )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      INTEGER :: spin, kpoint, iw, orbital
      COMPLEX, DIMENSION(giw%n_orbitals,giw%n_orbitals) :: g_tmp

      !=============================================================================================================================

      IF ( giw%is_ALLOCATED ) THEN
         IF ( .NOT. giw%has_DATA ) THEN
            !IF ( giw%has_MESH .AND. giw%has_H0 .AND. giw%has_SIGMA .AND. giw%has_NE .AND. giw%has_MU ) THEN
            IF ( giw%has_MESH .AND. giw%has_H0 .AND. giw%has_SIGMA .AND. giw%has_MU ) THEN

               !Update with chemical potential ...
               giw%data = CMPLX(0.,0.)

!             !MPI: superindex loop over kpoints and matsubara frequencies
!             CALL parallel_range(1, giw%n_spins*giw%n_iw*giw%n_kpoints, nprocs, myrank, ista, iend)
!             DO ilin=ista,iend
!
!               CALL parallel_lin2sub_triple(ilin, giw%n_spins, giw%n_iw, giw%n_kpoints, spin, iw, kpoint)
!
!         g_tmp = - giw%H0(:,:,kpoint) - giw%Sigma(:,:,spin,iw)
!
!         DO orbital=1,giw%n_orbitals
!       g_tmp(orbital,orbital) = g_tmp(orbital,orbital) + giw%mesh(iw) + giw%mu
!         ENDDO
!         CALL complex_matrix_inverse(giw%n_orbitals,g_tmp)
!         giw%data(:,:,spin,iw) = giw%data(:,:,spin,iw) + g_tmp         !summation over kpoints
!
!             ENDDO

               DO spin=1,giw%n_spins
                  DO iw=1,giw%n_iw
                     DO kpoint=1,giw%n_kpoints

                        g_tmp = - giw%H0(:,:,kpoint) - giw%Sigma(:,:,spin,iw)
                        DO orbital=1,giw%n_orbitals
                           g_tmp(orbital,orbital) = g_tmp(orbital,orbital) + giw%mesh(iw) + giw%mu
                        ENDDO
                        CALL complex_matrix_inverse(giw%n_orbitals,g_tmp)
                        giw%data(:,:,spin,iw) = giw%data(:,:,spin,iw) + g_tmp*giw%k_weight(kpoint)        !summation over kpoints

                     ENDDO
                  ENDDO
               ENDDO

!             !MPI
!             srcnt = (giw%n_spins)*(giw%n_iw)*(giw%n_orbitals**2)
!             CALL MPI_ALLREDUCE(MPI_IN_PLACE, giw%data, srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)

               !giw%data = giw%data / giw%n_kpoints                        !normalization wrt. kpoints

               giw%data = giw%data / SUM( giw%k_weight )

               giw%has_DATA = .TRUE.

               !gtail, automatic tail calculation
               IF ( giw%use_gtail_fit ) THEN
                  CALL gf_iw_FIT_GTAIL( giw )
               ELSE
                  CALL gf_iw_SET_GTAIL_serial( giw )
               ENDIF

            ELSE
               IF ( rank_0 ) THEN
                  WRITE(6,*) 'STOP: "gf_iw_CALC_G_serial()" cannot be called.'
               ENDIF
               CALL MPI_FINALIZE(i_error)
               STOP
            ENDIF
         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_CALC_G_serial()" already called.'
            ENDIF
         ENDIF
      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_CALC_G_serial

   !===============================================================================================================================

   SUBROUTINE gf_iw_SET_GTAIL( giw )
      !
      !  Purpose:
      !    Set the asymptotic tail of G(z) analytically
      !    We adopt the strategy outlined in the Phd-Thesis of E.Gull, Appendix B, p-136-141
      !
      !  Record of revisions:
      !      Date       Programmer  Description of change
      !      ====       ==========  =====================
      !    01/06/15     S. Barthel  Original code
      !
      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      INTEGER :: p

      INTEGER :: kpoint, orbital, orbital_, spin        !loop indices

      COMPLEX, DIMENSION(giw%n_orbitals, giw%n_orbitals, giw%n_kpoints, giw%n_spins,2) :: moment_k

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( giw%has_mu .AND. giw%has_H0 .AND. giw%has_STAIL ) THEN
         IF ( .NOT. giw%has_GTAIL ) THEN

            !reset
            giw%gtail = CMPLX(0.,0.)
            moment_k = CMPLX(0.,0.)

            !p=0
            giw%gtail(:,:,:,0) = CMPLX(0.,0.)                       !no static part

            !p=1
            IF ( giw%gtail_order_sum >= 1 ) THEN
               DO spin=1,giw%n_spins
                  DO orbital=1,giw%n_orbitals
                     giw%gtail(orbital,orbital,spin,1) = CMPLX(1.,0.)          !{c+,c}=1 if orbital=orbital_
                  ENDDO
               ENDDO
            ENDIF

            !p=2, first non trivial moment
            IF ( giw%gtail_order_sum >= 2 ) THEN

               !create k-dependant polynom, maybe we MPI this with all indices ...
               DO spin=1,giw%n_spins
                  DO kpoint=1,giw%n_kpoints
                     !moment_k(:,:,kpoint,spin,1) = giw%H0(:,:,kpoint) + giw%stail(:,:,spin,0)
                     !moment_k(:,:,kpoint,spin,1) = giw%H0(:,:,kpoint) + REAL( giw%stail(:,:,spin,0) ) !test real part only
                     !moment_k(:,:,kpoint,spin,1) = giw%H0(:,:,kpoint) + giw%Sigma(:,:,spin,giw%n_iw)  !largest frequency

                     !maybe real part of largest frequency in case of diagonal sigma due to symmetry ?
                     moment_k(:,:,kpoint,spin,1) = giw%H0(:,:,kpoint) + REAL( giw%Sigma(:,:,spin,giw%n_iw) )   !largest frequency

                  ENDDO
                  DO orbital=1,giw%n_orbitals
                     moment_k(orbital,orbital,:,spin,1) = moment_k(orbital,orbital,:,spin,1) - giw%mu
                  ENDDO
                  !giw%gtail(:,:,spin,2) = SUM( moment_k(:,:,:,spin,1),3 ) / giw%n_kpoints

                  !add k-weight
                  DO orbital_=1,giw%n_orbitals
                     DO orbital=1,giw%n_orbitals
                        giw%gtail(orbital,orbital_,spin,2) = SUM( moment_k(orbital,orbital_,:,spin,1)*giw%k_weight(:) )
                     ENDDO
                  ENDDO

                  giw%gtail(:,:,spin,2) = giw%gtail(:,:,spin,2) / SUM( giw%k_weight )

               ENDDO

            ENDIF

            !p>=3 contributions
            IF ( giw%gtail_order_sum >= 3 ) THEN

               CALL parallel_range(1, giw%n_orbitals*giw%n_orbitals*giw%n_kpoints, nprocs, myrank, ista, iend)

               !we have to update tmp_tail for each k-point !
               DO spin=1,giw%n_spins

                  !MPI matrix-matrix, first attempt to MPI this, maybe there is a better way
                  DO ilin=ista,iend
                     CALL parallel_lin2sub_triple(ilin, giw%n_orbitals, giw%n_orbitals, giw%n_kpoints, orbital, orbital_, kpoint)
                     moment_k(orbital,orbital_,kpoint,spin,2) = &
                        SUM( moment_k(orbital,:,kpoint,spin,1) * moment_k(:,orbital_,kpoint,spin,1) )
                  ENDDO
                  srcnt = (giw%n_orbitals)*(giw%n_orbitals)*(giw%n_kpoints)

                  CALL MPI_ALLREDUCE(MPI_IN_PLACE, moment_k(:,:,:,spin,2), srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, &
                    MPI_COMM_WORLD, i_error)

                  !giw%gtail(:,:,spin,3) = ( SUM( moment_k(:,:,:,spin,2),3 ) / giw%n_kpoints ) + giw%stail(:,:,spin,1)

                  !add k-weight
                  DO orbital_=1,giw%n_orbitals
                     DO orbital=1,giw%n_orbitals
                        giw%gtail(orbital,orbital_,spin,3) = &
                           ( SUM( moment_k(orbital,orbital_,:,spin,2)*giw%k_weight(:) )  / SUM( giw%k_weight ) ) &
                           + giw%stail(orbital,orbital_,spin,1)
                     ENDDO
                  ENDDO

               ENDDO
            ENDIF

            giw%has_GTAIL = .TRUE.

            !       IF ( rank_0 ) THEN
            !         WRITE(6,*) 'DEBUG: "gf_iw_SET_GTAIL" done. Maybe set element to zero if small enough?'
            !         WRITE(6,*) giw%gtail(1, 1, 1,:)
            !       ENDIF
            !
            !       CALL MPI_FINALIZE(i_error)
            !       STOP

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_SET_GTAIL()" already called.'
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_SET_GTAIL()" cannot be called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_SET_GTAIL

   !===============================================================================================================================

   SUBROUTINE gf_iw_SET_GTAIL_serial( giw )
      !
      !  Purpose:
      !    Set the asymptotic tail of G(z) analytically
      !    We adopt the strategy outlined in the Phd-Thesis of E.Gull, Appendix B, p-136-141
      !
      !  Record of revisions:
      !      Date       Programmer  Description of change
      !      ====       ==========  =====================
      !    01/06/15     S. Barthel  Original code
      !
      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      INTEGER :: p

      INTEGER :: kpoint, orbital, orbital_, spin        !loop indices

      COMPLEX, DIMENSION(giw%n_orbitals, giw%n_orbitals, giw%n_kpoints, giw%n_spins,2) :: moment_k

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( giw%has_mu .AND. giw%has_H0 .AND. giw%has_STAIL ) THEN
         IF ( .NOT. giw%has_GTAIL ) THEN

            !reset
            giw%gtail = CMPLX(0.,0.)
            moment_k = CMPLX(0.,0.)

            !p=0
            giw%gtail(:,:,:,0) = CMPLX(0.,0.)                       !no static part

            !p=1
            IF ( giw%gtail_order_sum >= 1 ) THEN
               DO spin=1,giw%n_spins
                  DO orbital=1,giw%n_orbitals
                     giw%gtail(orbital,orbital,spin,1) = CMPLX(1.,0.)          !{c+,c}=1 if orbital=orbital_
                  ENDDO
               ENDDO
            ENDIF

            !p=2, first non trivial moment
            IF ( giw%gtail_order_sum >= 2 ) THEN

               !create k-dependant polynom
               DO spin=1,giw%n_spins
                  DO kpoint=1,giw%n_kpoints
                     !moment_k(:,:,kpoint,spin,1) = giw%H0(:,:,kpoint) + giw%stail(:,:,spin,0)     !improves mu, worsens trace check ...
                     !moment_k(:,:,kpoint,spin,1) = giw%H0(:,:,kpoint) + REAL( giw%stail(:,:,spin,0) ) !test real part only, hf is real ?
                     !moment_k(:,:,kpoint,spin,1) = giw%H0(:,:,kpoint) + giw%Sigma(:,:,spin,giw%n_iw)  !largest frequency

                     !maybe real part of largest frequency in case of diagonal sigma due to symmetry ?
                     moment_k(:,:,kpoint,spin,1) = giw%H0(:,:,kpoint) + REAL( giw%Sigma(:,:,spin,giw%n_iw) )   !largest frequency

                  ENDDO
                  DO orbital=1,giw%n_orbitals
                     moment_k(orbital,orbital,:,spin,1) = moment_k(orbital,orbital,:,spin,1) - giw%mu
                  ENDDO
                  !giw%gtail(:,:,spin,2) = SUM( moment_k(:,:,:,spin,1),3 ) / giw%n_kpoints

                  !add k-weight
                  DO orbital_=1,giw%n_orbitals
                     DO orbital=1,giw%n_orbitals
                        giw%gtail(orbital,orbital_,spin,2) = SUM( moment_k(orbital,orbital_,:,spin,1)*giw%k_weight(:) )
                     ENDDO
                  ENDDO

                  giw%gtail(:,:,spin,2) = giw%gtail(:,:,spin,2) / SUM( giw%k_weight )

               ENDDO

            ENDIF

            !p>=3 contributions
            IF ( giw%gtail_order_sum >= 3 ) THEN

               !WRITE(6,*) 'ERROR: k-weight for tail order 3 not implemented !'
               !STOP

               !CALL parallel_range(1, giw%n_orbitals*giw%n_orbitals*giw%n_kpoints, nprocs, myrank, ista, iend)

               !we have to update tmp_tail for each k-point !
               DO spin=1,giw%n_spins

!       !MPI matrix-matrix, first attempt to MPI this, maybe there is a better way
!       DO ilin=ista,iend
!         CALL parallel_lin2sub_triple(ilin, giw%n_orbitals, giw%n_orbitals, giw%n_kpoints, orbital, orbital_, kpoint)
!         moment_k(orbital,orbital_,kpoint,spin,2) = &
!           SUM( moment_k(orbital,:,kpoint,spin,1) * moment_k(:,orbital_,kpoint,spin,1) )
!       ENDDO
!       srcnt = (giw%n_orbitals)*(giw%n_orbitals)*(giw%n_kpoints)
!       CALL MPI_ALLREDUCE(MPI_IN_PLACE, moment_k(:,:,:,spin,2), srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)

                  DO kpoint=1,giw%n_kpoints
                     DO orbital_=1,giw%n_orbitals
                        DO orbital=1,giw%n_orbitals

                           moment_k(orbital,orbital_,kpoint,spin,2) = &
                              SUM( moment_k(orbital,:,kpoint,spin,1) * moment_k(:,orbital_,kpoint,spin,1) )

                        ENDDO
                     ENDDO
                  ENDDO

                  !giw%gtail(:,:,spin,3) = ( SUM( moment_k(:,:,:,spin,2),3 ) / giw%n_kpoints ) + giw%stail(:,:,spin,1)

                  !add k-weight
                  DO orbital_=1,giw%n_orbitals
                     DO orbital=1,giw%n_orbitals
                        giw%gtail(orbital,orbital_,spin,3) = &
                           ( SUM( moment_k(orbital,orbital_,:,spin,2)*giw%k_weight(:) ) / SUM( giw%k_weight ) ) &
                           + giw%stail(orbital,orbital_,spin,1)
                     ENDDO
                  ENDDO

               ENDDO
            ENDIF

            giw%has_GTAIL = .TRUE.

            !       IF ( rank_0 ) THEN
            !         WRITE(6,*) 'DEBUG: "gf_iw_SET_GTAIL" done. Maybe set element to zero if small enough?'
            !         WRITE(6,*) giw%gtail(1, 1, 1,:)
            !       ENDIF
            !
            !       CALL MPI_FINALIZE(i_error)
            !       STOP

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_SET_GTAIL_serial()" already called.'
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_SET_GTAIL_serial()" cannot be called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_SET_GTAIL_serial

   !===============================================================================================================================

   SUBROUTINE gf_iw_FIT_GTAIL( giw )
      !
      !  Purpose:
      !    Fit the asymptotic tail of the self-energy
      !
      !  Record of revisions:
      !      Date       Programmer  Description of change
      !      ====       ==========  =====================
      !    01/09/15     S. Barthel  Original code
      !
      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      COMPLEX, DIMENSION(giw%n_fit) :: inverse_iw               !Inverse positive Matsubara frequencies
      COMPLEX, DIMENSION(giw%n_fit) :: tail_data                !Complex data to fit

      INTEGER :: spin, orbital, orbital_, iw, i                 !loop indices
      INTEGER :: dim_work_opt                           !Optimal work array length

      !-------------------------------------------------------------------------------------------------------------------------------

      IF ( giw%has_MESH .AND. giw%has_DATA ) THEN
         IF ( .NOT. giw%has_GTAIL ) THEN

            giw%gtail = CMPLX(0.,0.)

            DO iw=1,giw%n_fit
               inverse_iw(iw) = 1./giw%mesh(giw%fit_start+iw-1)
            ENDDO

            DO spin=1,giw%n_spins
               DO orbital_=1,giw%n_orbitals
                  DO orbital=1,giw%n_orbitals

                     !prepare data used for fitting
                     DO iw=1,giw%n_fit
                        tail_data(iw) = giw%data(orbital, orbital_, spin, giw%fit_start+iw-1)
                     ENDDO

                     dim_work_opt = -1   !optimal work array size

                     !fit of order (dim_p-1)+1, ugly
                     CALL complex_least_square_fit( giw%n_fit, tail_data, inverse_iw, giw%gtail_order_fit+1, dim_work_opt )
                     CALL complex_least_square_fit( giw%n_fit, tail_data, inverse_iw, giw%gtail_order_fit+1, dim_work_opt )

                     !indices are shifted in fit routine, ugly
                     giw%gtail(orbital, orbital_, spin,:) = tail_data(1:giw%gtail_order_fit+1)

                  ENDDO
               ENDDO
            ENDDO

            giw%has_GTAIL = .TRUE.

!     IF ( rank_0 ) THEN
!       WRITE(6,*) 'DEBUG: "gf_iw_FIT_GTAIL" done. Maybe set element to zero if small enough?'
!       WRITE(6,*) giw%gtail(1, 1, 1,:)
!     ENDIF
!
!     CALL MPI_FINALIZE(i_error)
!     STOP

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_FIT_GTAIL()" already called.'
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_SET_MESH" and/or "gf_iw_CALC_G()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_FIT_GTAIL

   !===============================================================================================================================

   SUBROUTINE gf_iw_CALC_EIGEN( giw )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      INTEGER :: spin, kpoint, orbital

      COMPLEX, DIMENSION(giw%n_orbitals, giw%n_orbitals) :: g_tmp

      INTEGER :: dim_work_opt                       !Optimal work array length

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( giw%is_ALLOCATED ) THEN
         IF ( giw%has_H0 .AND. giw%has_SIGMA ) THEN
            IF ( .NOT. giw%has_EIGEN ) THEN

               !giw%EW = CMPLX(0.,0.)
               giw%EW = 0.

               !MPI: superindex loop over spins and kpoints
               CALL parallel_range(1, giw%n_spins*giw%n_kpoints, nprocs, myrank, ista, iend)
               DO ilin=ista,iend

                  CALL parallel_lin2sub_double(ilin, giw%n_spins, giw%n_kpoints,  spin, kpoint)

                  g_tmp = giw%H0(:,:,kpoint) + giw%Sigma(:,:,spin,1)    !no frequency dependence, thus just use first frequency

                  dim_work_opt = -1 !optimal work array size

                  CALL hermitian_matrix_eigenvalues(giw%n_orbitals, g_tmp, giw%EW(:,spin,kpoint), dim_work_opt)
                  CALL hermitian_matrix_eigenvalues(giw%n_orbitals, g_tmp, giw%EW(:,spin,kpoint), dim_work_opt)

               ENDDO

               srcnt = giw%n_orbitals*giw%n_spins*giw%n_kpoints
               CALL MPI_ALLREDUCE(MPI_IN_PLACE,  giw%EW, srcnt, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD, i_error)

               !giw%EW = CMPLX(REAL(giw%EW),0.)    !the problem is hermitian, just zero the imaginary part noise

               !TODO: a) do we need to keep the eigenvectors ? just discard them here ...

               giw%has_EIGEN = .TRUE.

!       !debug
!       IF ( rank_0 ) THEN
!         WRITE(6,*) giw%EW(:,1,1)-giw%EW(:,2,1)
!       ENDIF

            ELSE
               IF ( rank_0 ) THEN
                  WRITE(6,*) 'INFO: "gf_iw_CALC_EIGEN()" already called.'
               ENDIF
            ENDIF

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_SET_H0()" and "gf_iw_SET_SIGMA" need to be called prior to "gf_iw_CALC_EIGEN()" '
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_CALC_EIGEN

   SUBROUTINE gf_iw_CALC_DIAG0( giw )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      INTEGER :: spin, kpoint, orbital

      COMPLEX, DIMENSION(giw%n_orbitals, giw%n_orbitals) :: g_tmp
      COMPLEX, DIMENSION(giw%n_orbitals, giw%n_orbitals) :: output

      INTEGER :: dim_work_opt                       !Optimal work array length

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( giw%is_ALLOCATED ) THEN

         IF ( giw%has_H0  ) THEN
            IF ( .NOT. giw%has_EIGEN0 ) THEN

               !giw%EW = CMPLX(0.,0.)
               giw%EW0 = 0.
               giw%EV0 = CMPLX(0.,0.)

               !MPI: superindex loop over spins and kpoints
               CALL parallel_range(1, giw%n_spins*giw%n_kpoints, nprocs, myrank, ista, iend)
               DO ilin=ista,iend

                  CALL parallel_lin2sub_double(ilin, giw%n_spins, giw%n_kpoints,  spin, kpoint)

                  g_tmp = giw%H0(:,:,kpoint)    !no frequency dependence, thus just use first frequency

                  dim_work_opt = -1 !optimal work array size

                  CALL hermitian_matrix_diagonalization(giw%n_orbitals, g_tmp, giw%EW0(:,spin,kpoint), dim_work_opt)
                  CALL hermitian_matrix_diagonalization(giw%n_orbitals, g_tmp, giw%EW0(:,spin,kpoint), dim_work_opt)
                  giw%EV0(:,:,spin,kpoint) = g_tmp
               ENDDO


               srcnt = giw%n_orbitals*giw%n_spins*giw%n_kpoints
               CALL MPI_ALLREDUCE(MPI_IN_PLACE,  giw%EW0, srcnt, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD, i_error)
               srcnt = giw%n_orbitals*giw%n_orbitals*giw%n_spins*giw%n_kpoints
               CALL MPI_ALLREDUCE(MPI_IN_PLACE,  giw%EV0, srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)

               !giw%EW = CMPLX(REAL(giw%EW),0.)    !the problem is hermitian, just zero the imaginary part noise

               !TODO: a) do we need to keep the eigenvectors ? just discard them here ...

               giw%has_EIGEN0 = .TRUE.


!       !debug
!       IF ( rank_0 ) THEN
!         WRITE(6,*) giw%EW(:,1,1)-giw%EW(:,,1)
!       ENDIF

            ELSE
               IF ( rank_0 ) THEN
                  WRITE(6,*) 'INFO: "gf_iw_CALC_DIAG()" already called.'
               ENDIF
            ENDIF

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_SET_H0()"  needs to be called prior to "gf_iw_CALC_EIGEN()" '
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_CALC_DIAG0

   SUBROUTINE gf_iw_CALC_DIAG( giw )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      INTEGER :: spin, kpoint, orbital

      COMPLEX, DIMENSION(giw%n_orbitals, giw%n_orbitals) :: g_tmp
      COMPLEX, DIMENSION(giw%n_orbitals, giw%n_orbitals) :: output

      INTEGER :: dim_work_opt                       !Optimal work array length

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( giw%is_ALLOCATED ) THEN
         IF ( giw%has_H0 .AND. giw%has_SIGMA ) THEN
            IF ( .NOT. giw%has_EIGEN ) THEN

               !giw%EW = CMPLX(0.,0.)
               giw%EW = 0.
               giw%EV = CMPLX(0.,0.)

               !MPI: superindex loop over spins and kpoints
               CALL parallel_range(1, giw%n_spins*giw%n_kpoints, nprocs, myrank, ista, iend)
               DO ilin=ista,iend

                  CALL parallel_lin2sub_double(ilin, giw%n_spins, giw%n_kpoints,  spin, kpoint)

                  g_tmp = giw%H0(:,:,kpoint) + giw%Sigma(:,:,spin,1)    !no frequency dependence, thus just use first frequency

                  dim_work_opt = -1 !optimal work array size

                  CALL hermitian_matrix_diagonalization(giw%n_orbitals, g_tmp, giw%EW(:,spin,kpoint), dim_work_opt)
                  CALL hermitian_matrix_diagonalization(giw%n_orbitals, g_tmp, giw%EW(:,spin,kpoint), dim_work_opt)
                  giw%EV(:,:,spin,kpoint) = g_tmp
               ENDDO

               srcnt = giw%n_orbitals*giw%n_spins*giw%n_kpoints
               CALL MPI_ALLREDUCE(MPI_IN_PLACE,  giw%EW, srcnt, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD, i_error)
               srcnt = giw%n_orbitals*giw%n_orbitals*giw%n_spins*giw%n_kpoints
               CALL MPI_ALLREDUCE(MPI_IN_PLACE,  giw%EV, srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)

               !giw%EW = CMPLX(REAL(giw%EW),0.)    !the problem is hermitian, just zero the imaginary part noise

               !TODO: a) do we need to keep the eigenvectors ? just discard them here ...

               giw%has_EIGEN = .TRUE.


!       !debug
!       IF ( rank_0 ) THEN
!         WRITE(6,*) giw%EW(:,1,1)-giw%EW(:,,1)
!       ENDIF

            ELSE
               IF ( rank_0 ) THEN
                  WRITE(6,*) 'INFO: "gf_iw_CALC_DIAG()" already called.'
               ENDIF
            ENDIF

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_SET_H0()" and "gf_iw_SET_SIGMA" need to be called prior to "gf_iw_CALC_EIGEN()" '
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_CALC_DIAG

   !===============================================================================================================================

   FUNCTION gf_iw_RETURN_DATA( giw )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      TYPE(gf_iw), INTENT(IN) :: giw
      REAL, DIMENSION(giw%n_orbitals, giw%n_orbitals, giw%n_spins, giw%n_iw) :: gf_iw_RETURN_DATA
         ! ALLOCATE ( giw%data(giw%n_orbitals, giw%n_orbitals, giw%n_spins, giw%n_iw), STAT=allocerror )

      !-----------------------------------------------------------------------------------------------------------------------------

      gf_iw_RETURN_DATA = giw%data

   END FUNCTION gf_iw_RETURN_DATA

   !===============================================================================================================================

   FUNCTION gf_iw_RETURN_EIGEN( giw )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      TYPE(gf_iw), INTENT(IN) :: giw
      REAL, DIMENSION(giw%n_orbitals, giw%n_spins, giw%n_kpoints) :: gf_iw_RETURN_EIGEN

      !-----------------------------------------------------------------------------------------------------------------------------

      gf_iw_RETURN_EIGEN = giw%EW

   END FUNCTION gf_iw_RETURN_EIGEN

   !===============================================================================================================================

   SUBROUTINE gf_iw_CALC_G_EIGEN( giw )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      INTEGER :: spin, kpoint, iw, orbital
      COMPLEX, DIMENSION(giw%n_orbitals,giw%n_orbitals) :: g_tmp

      !=============================================================================================================================

      IF ( giw%is_ALLOCATED ) THEN
         IF ( .NOT. giw%has_DATA ) THEN
            !IF ( giw%has_MESH .AND. giw%has_EIGEN .AND. giw%has_NE .AND. giw%has_MU ) THEN
            IF ( giw%has_MESH .AND. giw%has_EIGEN .AND. giw%has_MU ) THEN

               !Update with chemical potential ...
               giw%data = CMPLX(0.,0.)

               !MPI: superindex loop over kpoints and matsubara frequencies
               CALL parallel_range(1, giw%n_spins*giw%n_iw*giw%n_kpoints, nprocs, myrank, ista, iend)
               DO ilin=ista,iend

                  CALL parallel_lin2sub_triple(ilin, giw%n_spins, giw%n_iw, giw%n_kpoints, spin, iw, kpoint)
                  !g_tmp = CMPLX(0.,0.)


                  !summation over kpoints
                  DO orbital=1,giw%n_orbitals

                     giw%data(orbital,orbital,spin,iw) = giw%data(orbital,orbital,spin,iw) &
                        + giw%k_weight(kpoint) / ( giw%mesh(iw) + giw%mu - giw%EW(orbital,spin,kpoint) )  !k_weight added

                  ENDDO

               ENDDO

               !MPI
               srcnt = (giw%n_spins)*(giw%n_iw)*(giw%n_orbitals**2)
               CALL MPI_ALLREDUCE(MPI_IN_PLACE, giw%data, srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)

               !giw%data = giw%data / giw%n_kpoints                        !normalization wrt. kpoints

               giw%data = giw%data / SUM(giw%k_weight)                     !normalization wrt. kpoints

               giw%has_DATA = .TRUE.

               !gtail, automatic tail calculation assuming G(z) is diagonal
               IF ( giw%use_gtail_fit ) THEN
                  CALL gf_iw_FIT_GTAIL_EIGEN( giw )
               ELSE
                  CALL gf_iw_SET_GTAIL_EIGEN( giw )
               ENDIF

            ELSE
               IF ( rank_0 ) THEN
                  WRITE(6,*) 'STOP: "gf_iw_CALC_G()" cannot be called.'
               ENDIF
               CALL MPI_FINALIZE(i_error)
               STOP
            ENDIF
         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_CALC_G()" already called.'
            ENDIF
         ENDIF
      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_CALC_G_EIGEN

   !===============================================================================================================================

   SUBROUTINE gf_iw_SET_GTAIL_EIGEN( giw )
      !
      !  Purpose:
      !    Set the asymptotic tail of G(z) analytically assuming G(z) is diagonal, this routine is not MPI optimized much
      !    We adopt the strategy outlined in the Phd-Thesis of E.Gull, Appendix B, p-136-141
      !
      !  Record of revisions:
      !      Date       Programmer  Description of change
      !      ====       ==========  =====================
      !    01/06/15     S. Barthel  Original code
      !
      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      INTEGER :: kpoint, orbital, orbital_, spin                            !loop indices

      COMPLEX, DIMENSION(giw%n_orbitals, giw%n_orbitals, giw%n_kpoints, giw%n_spins,2) :: moment_k  !characteristic moment

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( giw%has_mu .AND. giw%has_H0 .AND. giw%has_EIGEN ) THEN
         IF ( .NOT. giw%has_GTAIL ) THEN

            !g is diagonal, i.e. sigma has no frequency dependance / is static: we only need to calculate diagonal coefficients

            !reset
            giw%gtail = CMPLX(0.,0.)
            moment_k = CMPLX(0.,0.)

            !p=0
            giw%gtail(:,:,:,0) = CMPLX(0.,0.)                     !no static part

            !p=1
            IF ( giw%gtail_order_sum >= 1 ) THEN
               DO spin=1,giw%n_spins
                  DO orbital=1,giw%n_orbitals
                     giw%gtail(orbital,orbital,spin,1) = CMPLX(1.,0.)            !{c+,c}=1 if orbital=orbital_
                  ENDDO
               ENDDO
            ENDIF

            !p=2, first non trivial moment
            IF ( giw%gtail_order_sum >= 2 ) THEN

               !create k-dependant polynom, maybe we MPI this with all indices ...
               DO spin=1,giw%n_spins
                  DO orbital=1,giw%n_orbitals
                     moment_k(orbital,orbital,:,spin,1) = giw%EW(orbital,spin,:) - giw%mu !not optimal memory alignment
                  ENDDO
                  !giw%gtail(:,:,spin,2) = SUM( moment_k(:,:,:,spin,1),3 ) / giw%n_kpoints

                  !add k-weight
                  DO orbital=1,giw%n_orbitals
                     giw%gtail(orbital,orbital,spin,2) = SUM( moment_k(orbital,orbital,:,spin,1)*giw%k_weight(:) )&
                                                            / SUM( giw%k_weight )
                  ENDDO

               ENDDO

            ENDIF

            !p>=3 contributions
            IF ( giw%gtail_order_sum >= 3 ) THEN

               CALL parallel_range(1, giw%n_orbitals*giw%n_orbitals*giw%n_kpoints, nprocs, myrank, ista, iend)

               !we have to update tmp_tail for each k-point !
               DO spin=1,giw%n_spins

                  !MPI matrix-matrix, first attempt to MPI this, maybe there is a better way
                  DO ilin=ista,iend
                     CALL parallel_lin2sub_triple(ilin, giw%n_orbitals, giw%n_orbitals, giw%n_kpoints, orbital, orbital_, kpoint)
                     moment_k(orbital,orbital_,kpoint,spin,2) = &
                        SUM( moment_k(orbital,:,kpoint,spin,1) * moment_k(:,orbital_,kpoint,spin,1) )
                  ENDDO

                  srcnt = (giw%n_orbitals)*(giw%n_orbitals)*(giw%n_kpoints)
                  CALL MPI_ALLREDUCE(MPI_IN_PLACE, moment_k(:,:,:,spin,2), srcnt, MPI_DOUBLE_COMPLEX,&
                                        MPI_SUM, MPI_COMM_WORLD, i_error)

                  !giw%gtail(:,:,spin,3) = ( SUM( moment_k(:,:,:,spin,2),3 ) / giw%n_kpoints )

                  !add k-weight
                  DO orbital_=1,giw%n_orbitals
                     DO orbital=1,giw%n_orbitals
                        giw%gtail(orbital,orbital_,spin,3) = &
                           SUM( moment_k(orbital,orbital_,:,spin,2)*giw%k_weight(:) ) / SUM( giw%k_weight )
                     ENDDO
                  ENDDO

               ENDDO

            ENDIF

            giw%has_GTAIL = .TRUE.

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_SET_GTAIL_EIGEN" already called.'
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_SET_GTAIL_EIGEN()" cannot be called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_SET_GTAIL_EIGEN

   !===============================================================================================================================

   SUBROUTINE gf_iw_FIT_GTAIL_EIGEN( giw )
      !
      !  Purpose:
      !    Fit the asymptotic tail of G(z) assuming G(z) is diagonal
      !
      !  Record of revisions:
      !      Date       Programmer  Description of change
      !      ====       ==========  =====================
      !    03/09/15     S. Barthel  Original code
      !
      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      COMPLEX, DIMENSION(giw%n_fit) :: inverse_iw               !Inverse positive Matsubara frequencies
      COMPLEX, DIMENSION(giw%n_fit) :: tail_data                !Complex data to fit

      INTEGER :: spin, orbital, iw, i                       !loop indices
      INTEGER :: dim_work_opt                           !Optimal work array length

      !-------------------------------------------------------------------------------------------------------------------------------

      IF ( giw%has_MESH .AND. giw%has_DATA ) THEN
         IF ( .NOT. giw%has_GTAIL ) THEN

            giw%gtail = CMPLX(0.,0.)

            DO iw=1,giw%n_fit
               inverse_iw(iw) = 1./giw%mesh(giw%fit_start+iw)
            ENDDO

            DO spin=1,giw%n_spins
               DO orbital=1,giw%n_orbitals

                  !prepare data used for fitting
                  DO iw=1,giw%n_fit
                     tail_data(iw) = giw%data(orbital, orbital, spin, giw%fit_start+iw)
                  ENDDO

                  dim_work_opt = -1 !optimal work array size

                  !fit of order (dim_p-1)+1, ugly
                  CALL complex_least_square_fit( giw%n_fit, tail_data, inverse_iw, giw%gtail_order_fit+1, dim_work_opt )
                  CALL complex_least_square_fit( giw%n_fit, tail_data, inverse_iw, giw%gtail_order_fit+1, dim_work_opt )

                  !indices are shifted in fit routine, ugly
                  giw%gtail(orbital, orbital, spin,:) = tail_data(1:giw%gtail_order_fit+1)

               ENDDO
            ENDDO

            giw%has_GTAIL = .TRUE.

!     IF ( rank_0 ) THEN
!       WRITE(6,*) 'DEBUG: "gf_iw_FIT_GTAIL" done. Maybe set element to zero if small enough?'
!       WRITE(6,*) giw%gtail(1, 1, 1,:)
!     ENDIF
!
!     CALL MPI_FINALIZE(i_error)
!     STOP

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_FIT_GTAIL()" already called.'
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_SET_MESH" and/or "gf_iw_CALC_G()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_FIT_GTAIL_EIGEN

   !===============================================================================================================================

   SUBROUTINE gf_iw_CALC_DENSITY( giw )    !old, should not be used anymore

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      INTEGER :: ioerror, cut

      INTEGER :: spin, orbital, orbital_prime, iw           !loop indices

      TYPE(gf_iw), INTENT(INOUT) :: giw

      COMPLEX, DIMENSION(giw%n_iw) :: g_tmp
      REAL :: R2

      !=============================================================================================================================

      IF ( giw%has_DATA ) THEN
         IF ( .NOT. giw%has_DENSITY ) THEN

            giw%density = CMPLX(0.,0.)

            CALL parallel_range(1, giw%n_orbitals*giw%n_orbitals*giw%n_spins, nprocs, myrank, ista, iend)
            DO ilin=ista,iend
               CALL parallel_lin2sub_triple(ilin, giw%n_orbitals, giw%n_orbitals, giw%n_spins, orbital, orbital_prime, spin)

               g_tmp = giw%data(orbital,orbital_prime,spin,:)

!       OPEN (UNIT=26,FILE='./G_DBG.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
!       DO iw=1,giw%n_iw
!         WRITE(26,*) iw, REAL(g_tmp(iw)), AIMAG(g_tmp(iw))
!       ENDDO
!       CLOSE(26)
!
!       STOP

               !CALL matsubara_sum(giw%n_iw,AIMAG(giw%mesh),g_tmp,giw%n_iw/10,4,giw%density(orbital,orbital_prime,spin),R2)

               !Loop over cutoff frequencies

!       OPEN (UNIT=26,FILE='./SUM_EXTRAPOLATED.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
!
!       DO cut=3,giw%n_iw/2
!
!         g_tmp = giw%data(orbital,orbital_prime,spin,:)

               IF ( orbital .EQ. orbital_prime ) THEN
                  g_tmp = g_tmp - 1./giw%mesh !asymptotic
               ENDIF

               !simple sum (this yields almost exactly identical occupations using inversion vs. eigenbasis)
               !giw%density(orbital,orbital_prime,spin) = SUM(g_tmp)

               !extrapolated sum (this yields LESS exactly identical occupations using inversion vs. eigenbasis)
               !giw%density(orbital,orbital_prime,spin) = CMPLX(0.,0.)
               !CALL matsubara_sum(giw%n_iw, AIMAG(giw%mesh), g_tmp, cut, 4, giw%density(orbital,orbital_prime,spin) , R2)

               CALL complex_matsubara_sum(giw%n_iw, giw%mesh, g_tmp, 2, 2, giw%density(orbital,orbital_prime,spin) , R2) !fail safe
               !CALL complex_matsubara_sum(giw%n_iw, giw%mesh, g_tmp, INT(giw%n_iw/4.), 4, giw%density(orbital,orbital_prime,spin) , R2) !fail safe

!         WRITE(26,'(3x,F19.15,1X,F19.15,1X,F19.15)') AIMAG(giw%mesh(giw%n_iw-cut+1)), REAL(giw%density(orbital,orbital_prime,spin)/giw%beta), &
!       AIMAG(giw%density(orbital,orbital_prime,spin)/giw%beta)

!         WRITE(26,'(3x,I8,1X,F19.15,1X,F19.15)') (giw%n_iw-cut+1), REAL(giw%density(orbital,orbital_prime,spin)/giw%beta), &
!       AIMAG(giw%density(orbital,orbital_prime,spin)/giw%beta)



!
!       ENDDO
!
!       CLOSE(26)
!
!       STOP

               giw%density(orbital,orbital_prime,spin) = giw%density(orbital,orbital_prime,spin) / giw%beta    !beta

               IF ( orbital .EQ. orbital_prime ) THEN
                  giw%density(orbital,orbital,spin) = CMPLX(REAL(giw%density(orbital,orbital,spin))+0.5,0.) !asymptotic & real occupations
               ENDIF

            ENDDO

            srcnt = giw%n_orbitals*giw%n_orbitals*giw%n_spins
            CALL MPI_ALLREDUCE(MPI_IN_PLACE, giw%density, srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)

            giw%has_DENSITY = .TRUE.

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_CALC_DENSITY()" already called.'
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'INFO: "gf_iw_CALC_G()" needs to be called prior to "gf_iw_CALC_DENSITY()" '
         ENDIF
      ENDIF

   END SUBROUTINE gf_iw_CALC_DENSITY

   !===============================================================================================================================

   SUBROUTINE gf_iw_CALC_DENSITY_TAIL( giw, only_diag )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      LOGICAL, INTENT(IN) :: only_diag

      INTEGER :: ioerror, cut

      INTEGER :: spin, orbital, orbital_prime, iw           !loop indices

      COMPLEX, DIMENSION(giw%n_iw) :: g_tmp
      REAL :: R2

      INTEGER :: p

      !             !n=1  1/2
      !             !n=2 -beta/4
      !             !n=3  0
      !             !n=4  (beta^3)/48
      !             !n=5  0
      !             !n=6  -beta^5/480
      !             !n=7  0
      !             !n=8  17*beta^7/16/5040
      !             !n=9  0
      !             !series expansion of fermi-function (needs testing)

      REAL, DIMENSION(1:12) :: w_gtail  !analytical corrections to matsubara sum up to order 10

      !=============================================================================================================================

      IF ( giw%has_DATA .AND. giw%has_GTAIL ) THEN
         IF ( .NOT. giw%has_DENSITY ) THEN

            !analytical correction (series expansion of Fermi-function)
            w_gtail = 0.
            w_gtail(1) = (1.) / 2
            w_gtail(2) = -(giw%beta) / 4
            w_gtail(4) =  (giw%beta**3) / 48
            w_gtail(6) = -(giw%beta**5) / 480
            w_gtail(8) =  ( 17*(giw%beta**7) ) / 80640
            w_gtail(10) = (-31*(giw%beta**9) ) / 1451520
            w_gtail(12) = (691*(giw%beta**11) ) / 319334400

            giw%density = CMPLX(0.,0.)

            !calculate full density matrix
            IF ( .NOT. only_diag ) THEN

               CALL parallel_range(1, giw%n_orbitals*giw%n_orbitals*giw%n_spins, nprocs, myrank, ista, iend)
               DO ilin=ista,iend
                  CALL parallel_lin2sub_triple(ilin, giw%n_orbitals, giw%n_orbitals, giw%n_spins, orbital, orbital_prime, spin)

                  g_tmp = giw%data(orbital,orbital_prime,spin,:)

                  !subtract the whole tail, but not zero order !
                  DO p=1,giw%gtail_order_sum
                     g_tmp = g_tmp - ( giw%gtail(orbital, orbital_prime, spin, p) )*( (1./giw%mesh)**(p) )
                  ENDDO

                  !simple sum
                  giw%density(orbital,orbital_prime,spin) = SUM(g_tmp) / giw%beta   !beta

                  !         !extrapolated integral, needs testing, does not agree with TRIQS & W2DYN, but may be more exact ...
                  !         CALL complex_matsubara_sum(giw%n_iw, giw%mesh, g_tmp, 2, 2, giw%density(orbital,orbital_prime,spin) , R2)
                  !         giw%density(orbital,orbital_prime,spin) = giw%density(orbital,orbital_prime,spin) / giw%beta

                  !analytical corrections
                  DO p=1,giw%gtail_order_sum
                     giw%density(orbital,orbital_prime,spin) = giw%density(orbital,orbital_prime,spin) &
                        + giw%gtail(orbital,orbital_prime,spin,p)*w_gtail(p)
                     !           IF ( rank_0 ) THEN
                     !         IF ( orbital == orbital_prime ) THEN
                     !           WRITE(6,*) orbital, p, giw%gtail(orbital,orbital_prime,spin,p), w_gtail(p)
                     !         ENDIF
                     !               ENDIF
                  ENDDO

                  !         CALL MPI_FINALIZE(i_error)
                  !         STOP

                  !just disable for testing
                  !         IF ( orbital .EQ. orbital_prime ) THEN
                  !           giw%density(orbital,orbital,spin) = CMPLX(REAL(giw%density(orbital,orbital,spin))+0.5,0.) !asymptotic & real occupations
                  !         ENDIF

               ENDDO

               !calculate only diagonal of density matrix, i.e. occupations
            ELSE

               CALL parallel_range(1, giw%n_orbitals*giw%n_spins, nprocs, myrank, ista, iend)
               DO ilin=ista,iend
                  CALL parallel_lin2sub_double(ilin, giw%n_orbitals, giw%n_spins, orbital, spin)

                  g_tmp = giw%data(orbital,orbital,spin,:)

                  !subtract the whole tail, but not zero order !
                  DO p=1,giw%gtail_order_sum
                     g_tmp = g_tmp - ( giw%gtail(orbital, orbital, spin, p) )*( (1./giw%mesh)**(p) )
                  ENDDO

                  !simple sum
                  giw%density(orbital,orbital,spin) = SUM(g_tmp) / giw%beta !beta

                  !analytical corrections
                  DO p=1,giw%gtail_order_sum
                     giw%density(orbital,orbital,spin) = giw%density(orbital,orbital,spin) + &
                                                            giw%gtail(orbital,orbital,spin,p)*w_gtail(p)
                  ENDDO

               ENDDO

            ENDIF

            srcnt = giw%n_orbitals*giw%n_orbitals*giw%n_spins
            CALL MPI_ALLREDUCE(MPI_IN_PLACE, giw%density, srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)

            giw%has_DENSITY = .TRUE.

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_CALC_DENSITY()" already called.'
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'INFO: "gf_iw_CALC_G()" needs to be called prior to "gf_iw_CALC_DENSITY()" '
         ENDIF
      ENDIF

   END SUBROUTINE gf_iw_CALC_DENSITY_TAIL

   SUBROUTINE gf_iw_CALC_DENSITY_T0( giw, only_diag, single_kpoint )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      LOGICAL, INTENT(IN) :: only_diag
      LOGICAL, INTENT(IN) :: single_kpoint

      INTEGER :: ioerror, cut

      INTEGER :: spin, orbital, orbital_prime,  kpoint, iw          !loop indices
      INTEGER :: orbital_i, orbital_j,  orbital_k, orbital_l            !loop indices

      COMPLEX, DIMENSION(giw%n_iw) :: g_tmp

      REAL, DIMENSION(giw%n_orbitals,giw%n_spins,giw%n_kpoints) :: density_k

      REAL :: density_sum, counter, missing, trace_rho_Hk

      COMPLEX, DIMENSION(giw%n_orbitals,giw%n_orbitals,giw%n_spins,giw%n_kpoints) :: density_k_loc
      COMPLEX, DIMENSION(giw%n_orbitals,giw%n_orbitals,giw%n_spins) :: density_sum_loc
      COMPLEX, DIMENSION(giw%n_orbitals,giw%n_spins) :: density_sum_loc_diag

      REAL :: R2

      INTEGER :: p

      REAL, DIMENSION(1:12) :: w_gtail  !analytical corrections to matsubara sum up to order 10

      !=============================================================================================================================

      IF ( .NOT. giw%has_DENSITY ) THEN

         !analytical correction (series expansion of Fermi-function)

         giw%density = CMPLX(0.,0.)

         density_k = 0.0
         density_sum = 0.0

         density_k_loc = CMPLX(0.,0.)
         density_sum_loc = CMPLX(0.,0.)
         density_sum_loc_diag = CMPLX(0.,0.)

         ! fill up the density matrix in eigenbasis by fermifunction
         counter = 0.0
         CALL parallel_range(1, giw%n_spins*giw%n_kpoints*giw%n_orbitals, nprocs, myrank, ista, iend)
         DO orbital = 1,giw%n_orbitals
            DO spin = 1,giw%n_spins
               DO kpoint = 1,giw%n_kpoints
                  IF ( giw%EW(orbital,spin,kpoint) < giw%mu ) THEN
                     density_k(orbital,spin,kpoint) = 1.0
                  ELSEIF ( giw%EW(orbital,spin,kpoint) == giw%mu ) THEN
                     counter = counter + 1.0
                     !WRITE(6,*) giw%EW(orbital,spin,kpoint)
                  ELSE
                     density_k(orbital,spin,kpoint) = 0.0
                  ENDIF
               END DO
            END DO
         END DO
         !DO ilin=ista,iend
         !  CALL parallel_lin2sub_triple(ilin, giw%n_orbitals,giw%n_spins, giw%n_kpoints, orbital,  spin, kpoint)
         !    IF ( giw%EW(orbital,spin,kpoint) < giw%mu ) THEN
         !            density_k(orbital,spin,kpoint) = 1.0
         !        ELSEIF ( giw%EW(orbital,spin,kpoint) == giw%mu ) THEN
         !            counter = counter + 1.0
         !            !WRITE(6,*) giw%EW(orbital,spin,kpoint)
         !        ELSE
         !            density_k(orbital,spin,kpoint) = 0.0
         !    ENDIF

         !ENDDO

         !srcnt = giw%n_spins*giw%n_kpoints*giw%n_orbitals
         !CALL MPI_ALLREDUCE(MPI_IN_PLACE, density_k, srcnt, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD, i_error)
         missing = giw%Ne*giw%n_kpoints - SUM(density_k)
         DO orbital = 1,giw%n_orbitals
            DO spin = 1,giw%n_spins
               DO kpoint = 1,giw%n_kpoints
                  IF ( giw%EW(orbital,spin,kpoint) == giw%mu ) THEN
                     ! equally distribute to degenrate highest occ. eigenvalues
                     density_k(orbital,spin,kpoint) = missing/counter
                     ! just put it on the first few...
                     !density_k(orbital,spin,kpoint) = 1.0
                     !missing = missing - 1.0
                     !IF (ABS(missing) < 0.0001) then
                     !    EXIT
                     !END IF

                  ENDIF
               END DO
            END DO
         END DO
         !WRITE(6,*) missing, giw%Ne, SUM(density_k)
         !CALL parallel_range(1, giw%n_spins*giw%n_kpoints*giw%n_orbitals, nprocs, myrank, ista, iend)
         !    DO ilin=ista,iend
         !      CALL parallel_lin2sub_triple(ilin, giw%n_orbitals,giw%n_spins, giw%n_kpoints, orbital,  spin, kpoint)
         !        IF ( giw%EW(orbital,spin,kpoint) == giw%mu ) THEN
         !                ! equally distribute to degenrate highest occ. eigenvalues
         !                !density_k(orbital,spin,kpoint) = missing/counter
         !                ! just put it on the first few...
         !                density_k(orbital,spin,kpoint) = 1.0
         !                missing = missing - 1.0
         !                IF (ABS(missing) < 0.0001) then
         !                    EXIT
         !                END IF
         !
         !        ENDIF
         !
         !    ENDDO

         !srcnt = giw%n_spins*giw%n_kpoints*giw%n_orbitals
         !CALL MPI_ALLREDUCE(MPI_IN_PLACE, density_k, srcnt, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD, i_error)
         giw%density_k_eig = density_k
         density_sum = SUM(density_k)/ REAL(SUM( giw%k_weight ))
         !WRITE(6,*) SUM(density_k), counter
         !WRITE(6,*) density_k(1,1,1)
         !WRITE(6,*) giw%k_weight(1), REAL(SUM( giw%k_weight ))
         !trace_rho_Hk = SUM(density_k*giw%EW)/REAL(SUM( giw%k_weight ))
         IF ( .NOT. single_kpoint ) THEN
            IF ( rank_0 ) THEN
               WRITE(6,'(3X,"Number of electrons in density matrix:",1X,F19.11)'), density_sum
               !WRITE(6,'(3X,"Trace rho eps_HF:",1X,F19.11)'), trace_rho_Hk
            ENDIF
         ENDIF
         IF ( .NOT. single_kpoint ) THEN
            IF ( ABS( density_sum- giw%Ne) > 0.00000001 ) THEN
               IF ( rank_0 ) THEN
                  WRITE(6,*) 'Number of electrons in density matrix does not equal total electron number! Exiting...'
                  CALL EXIT(0)
               ENDIF
            ENDIF
         ENDIF
         !calculate full density matrix in localized basis

         ! transform back to localized basis on every k-point
         CALL parallel_range(1, giw%n_spins*giw%n_kpoints, nprocs, myrank, ista, iend)

         DO ilin=ista,iend
            CALL parallel_lin2sub_double(ilin, giw%n_spins, giw%n_kpoints, spin, kpoint)
            CALL trans_REAL_EIG_TO_BASIS_HE(giw%n_orbitals,density_k(:, spin, kpoint),giw%EV(:,:,spin,kpoint),&
                                            density_k_loc(:,:, spin, kpoint))
         END DO


         srcnt = giw%n_spins*giw%n_kpoints*giw%n_orbitals*giw%n_orbitals
         CALL MPI_ALLREDUCE(MPI_IN_PLACE, density_k_loc, srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)

         density_sum_loc = SUM(density_k_loc,4)
         ! normalize over number of k-points
         density_sum_loc = density_sum_loc / CMPLX( giw%n_kpoints ,0.0)
         !density_sum_loc_diag = density_sum_loc_diag / REAL((giw%n_kpoints))
         !IF ( rank_0 ) THEN
         !    WRITE(6,'(3X,"Number of electrons in density matrix from loc basis:",1X,F4.1,1X,F4.1)'),  REAL(SUM(density_sum_loc_diag))
         ! ENDIF

         giw%density = density_sum_loc
         giw%density_k = density_k_loc

         giw%has_DENSITY = .TRUE.

         !CALL EXIT(0)
      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'INFO: "gf_iw_CALC_DENSITY()" already called.'
         ENDIF
      ENDIF
   END SUBROUTINE gf_iw_CALC_DENSITY_T0

   !===============================================================================================================================

   SUBROUTINE gf_iw_CALC_DENSITY_TAIL_serial( giw, only_diag )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      LOGICAL, INTENT(IN) :: only_diag

      INTEGER :: ioerror, cut

      INTEGER :: spin, orbital, orbital_prime, iw           !loop indices

      COMPLEX, DIMENSION(giw%n_iw) :: g_tmp
      REAL :: R2

      INTEGER :: p

      !             !n=1  1/2
      !             !n=2 -beta/4
      !             !n=3  0
      !             !n=4  (beta^3)/48
      !             !n=5  0
      !             !n=6  -beta^5/480
      !             !n=7  0
      !             !n=8  17*beta^7/16/5040
      !             !n=9  0
      !             !series expansion of fermi-function (needs testing)

      REAL, DIMENSION(1:12) :: w_gtail  !analytical corrections to matsubara sum up to order 10

      !=============================================================================================================================

      IF ( giw%has_DATA .AND. giw%has_GTAIL ) THEN
         IF ( .NOT. giw%has_DENSITY ) THEN

            !analytical correction (series expansion of Fermi-function)
            w_gtail = 0.
            w_gtail(1) = (1.) / 2
            w_gtail(2) = -(giw%beta) / 4
            w_gtail(4) =  (giw%beta**3) / 48
            w_gtail(6) = -(giw%beta**5) / 480
            w_gtail(8) =  ( 17*(giw%beta**7) ) / 80640
            w_gtail(10) = (-31*(giw%beta**9) ) / 1451520
            w_gtail(12) = (691*(giw%beta**11) ) / 319334400

            giw%density = CMPLX(0.,0.)

            !calculate full density matrix
            IF ( .NOT. only_diag ) THEN

!       CALL parallel_range(1, giw%n_orbitals*giw%n_orbitals*giw%n_spins, nprocs, myrank, ista, iend)
!       DO ilin=ista,iend

               DO spin=1,giw%n_spins
                  DO orbital_prime=1,giw%n_orbitals
                     DO orbital=1,giw%n_orbitals


                        !         CALL parallel_lin2sub_triple(ilin, giw%n_orbitals, giw%n_orbitals, giw%n_spins, orbital, orbital_prime, spin)

                        g_tmp = giw%data(orbital,orbital_prime,spin,:)

                        !subtract the whole tail, but not zero order !
                        DO p=1,giw%gtail_order_sum
                           g_tmp = g_tmp - ( giw%gtail(orbital, orbital_prime, spin, p) )*( (1./giw%mesh)**(p) )
                        ENDDO

                        !simple sum
                        giw%density(orbital,orbital_prime,spin) = SUM(g_tmp) / giw%beta   !beta

                        !         !extrapolated integral, needs testing, does not agree with TRIQS & W2DYN, but may be more exact ...
                        !         CALL complex_matsubara_sum(giw%n_iw, giw%mesh, g_tmp, 2, 2, giw%density(orbital,orbital_prime,spin) , R2)
                        !         giw%density(orbital,orbital_prime,spin) = giw%density(orbital,orbital_prime,spin) / giw%beta

                        !analytical corrections
                        DO p=1,giw%gtail_order_sum
                           giw%density(orbital,orbital_prime,spin) = giw%density(orbital,orbital_prime,spin) &
                              + giw%gtail(orbital,orbital_prime,spin,p)*w_gtail(p)
                           !           IF ( rank_0 ) THEN
                           !         IF ( orbital == orbital_prime ) THEN
                           !           WRITE(6,*) orbital, p, giw%gtail(orbital,orbital_prime,spin,p), w_gtail(p)
                           !         ENDIF
                           !               ENDIF
                        ENDDO

                        !         CALL MPI_FINALIZE(i_error)
                        !         STOP

                        !just disable for testing
                        !         IF ( orbital .EQ. orbital_prime ) THEN
                        !           giw%density(orbital,orbital,spin) = CMPLX(REAL(giw%density(orbital,orbital,spin))+0.5,0.) !asymptotic & real occupations
                        !         ENDIF

                     ENDDO
                  ENDDO
               ENDDO

               !calculate only diagonal of density matrix, i.e. occupations
            ELSE

               !CALL parallel_range(1, giw%n_orbitals*giw%n_spins, nprocs, myrank, ista, iend)
               !DO ilin=ista,iend

               DO spin=1,giw%n_spins
                  DO orbital=1,giw%n_orbitals

                     !CALL parallel_lin2sub_double(ilin, giw%n_orbitals, giw%n_spins, orbital, spin)

                     g_tmp = giw%data(orbital,orbital,spin,:)

                     !subtract the whole tail, but not zero order !
                     DO p=1,giw%gtail_order_sum
                        g_tmp = g_tmp - ( giw%gtail(orbital, orbital, spin, p) )*( (1./giw%mesh)**(p) )
                     ENDDO

                     !simple sum
                     giw%density(orbital,orbital,spin) = SUM(g_tmp) / giw%beta   !beta

                     !analytical corrections
                     DO p=1,giw%gtail_order_sum
                        giw%density(orbital,orbital,spin) = giw%density(orbital,orbital,spin) &
                                                                + giw%gtail(orbital,orbital,spin,p)*w_gtail(p)
                     ENDDO

                  ENDDO
               ENDDO

            ENDIF

!     srcnt = giw%n_orbitals*giw%n_orbitals*giw%n_spins
!     CALL MPI_ALLREDUCE(MPI_IN_PLACE, giw%density, srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)

            giw%has_DENSITY = .TRUE.

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_CALC_DENSITY()" already called.'
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'INFO: "gf_iw_CALC_G()" needs to be called prior to "gf_iw_CALC_DENSITY()" '
         ENDIF
      ENDIF

   END SUBROUTINE gf_iw_CALC_DENSITY_TAIL_serial

   !===============================================================================================================================

   FUNCTION gf_iw_RETURN_DENSITY_K( giw )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      TYPE(gf_iw), INTENT(INOUT) :: giw     !this is AFM hack ...

      COMPLEX, DIMENSION(giw%n_orbitals, giw%n_orbitals, giw%n_spins,giw%n_kpoints) :: gf_iw_RETURN_DENSITY_K

      gf_iw_RETURN_DENSITY_K = giw%density_k

   END FUNCTION gf_iw_RETURN_DENSITY_K


   FUNCTION gf_iw_RETURN_DENSITY_K_EIG( giw )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      TYPE(gf_iw), INTENT(INOUT) :: giw     !this is AFM hack ...

      COMPLEX, DIMENSION(giw%n_orbitals,  giw%n_spins,giw%n_kpoints) :: gf_iw_RETURN_DENSITY_K_EIG
      gf_iw_RETURN_DENSITY_K_EIG = giw%density_k_eig

   END FUNCTION gf_iw_RETURN_DENSITY_K_EIG

   FUNCTION gf_iw_RETURN_H0_Eig( giw )


      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      TYPE(gf_iw), INTENT(INOUT) :: giw     !this is AFM hack ...

      COMPLEX, DIMENSION(giw%n_orbitals, giw%n_spins,giw%n_kpoints) :: gf_iw_RETURN_H0_Eig
      gf_iw_RETURN_H0_Eig = giw%EW0

   END FUNCTION gf_iw_RETURN_H0_Eig


   FUNCTION gf_iw_RETURN_DENSITY( giw, sym_t2g_eg, sym_afm, sym_ions, n_ions )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      TYPE(gf_iw), INTENT(INOUT) :: giw     !this is AFM hack ...
      LOGICAL, INTENT(IN) :: sym_t2g_eg
      LOGICAL, INTENT(IN) :: sym_afm
      LOGICAL, INTENT(IN) :: sym_ions
      INTEGER, INTENT(IN) :: n_ions

      COMPLEX, DIMENSION(giw%n_orbitals, giw%n_orbitals, giw%n_spins) :: gf_iw_RETURN_DENSITY

      !REAL :: n_t2g, n_eg      !occupation in the t2g/eg symmetry

      INTEGER :: spin, ion, orbital     !loop indices
      INTEGER, PARAMETER :: L_max = 5
      REAL, DIMENSION(2) :: n_t2g_eg        !symmetrized occupations
      INTEGER, DIMENSION(5) :: idx_t2g_eg   !index array for t2g/eg

      !-----------------------------------------------------------------------------------------------------------------------------

      !return
      gf_iw_RETURN_DENSITY = giw%density

      IF ( sym_t2g_eg ) THEN

         idx_t2g_eg = (/1,1,2,1,2/)

         !Symmetrize density matrix (t2g, eg) quick and dirty approach, we will write a general symmetry module later, VASP order
         !
         ! t2g   0   0   0   0
         !   0 t2g   0   0   0
         !   0   0  eg   0   0
         !   0   0   0 t2g   0
         !   0   0   0   0  eg

!   !symmetrized occupations
!   n_t2g = REAL( ( giw%density(1,1,1)+giw%density(2,2,1)+giw%density(4,4,1) ) ) / 3.
!   n_eg = REAL( ( giw%density(3,3,1)+giw%density(5,5,1) ) ) / 2.
!
!   !zero t2g/eg
!   gf_iw_RETURN_DENSITY(1:5,1:5,:) = CMPLX(0.,0.)
!
!   !set t2g/eg
!   gf_iw_RETURN_DENSITY(1,1,:) = CMPLX(n_t2g, 0.)
!   gf_iw_RETURN_DENSITY(2,2,:) = CMPLX(n_t2g, 0.)
!   gf_iw_RETURN_DENSITY(3,3,:) = CMPLX(n_eg, 0.)
!   gf_iw_RETURN_DENSITY(4,4,:) = CMPLX(n_t2g, 0.)
!   gf_iw_RETURN_DENSITY(5,5,:) = CMPLX(n_eg, 0.)


         IF ( sym_afm .AND. n_ions == 2 ) THEN
            !AFM NiO
            giw%density(1:L_max,1:L_max,1) = (giw%density(1:L_max,1:L_max,1)+giw%density(L_max+1:2*L_max,L_max+1:2*L_max,2)) / 2.
            giw%density(L_max+1:2*L_max,L_max+1:2*L_max,2) = giw%density(1:L_max,1:L_max,1)

            giw%density(1:L_max,1:L_max,2) = (giw%density(1:L_max,1:L_max,2)+giw%density(L_max+1:2*L_max,L_max+1:2*L_max,1)) / 2.
            giw%density(L_max+1:2*L_max,L_max+1:2*L_max,1) = giw%density(1:L_max,1:L_max,2)
         ENDIF

         DO spin=1,giw%n_spins
            DO ion=1,n_ions

               !symmetrized occupations
               n_t2g_eg = 0.
               DO orbital=1,L_max
                  n_t2g_eg( idx_t2g_eg(orbital) ) = n_t2g_eg( idx_t2g_eg(orbital) ) &
                     + REAL( giw%density((ion-1)*L_max+orbital,(ion-1)*L_max+orbital,spin) )
               ENDDO
               n_t2g_eg(1) = n_t2g_eg(1) / 3.
               n_t2g_eg(2) = n_t2g_eg(2) / 2.

               !zero t2g/eg
               gf_iw_RETURN_DENSITY((ion-1)*L_max+1:(ion-1)*L_max+5,(ion-1)*L_max+1:(ion-1)*L_max+5,spin) = CMPLX(0.,0.)

               !set t2g/eg
               DO orbital=1,L_max
                  gf_iw_RETURN_DENSITY( (ion-1)*L_max+orbital,(ion-1)*L_max+orbital,spin) = CMPLX(n_t2g_eg(idx_t2g_eg(orbital)),0.)
               ENDDO

            ENDDO
         ENDDO

      ENDIF

      !ZnO LDA+U result - we need to understand why there are still imaginary parts ...
!         1.0181  0.0000 -0.0000 -0.0115 -0.0000
!     0.0000  1.0073 -0.0000  0.0000 -0.0115
!        -0.0000 -0.0000  1.0263  0.0000  0.0000
!        -0.0115  0.0000  0.0000  1.0073 -0.0000
!        -0.0000 -0.0115  0.0000 -0.0000  1.0181

      !symmetrize ions

      IF ( sym_ions .AND. n_ions == 2 ) THEN
         !paramagnetic VO2
         giw%density(1:L_max,1:L_max,:) = (giw%density(1:L_max,1:L_max,:)+giw%density(L_max+1:2*L_max,L_max+1:2*L_max,:)) / 2.
         giw%density(L_max+1:2*L_max,L_max+1:2*L_max,:) = giw%density(1:L_max,1:L_max,:)
         gf_iw_RETURN_DENSITY = giw%density
      ENDIF

   END FUNCTION gf_iw_RETURN_DENSITY

   !===============================================================================================================================

   FUNCTION gf_iw_RETURN_DENSITY_TRACE( giw, depth_trace ) !SPIN-RESOLVED

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      TYPE(gf_iw), INTENT(IN) :: giw
      INTEGER, INTENT(IN) :: depth_trace

      COMPLEX, DIMENSION(giw%n_spins) :: gf_iw_RETURN_DENSITY_TRACE !trace of density-matrix wrt. orbitals, spin-resolved
      INTEGER :: orbital                        !loop index
      INTEGER :: orbital_trace

      !-----------------------------------------------------------------------------------------------------------------------------

      gf_iw_RETURN_DENSITY_TRACE = CMPLX(0.,0.)

      orbital_trace = depth_trace
      IF ( (orbital_trace > giw%n_orbitals) .OR. (orbital_trace <= 0) ) THEN
         orbital_trace = giw%n_orbitals
      ENDIF

      DO orbital=1,orbital_trace
         gf_iw_RETURN_DENSITY_TRACE = gf_iw_RETURN_DENSITY_TRACE + giw%density(orbital,orbital,:)
      ENDDO

   END FUNCTION gf_iw_RETURN_DENSITY_TRACE

   !===============================================================================================================================

   SUBROUTINE gf_iw_ADJUST_MU( giw, mu_int, is_metallic, only_diag, set_mu_dft )

      USE timings
      !USE ifport

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      REAL, DIMENSION(3), INTENT(IN) :: mu_int
      LOGICAL, INTENT(IN) :: is_metallic
      LOGICAL, INTENT(IN) :: only_diag
      LOGICAL, INTENT(IN) :: set_mu_dft

      REAL, DIMENSION(4) :: mu
      REAL, DIMENSION(4) :: Ne                          !Total number of electrons during nested intervalls
      REAL, DIMENSION(4) :: delta
      REAL, DIMENSION(3) :: dmu
      REAL :: mu_nr, tmp

      REAL :: s

      LOGICAL :: mflag

      INTEGER :: iteration
      INTEGER :: spin, kpoint, orbital, iw                  !loop indices
      INTEGER :: i, i_start, i_end                      !Loop indices
      INTEGER :: ioerror

      INTEGER :: orbital_
      INTEGER :: p

      !=============================================================================================================================

      IF ( giw%has_MESH .AND. giw%has_H0 .AND. giw%has_SIGMA .AND. giw%has_NE ) THEN


         IF ( .NOT. giw%is_CONSISTENT ) THEN

            IF ( rank_0 ) THEN
               WRITE(6,*) '  Calculating chemical potential µ ...'
               IF ( giw%has_EIGEN ) THEN
                  WRITE(6,*) '  Calculating Green function in Eigen basis'
               ELSE
                  WRITE(6,*) '  Not calculating Green function in Eigen basis'
               END IF
               WRITE(6,'(3X,"Inverse temperature β:",1X,F21.16,1X,"1/eV")'), giw%beta
               WRITE(6,'(3X,"Temperature T:",1X,F21.16,1X,"K")'), 1./(giw%beta*kB)
               WRITE(6,'(3X,"Number of Mats. Freq.:",1X,I5,1X)'), (giw%n_iw)
               WRITE(6,'(3X,"Total charge N:",1X,F20.16,1X,"e-")'), giw%Ne
               WRITE(6,'(3X,"µ in [",F14.10,1X,F14.10,"]")') mu_int(1), mu_int(3)
               !OPEN(UNIT=6, CARRIAGECONTROL='FORTRAN')
               OPEN(UNIT=6)
               WRITE(6,*) ''
            ENDIF

            mu(1:3) = mu_int
            mu(4) = -1.
            delta(2) = 1.
            delta(4) = mu(2)-mu(4)

            timesum = MPI_WTIME()

            iteration = 0

            IF (set_mu_dft .EQV. .FALSE.) THEN
               DO WHILE ( ABS(delta(4)) .GT. giw%tol )   !mu

                  iteration = iteration + 1
                  ! WRITE(6,*) 'debug 1'

                  IF ( iteration .EQ. 1 ) THEN
                     i_start = 1
                     i_end = 3
                  ELSE
                     i_start = 2
                     i_end = 2
                  ENDIF

                  !nested intervalls loop
                  DO i=i_start,i_end

                  ! WRITE(6,*) 'debug 2'
                     CALL gf_iw_SET_MU( giw, mu(i) )
                     ! WRITE(6,*) 'debug 4'

                     IF ( giw%has_EIGEN ) THEN
                         ! WRITE(6,*) 'debug 11'
                        CALL gf_iw_CALC_G_EIGEN( giw )
                     ELSE
                         ! WRITE(6,*) 'debug 12'
                        CALL gf_iw_CALC_G( giw )
                     ENDIF
                     ! WRITE(6,*) 'debug 5'

                     !only diagonal elements of density-matrix are needed at this point
                     ! IF (rank_0) THEN
                     !    WRITE(*,*) ' calculate density ...'
                     ! ENDIF
                     CALL gf_iw_CALC_DENSITY_TAIL( giw, .TRUE. )

                     ! WRITE(6,*) 'debug 6'
                     !Trace out total number of electrons ...
                     Ne(i) = 0.
                     DO orbital=1,giw%n_orbitals
                        Ne(i) = Ne(i) + SUM(giw%density(orbital,orbital,:))
                     ENDDO
                     ! WRITE(6,*) 'debug 7'

                  ENDDO

                  !New intervall (keep already calculated values)
                  delta(1:3) = Ne(1:3)-giw%Ne
                  !dmu(3) = mu(3)-mu(2)
                  !dmu(1) = mu(2)-mu(1)
                  delta(4) = mu(2)-mu(4)
                  mu(4) = mu(2) !old mu

                  ! WRITE(6,*) 'debug 8'
                  IF ( ( delta(1)*delta(2) ) .GT. 0.0 ) THEN
                     mu(1) = mu(2)
                     Ne(1) = Ne(2)
                     delta(1) = delta(2)
                  ELSE
                     mu(3) = mu(2)
                     Ne(3) = Ne(2)
                     delta(3) = delta(2)
                  ENDIF
                  !mu(4) = mu(2)    !old mu
                  ! WRITE(6,*) 'debug 9'

                  IF ( is_metallic ) THEN
                     mu(2) = ( ABS(delta(3))*mu(1)+ABS(delta(1))*mu(3) )/( ABS(delta(3))+ABS(delta(1)) )   !predicted mu, metallic
                  ELSE
                     !mu(2) = (mu(3)+mu(1))/2  !new mu (semiconductor, insulators)
                     mu(2) = ( (mu(3)+mu(1))/2 + ( ABS(delta(3))*mu(1)+ABS(delta(1))*mu(3) )/( ABS(delta(3))+ABS(delta(1)) ) ) / 2
                  ENDIF

                  ! WRITE(6,*) 'debug 10'

                  IF ( rank_0 ) THEN

                     WRITE(6,*)
                     WRITE(6,'(A1,A1,A1,3X,"~ Interval: ",I3,",  N: ",F20.16," e-,  µ: ",F20.16," eV")', ADVANCE='YES') '+', '+', &
                                    CHAR(13),iteration, Ne(2), mu(4)

                  ENDIF

                  !timesum = timesum + (MPI_WTIME()-mtime)

               ENDDO

               IF ( rank_0 ) THEN
                  WRITE(6,*) 'INFO: executing `gf_iw_SET_MU`'
               ENDIF
               CALL gf_iw_SET_MU( giw, mu(4) )   !maybe mu(2) is incorrect ?

               IF ( rank_0 ) THEN
                  WRITE(6,*) 'INFO: executing `gf_iw_CALC_G`'
               ENDIF
               CALL gf_iw_CALC_G( giw )

               IF ( rank_0 ) THEN
                  WRITE(6,*) 'INFO: executing `gf_iw_CALC_DENSITY_TAIL`'
               ENDIF
               CALL gf_iw_CALC_DENSITY_TAIL( giw, only_diag )

               timesum = MPI_WTIME()-timesum

               IF ( rank_0 ) THEN
                  !CLOSE(6)
                  WRITE(6,*)
                  WRITE(6,'(3X,"Total elapsed time, TAIL:",1X,F8.2,1X,"sec.")') timesum
                  WRITE(6,*) '-'
                  !WRITE(6,'(3X,"Interval: ",I3,", N(e-): ",F21.15,", µ: ",F19.15" eV")') iteration, Ne(2), mu(2)
               ENDIF
            ELSE

               IF ( rank_0 ) THEN
                  WRITE(*,*) 'ABORT'
               ENDIF

               CALL ABORT
!               iteration = iteration + 1

!               IF ( iteration .EQ. 1 ) THEN
!                  i_start = 1
!                  i_end = 3
!               ELSE
!                  i_start = 2
!                  i_end = 2
!               ENDIF

!               !nested intervalls loop
!               DO i=i_start,i_end

!                  CALL gf_iw_SET_MU( giw, mu(i) )

!                  IF ( giw%has_EIGEN ) THEN
!                     CALL gf_iw_CALC_G_EIGEN( giw )
!                  ELSE
!                     CALL gf_iw_CALC_G( giw )
!                  ENDIF

!                  !only diagonal elements of density-matrix are needed at this point
!                  CALL gf_iw_CALC_DENSITY_TAIL( giw, .TRUE. )

!                  !Trace out total number of electrons ...
!                  Ne(i) = 0.
!                  DO orbital=1,giw%n_orbitals
!                     Ne(i) = Ne(i) + SUM(giw%density(orbital,orbital,:))
!                  ENDDO

!               ENDDO

!               !New intervall (keep already calculated values)
!               delta(1:3) = Ne(1:3)-giw%Ne
!               !dmu(3) = mu(3)-mu(2)
!               !dmu(1) = mu(2)-mu(1)
!               delta(4) = mu(2)-mu(4)
!               mu(4) = mu(2)   !old mu

!               !mu_nr = mu(4) - delta(2) / (0.5*( ((delta(3)-delta(2))/dmu(3))+((delta(2)-delta(1))/dmu(1)) )) !newton-raphson

!               IF ( ( delta(1)*delta(2) ) .GT. 0.0 ) THEN
!                  mu(1) = mu(2)
!                  Ne(1) = Ne(2)
!                  delta(1) = delta(2)
!               ELSE
!                  mu(3) = mu(2)
!                  Ne(3) = Ne(2)
!                  delta(3) = delta(2)
!               ENDIF
!               !mu(4) = mu(2)  !old mu

!               IF ( is_metallic ) THEN
!                  mu(2) = mu(2)
!               ELSE
!                  mu(2) = mu(2)
!               ENDIF


!               IF ( rank_0 ) THEN
!                  WRITE(6,'(A1,A1,3X,"Interval: ",I3,",  N: ",F20.16," e-,  µ: ",F20.16," eV")') '+', CHAR(13),iteration, Ne(2), mu(4)
!               ENDIF

!               ! IF ( rank_0 ) THEN
!               !    CLOSE(6)
!               ! ENDIF

!               !recalculate full density-matrix in non-diagonal basis
!               IF ( rank_0 ) THEN
!                   WRITE(6,*) 'INFO: executing `gf_iw_SET_MU`'
!               ENDIF

!               CALL gf_iw_SET_MU( giw, mu(2) )   !maybe mu(2) is incorrect ?
!               IF ( rank_0 ) THEN
!                   WRITE(6,*) 'INFO: executing `gf_iw_CALC_G`'
!               ENDIF
!               CALL gf_iw_CALC_G( giw )

!               !calculate only diagonal elements of density
!               !CALL gf_iw_CALC_DENSITY_TAIL( giw, .FALSE. )
!               IF ( rank_0 ) THEN
!                   WRITE(6,*) 'INFO: executing `gf_iw_CALC_DENSITY_TAIL`'
!               ENDIF
!               CALL gf_iw_CALC_DENSITY_TAIL( giw, only_diag )

!               timesum = MPI_WTIME()-timesum

!               IF ( rank_0 ) THEN
!                  !CLOSE(6)
!                  WRITE(6,*)
!                  WRITE(6,'(3X,"Total elapsed time, TAIL:",1X,F8.2,1X,"sec.")') timesum
!                  WRITE(6,*) '-'
!                  !WRITE(6,'(3X,"Interval: ",I3,", N(e-): ",F21.15,", µ: ",F19.15" eV")') iteration, Ne(2), mu(2)
!               ENDIF
            ENDIF
!-----------------------------------------------------------------------------------------------------------------------------------


            giw%is_CONSISTENT = .TRUE.

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_ADJUST_MU()" already called.'
            ENDIF
         ENDIF
      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ADJUST_MU()" cannot be called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_ADJUST_MU


   SUBROUTINE gf_iw_ADJUST_MU_T0( giw, mu_int, is_metallic, only_diag, set_mu_dft )

      USE timings
      !USE ifport

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      REAL, DIMENSION(3), INTENT(IN) :: mu_int
      LOGICAL, INTENT(IN) :: is_metallic
      LOGICAL, INTENT(IN) :: only_diag
      LOGICAL, INTENT(IN) :: set_mu_dft

      REAL, DIMENSION(giw%n_kpoints*giw%n_spins*giw%n_orbitals) :: EW_list
      REAL, DIMENSION(giw%n_kpoints*giw%n_spins*giw%n_orbitals) :: EW_list_sort

      REAL :: mu
      REAL :: mu_nr, tmp

      REAL :: s

      LOGICAL :: mflag

      INTEGER :: spin, kpoint, orbital                  !loop indices
      INTEGER :: ioerror

      !=============================================================================================================================

      IF ( giw%has_MESH .AND. giw%has_H0 .AND. giw%has_SIGMA .AND. giw%has_NE ) THEN


         IF ( .NOT. giw%is_CONSISTENT ) THEN

            IF ( rank_0 ) THEN
               WRITE(6,*) '  Calculating chemical potential µ ...'
               WRITE(6,'(3X,"Temperature T:",1X,F4.1,1X,"K")'), 0.0
               WRITE(6,'(3X,"Total charge N:",1X,F20.16,1X,"e-")'), giw%Ne
               !OPEN(UNIT=6, CARRIAGECONTROL='FORTRAN')
               OPEN(UNIT=6)
               WRITE(6,*) ''
            ENDIF

            EW_list = reshape( giw%EW, (/ giw%n_kpoints*giw%n_spins*giw%n_orbitals /) )
            
            CALL Sort(EW_list, giw%n_kpoints*giw%n_spins*giw%n_orbitals)

            mu = EW_list(giw%n_kpoints*INT(giw%Ne))


            CALL gf_iw_SET_MU( giw, mu )
            IF ( rank_0 ) THEN
               WRITE(6,'(3X,"New chemical potential:",1X,F19.16,1X,"eV")'), mu
            ENDIF
            CALL gf_iw_CALC_DENSITY_T0( giw, only_diag, .FALSE. )


            giw%is_CONSISTENT = .TRUE.

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_ADJUST_MU()" already called.'
            ENDIF
         ENDIF
      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ADJUST_MU()" cannot be called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_ADJUST_MU_T0

   !===============================================================================================================================

   FUNCTION gf_iw_RETURN_MU( giw )

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      TYPE(gf_iw), INTENT(IN) :: giw
      REAL :: gf_iw_RETURN_MU

      !-----------------------------------------------------------------------------------------------------------------------------

      gf_iw_RETURN_MU = giw%mu

   END FUNCTION gf_iw_RETURN_MU

   !===============================================================================================================================

   SUBROUTINE gf_iw_EXPORT_SIGMA( giw, sym_spin )
      !
      !  Purpose:
      !    Write the self-energy to file "SIGMA.DAT", call is MPI safe
      !  Record of revisions:
      !      Date       Programmer    Description of change
      !      ====       ==========    =====================
      !    28/08/15     S. Barthel    Original code
      !

      IMPLICIT NONE

      !TODO: ensure all data we write is present

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      LOGICAL, INTENT(IN) :: sym_spin       !enable paramagnetic symmetrization of export data

      INTEGER :: ioerror
      INTEGER :: iw, spin, orbital_, orbital

      COMPLEX, DIMENSION(giw%n_orbitals,giw%n_orbitals,giw%n_spins,giw%n_iw) :: Sigma

      IF ( rank_0 ) THEN

         IF ( giw%is_CONSISTENT ) THEN

            !copy
            Sigma = giw%Sigma

            !paramagnetic calculation (symmetrization wrt. spin)
            IF ( sym_spin ) THEN
               Sigma(:,:,1,:) = SUM(Sigma,3)/2
               Sigma(:,:,2,:) = Sigma(:,:,1,:)
            ENDIF

            !export self-energy to interface
            OPEN (UNIT=26,FILE='./SIGMA.DAT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)

            !header
            WRITE(26,*) giw%beta
            WRITE(26,*) giw%n_iw
            WRITE(26,*) giw%n_spins
            WRITE(26,*) giw%n_kpoints
            WRITE(26,*) giw%n_orbitals
            WRITE(26,*) giw%Ne
            WRITE(26,*) giw%mu
            WRITE(26,*) giw%fit_start

            !data
            DO iw=1,giw%n_iw
               DO spin=1,giw%n_spins
                  DO orbital_=1,giw%n_orbitals
                     DO orbital=1,giw%n_orbitals
                        WRITE(26,*) orbital, orbital_, spin, iw
                        WRITE(26,*) REAL(Sigma(orbital, orbital_, spin, iw)), AIMAG(Sigma(orbital, orbital_, spin, iw))
                     ENDDO
                  ENDDO
               ENDDO
            ENDDO

            CLOSE(26)

         ELSE

            WRITE(6,*) 'STOP: "gf_iw_ADJUST_MU()" needs to be called prior to "gf_iw_EXPORT_SIGMA()'
            CALL MPI_Barrier(MPI_COMM_WORLD, i_error)
            STOP

         ENDIF

      ENDIF

      CALL MPI_Barrier(MPI_COMM_WORLD, i_error)

   END SUBROUTINE gf_iw_EXPORT_SIGMA

   !===============================================================================================================================

   !SUBROUTINE gf_iw_IMPORT_SIGMA( giw )    !needs more code to be useful ...
   !  !
   !  !  Purpose:
   !  !    Read the self-energy file "SIGMA.DAT", call is MPI safe
   !  !  Record of revisions:
   !  !      Date       Programmer    Description of change
   !  !      ====       ==========    =====================
   !  !    28/08/15     S. Barthel    Original code
   !  !

   !  !Note: This routine should not be a class member ... but it will lower code maintenance effort, maybe we just move ot to
   !  !Interface

   !  IMPLICIT NONE

   !  ! Data dictionary: declare constants

   !  ! Data dictionary: declare variable types, definitions, units

   !  TYPE(gf_iw), INTENT(INOUT) :: giw

   !  INTEGER :: allocerror, ioerror
   !  INTEGER :: iw, spin, orbital_, orbital    !loop indices

   !  !-----------------------------------------------------------------------------------------------------------------------------
   !  !Shared data block of top-level code
   !  !-----------------------------------------------------------------------------------------------------------------------------

   !  !quantities read from file
   !  REAL :: beta              !inverse temperature
   !  INTEGER :: n_iw               !number of matsubara frequencies
   !  INTEGER :: n_spins
   !  INTEGER :: n_kpoints
   !  INTEGER :: n_orbitals
   !  REAL :: Ne                !total number of electrons
   !  REAL :: mu                !chemical potential
   !  INTEGER :: fit_start

   !  COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: Sigma

   !  !-----------------------------------------------------------------------------------------------------------------------------

   !  INTEGER :: iw_read, spin_read, orbital__read, orbital_read
   !  REAL :: sigma_real_read, sigma_imag_read

   !  !-----------------------------------------------------------------------------------------------------------------------------

   !  !header
   !  IF ( rank_0 ) THEN

   !WRITE(6,*) 'rank 0: Reading header of SIGMA.DAT ...'

   !!export self-energy to interface
   !OPEN (UNIT=26,FILE='./SIGMA.DAT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)

   !!header
   !READ(26,*) beta
   !READ(26,*) n_iw
   !READ(26,*) n_spins
   !READ(26,*) n_kpoints
   !READ(26,*) n_orbitals
   !READ(26,*) Ne
   !READ(26,*) mu
   !READ(26,*) fit_start

   !!note: these variables should initialize the shared data ...

   !  ENDIF

   !  !broadcast header
   !  srcnt = 1
   !  CALL MPI_BCAST(beta,       srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)
   !  CALL MPI_BCAST(n_iw,       srcnt, MPI_INTEGER,        0, MPI_COMM_WORLD, i_error)
   !  CALL MPI_BCAST(n_spins,    srcnt, MPI_INTEGER,        0, MPI_COMM_WORLD, i_error)
   !  CALL MPI_BCAST(n_kpoints,  srcnt, MPI_INTEGER,        0, MPI_COMM_WORLD, i_error)
   !  CALL MPI_BCAST(n_orbitals, srcnt, MPI_INTEGER,        0, MPI_COMM_WORLD, i_error)
   !  CALL MPI_BCAST(Ne,         srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)
   !  CALL MPI_BCAST(mu,         srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)
   !  CALL MPI_BCAST(fit_start,      srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)

   !  !initialization calls
   !  CALL gf_iw_ALLOCATE( giw, n_iw, n_spins, n_kpoints, n_orbitals )
   !  CALL gf_iw_SET_MESH( giw, beta, fit_start )

   !  !data
   !  ALLOCATE ( Sigma( n_orbitals, n_orbitals, n_spins, n_iw ), STAT=allocerror )
   !  IF ( allocerror > 0 ) THEN
   !WRITE(6,*) 'ERROR: Memory for "Sigma" could not be allocated'
   !CALL MPI_FINALIZE(i_error)
   !STOP
   !  ELSE

   !IF ( rank_0 ) THEN

   !  WRITE(6,*) 'rank 0: Reading data of SIGMA.DAT ...'

   !  !data
   !  DO iw=1,n_iw
   !    DO spin=1,n_spins
   !      DO orbital_=1,n_orbitals
   !    DO orbital=1,n_orbitals
   !      READ(26,*) orbital_read, orbital__read, spin_read, iw_read
   !      READ(26,*) sigma_real_read, sigma_imag_read
   !      Sigma(orbital_read, orbital__read, spin_read, iw_read) = CMPLX(sigma_real_read, sigma_imag_read)
   !    ENDDO
   !      ENDDO
   !    ENDDO
   !  ENDDO

   !  CLOSE(26)

   !ENDIF

   !!broadcast sigma
   !srcnt = giw%n_orbitals*giw%n_orbitals*giw%n_spins*giw%n_iw
   !CALL MPI_BCAST(Sigma, srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)

   !  ENDIF

   !  !initialization call for sigma
   !  CALL gf_iw_SET_SIGMA( giw, Sigma )

   !  DEALLOCATE ( Sigma, STAT=allocerror )
   !  IF ( allocerror > 0 ) THEN
   !WRITE(6,*) 'ERROR: Memory for "Sigma" could not be deallocated'
   !CALL MPI_FINALIZE(i_error)
   !STOP
   !  ENDIF

   !END SUBROUTINE gf_iw_IMPORT_SIGMA

   !===============================================================================================================================

   FUNCTION gf_iw_IS_IDENTICAL( giw_1, giw_2 )
      !
      !  Purpose:
      !    Compare two Green's functions for allocation, identical dimensions & meshes and return True/False
      !  Record of revisions:
      !      Date       Programmer    Description of change
      !      ====       ==========    =====================
      !    04/09/15     S. Barthel    Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      TYPE(gf_iw), INTENT(IN) :: giw_1, giw_2

      LOGICAL :: gf_iw_IS_IDENTICAL

      !-----------------------------------------------------------------------------------------------------------------------------

      gf_iw_IS_IDENTICAL = .FALSE.

      IF ( giw_1%is_ALLOCATED .AND. giw_2%is_ALLOCATED ) THEN

         IF ( giw_1%n_iw .EQ. giw_2%n_iw ) THEN
            IF ( giw_1%n_spins .EQ. giw_2%n_spins) THEN
               IF ( giw_1%n_kpoints .EQ. giw_2%n_kpoints ) THEN
                  IF ( giw_1%n_orbitals .EQ. giw_2%n_orbitals ) THEN

                     IF ( giw_1%beta .EQ. giw_2%beta ) THEN
                        !IF ( giw_1%w_max.EQ. giw_2%w_max) THEN

                        gf_iw_IS_IDENTICAL = .TRUE.

                        !ENDIF
                     ENDIF

                  ENDIF
               ENDIF
            ENDIF
         ENDIF

      ENDIF

   END FUNCTION gf_iw_IS_IDENTICAL

   !===============================================================================================================================

   SUBROUTINE gf_iw_ZERO_G( giw )
      !
      !  Purpose:
      !    Reset G(z) to a clean initialization state (is_ALLOCATED, has_MESH)
      !
      !  Record of revisions:
      !      Date       Programmer    Description of change
      !      ====       ==========    =====================
      !    03/09/15     S. Barthel    Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      !=============================================================================================================================

      IF ( giw%is_ALLOCATED .AND. giw%has_MESH ) THEN

         giw%has_H0 = .FALSE.
         giw%H0 = CMPLX(0.,0.)

         giw%has_SIGMA = .FALSE.
         giw%Sigma = CMPLX(0.,0.)

         giw%has_NE = .FALSE.
         giw%Ne = 0.

         giw%has_MU = .FALSE.
         giw%mu = 0.

         giw%has_DATA = .FALSE.
         giw%data = CMPLX(0.,0.)

         giw%has_DENSITY = .FALSE.
         giw%density = CMPLX(0.,0.)

         giw%is_CONSISTENT = .FALSE.

         giw%has_EIGEN = .FALSE.
         !giw%EW = CMPLX(0.,0.)
         giw%EW = 0.

         giw%has_GTAIL = .FALSE.
         giw%gtail = CMPLX(0.,0.)

         giw%has_STAIL = .FALSE.
         giw%stail = CMPLX(0.,0.)

         !-optional- not meant to be changed by member functions, should be defaults anyways
!   giw%use_gtail_fit = .FALSE.
!   giw%gtail_order_fit = 6
!   giw%gtail_order_sum = 3
!   giw%stail_order = 1

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ZERO_G" cannot be called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_ZERO_G

   !===============================================================================================================================

   SUBROUTINE gf_iw_G_MINUS_G( giw_1, giw_2, giw_3 )
      !
      !  Purpose:
      !    Subtract two Greens-functions: G3(z) = G1(z)-G2(z)
      !    The new data & tail is computed automatically
      !
      !  Record of revisions:
      !      Date       Programmer    Description of change
      !      ====       ==========    =====================
      !    03/09/15     S. Barthel    Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw_1, giw_2, giw_3

      !=============================================================================================================================

      !IF ( gf_iw_IS_IDENTICAL(giw_1,giw_2) .AND. (.NOT.giw_3%is_ALLOCATED) ) THEN

      IF ( gf_iw_IS_IDENTICAL(giw_1,giw_2) .AND. gf_iw_IS_IDENTICAL(giw_1,giw_3) ) THEN

         !clean giw_3
         CALL gf_iw_ZERO_G( giw_3 )

         !subtract data
         giw_3%data = (giw_1%data - giw_2%data)
         giw_3%has_DATA = .TRUE.

         !subtract tails
         giw_3%gtail = (giw_1%gtail - giw_2%gtail)
         giw_3%has_GTAIL = .TRUE.

         !calculate density / matsubara sum
         !CALL gf_iw_CALC_DENSITY_TAIL( giw_3, .FALSE. )

         !TODO: prevent user from messing with giw_3 ( set HO, Sigma, calc_G etc.), lets hope for the moment the user is smart ...
         !giw_3%LOCKED = .TRUE.

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_G_MINUS_G()" cannot be called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_G_MINUS_G

   !===============================================================================================================================

   SUBROUTINE gf_iw_G_TIMES_G( giw_1, giw_2, giw_3 )
      !
      !  Purpose:
      !    Multiply two Greens-functions: G3(z) = G1(z) * G2(z)
      !    The new data & tail is computed automatically, fitting is not implemented
      !
      !  Record of revisions:
      !      Date       Programmer    Description of change
      !      ====       ==========    =====================
      !    21/09/15     S. Barthel    Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw_1, giw_2, giw_3

      INTEGER :: iw, spin, orbital_, orbital, order_, order

      !=============================================================================================================================

      IF ( gf_iw_IS_IDENTICAL(giw_1,giw_2) .AND. gf_iw_IS_IDENTICAL(giw_1,giw_3) ) THEN

         !clean giw_3
         CALL gf_iw_ZERO_G( giw_3 )

         !matrix-matrix for data
         DO iw=1,giw_3%n_iw
            DO spin=1,giw_3%n_spins
               DO orbital_=1,giw_3%n_orbitals
                  DO orbital=1,giw_3%n_orbitals
                     giw_3%data(orbital,orbital_,spin,iw) = SUM(giw_1%data(orbital,:,spin,iw)*giw_2%data(:,orbital_,spin,iw))
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
         giw_3%has_DATA = .TRUE.

         !loop over tail orders including matrix-matrix multiplication
         DO order_=1,giw_1%gtail_order_sum
            DO order=1,giw_2%gtail_order_sum

               IF ( (order+order_) <= giw_3%gtail_order_sum ) THEN

                  DO spin=1,giw_3%n_spins
                     DO orbital_=1,giw_3%n_orbitals
                        DO orbital=1,giw_3%n_orbitals

                           !this should be okay ...
                           giw_3%gtail(orbital, orbital_, spin, order+order_) = giw_3%gtail(orbital, orbital_, spin, order+order_)+&
                              SUM( giw_1%gtail(orbital, :, spin, order) * giw_2%gtail(:, orbital_, spin, order_) )

                        ENDDO
                     ENDDO
                  ENDDO

               ENDIF

            ENDDO
         ENDDO
         giw_3%has_GTAIL = .TRUE.

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_G_TIMES_G()" cannot be called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_G_TIMES_G

   !===============================================================================================================================

   FUNCTION gf_iw_CALC_Epot( giw )
      !
      !  Purpose:
      !    Calculate potential energy arising from the beyond-DFT interactions
      !    Implementation following PRB 90, 235103 (2014) H.Park, A. J.Millis and C.A. Marianetti, Equation (34) only
      !    Double-counting potential has to be included in self-energy
      !
      !  Record of revisions:
      !      Date       Programmer    Description of change
      !      ====       ==========    =====================
      !    21/09/15     S. Barthel    Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      TYPE(gf_iw) :: s_tmp, g_tmp

      REAL, DIMENSION(2) :: tmp
      REAL :: gf_iw_CALC_Epot

      !=============================================================================================================================
      IF ( rank_0 ) THEN
         WRITE(6,*) giw%has_DATA
         WRITE(6,*) giw%has_GTAIL
      ENDIF
      IF ( giw%has_DATA .AND. giw%has_GTAIL ) THEN

         !test tail orders
         IF ( giw%stail_order > giw%gtail_order_fit ) THEN
            IF ( rank_0 ) THEN
               WRITE(6,*) 'STOP: "giw%stail_order > giw%gtail_order_fit"'
            ENDIF
            CALL MPI_FINALIZE(i_error)
            STOP
         ENDIF

         !create temporary GF taking the role of the self energy
         CALL gf_iw_ALLOCATE( s_tmp, giw%n_iw, giw%n_spins, giw%n_kpoints, giw%n_orbitals)
         CALL gf_iw_SET_MESH( s_tmp, giw%beta, giw%fit_start )
         CALL gf_iw_ZERO_G( s_tmp )  !maybe not needed

         !copy self-energy to GF
         s_tmp%data = giw%Sigma
         s_tmp%has_DATA = .TRUE.

         s_tmp%gtail(:,:,:,0:giw%stail_order) = giw%stail    !this is why we need the tail orders test
         s_tmp%has_GTAIL = .TRUE.

         !create temporary GF holding the multiplication result
         CALL gf_iw_ALLOCATE( g_tmp, giw%n_iw, giw%n_spins, giw%n_kpoints, giw%n_orbitals)
         CALL gf_iw_SET_MESH( g_tmp, giw%beta, giw%fit_start )

         !***************************************************************************************************************************
         !Note: Because the self-energy is block-diagonal and nonzero only in the correlated subspace,
         !      we can work with Σ(iw) and G(iw) in the full localized subspace numerically
         !***************************************************************************************************************************

         !Equation (34), PRB 90, 235103 (2014) H.Park, A. J.Millis and C.A. Marianetti
         CALL gf_iw_G_TIMES_G( s_tmp, giw, g_tmp )               !Σ(iw)*G(iw)
         CALL gf_iw_CALC_DENSITY_TAIL_serial( g_tmp, .TRUE. )            !Matsubara sum (diagonal elements only)
         tmp = gf_iw_RETURN_DENSITY_TRACE( g_tmp, giw%n_orbitals )       !full trace
         gf_iw_CALC_Epot = SUM(tmp) / 2                      !sum over spin and prefactor

         !deallocation
         CALL gf_iw_DEALLOCATE( s_tmp )
         CALL gf_iw_DEALLOCATE( g_tmp )

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'INFO: "gf_iw_CALC_G()" needs to be called prior to "gf_iw_CALC_Epot()" '
         ENDIF
      ENDIF

   END FUNCTION gf_iw_CALC_Epot

   FUNCTION gf_iw_CALC_Ekin_T0( giw )
      !
      !  Purpose:
      !    Calculate kinetic energy arising
      !
      !  Record of revisions:
      !      Date       Programmer    Description of change
      !      ====       ==========    =====================
      !    21/09/15     S. Barthel    Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      REAL :: E_kin, gf_iw_CALC_Ekin_T0

      INTEGER :: kpoint, spin, orbital

      E_kin = 0.0
      DO kpoint=1,giw%n_kpoints
         DO spin=1,giw%n_spins
            DO orbital=1,giw%n_orbitals
               E_kin = E_kin + giw%density_k_eig(orbital,spin,kpoint) * giw%EW0(orbital,spin,kpoint) / float(giw%n_kpoints)
            ENDDO
         ENDDO
      ENDDO
      gf_iw_CALC_Ekin_T0 = E_kin

   END FUNCTION gf_iw_CALC_Ekin_T0

   FUNCTION gf_iw_CALC_Epot_T0( giw )
      !
      !  Purpose:
      !    Calculate potential energy arising from the beyond-DFT interactions
      !
      !  Record of revisions:
      !      Date       Programmer    Description of change
      !      ====       ==========    =====================
      !    21/09/15     S. Barthel    Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw

      REAL :: E_pot, gf_iw_CALC_Epot_T0

      INTEGER :: kpoint, spin, orbital, orbital_prime

      E_pot = 0.0
      DO spin=1,giw%n_spins
         DO orbital=1,giw%n_orbitals
            DO orbital_prime=1,giw%n_orbitals
               E_pot = E_pot + giw%density(orbital,orbital_prime,spin) *giw%Sigma(orbital_prime,orbital,spin,1)
            ENDDO
         ENDDO
      ENDDO
      gf_iw_CALC_Epot_T0 = 0.5*E_pot

   END FUNCTION gf_iw_CALC_Epot_T0


   !===============================================================================================================================

   SUBROUTINE gf_iw_CALC_EXPORT_DOS_HF( giw, label, idelta)
      !
      !  Purpose:
      !    Calculate density of states for Hartree-Fock solution using real-energy G(E), MPI version
      !
      !  Record of revisions:
      !      Date       Programmer    Description of change
      !      ====       ==========    =====================
      !    23/09/15     S. Barthel    Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      CHARACTER(4), INTENT(IN) :: label
      REAL, INTENT(IN) :: idelta

      INTEGER :: spin, kpoint, iw, orbital

      COMPLEX, DIMENSION(giw%n_orbitals, giw%n_orbitals) :: ev
      REAL, DIMENSION(giw%n_orbitals) :: ew

      INTEGER, PARAMETER :: n_E = 10001

      REAL :: Emin, Emax, dE

      REAL, DIMENSION(n_E, giw%n_orbitals,giw%n_spins) :: orbital_dos

      INTEGER :: E                              !loop indices
      REAL, DIMENSION(n_E) :: rE                        !real energy axis
      COMPLEX, DIMENSION(n_E) :: zE                     !complex energy axis

      INTEGER :: allocerror, ioerror                        !Error code for allocation
      INTEGER :: dim_work_opt

      !=============================================================================================================================

      IF ( giw%is_ALLOCATED ) THEN
         IF ( giw%has_H0 .AND. giw%has_SIGMA ) THEN

            Emin = -20.
            Emax = 10.

            dE = (Emax-Emin) / (n_E-1)
            DO E=1,n_E
               rE(E) = dE*(E-1)+Emin
               zE(E) = CMPLX(rE(E),idelta)
            ENDDO

            orbital_dos = 0.

            !MPI: superindex loop over kpoints and spins
            CALL parallel_range(1, giw%n_kpoints*giw%n_spins, nprocs, myrank, ista, iend)
            DO ilin=ista,iend

               CALL parallel_lin2sub_double(ilin, giw%n_kpoints, giw%n_spins, kpoint, spin)

               ev = giw%H0(:,:,kpoint) + giw%Sigma(:,:,spin,1) !no frequency dependence, thus just use first frequency
               ew = 0.

               ev = giw%H0(:,:,kpoint) + giw%Sigma(:,:,spin,1) !no frequency dependence, thus just use first frequency
               ew = 0.

               dim_work_opt = -1   !optimal work array size
               CALL hermitian_matrix_diagonalization(giw%n_orbitals, ev, ew, dim_work_opt)
               CALL hermitian_matrix_diagonalization(giw%n_orbitals, ev, ew, dim_work_opt)     !we get <a|n,k> ?

               DO orbital=1,giw%n_orbitals
                  DO E=1,n_E
                     orbital_dos(E,orbital,spin) = orbital_dos(E,orbital,spin) &
                        -giw%k_weight(kpoint)*AIMAG( SUM( ( ev(orbital,:)*CONJG(ev(orbital,:)) ) / (zE(E)+giw%mu-ew) ) )/PI   ! sum_n <a|n,k><n,k|a>
                  ENDDO
               ENDDO

            ENDDO

            !MPI
            srcnt = n_E*giw%n_orbitals*giw%n_spins
            CALL MPI_ALLREDUCE(MPI_IN_PLACE, orbital_dos, srcnt, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD, i_error)

            !orbital_dos = orbital_dos / giw%n_kpoints    !normalize wrt. k-points

            orbital_dos = orbital_dos / SUM( giw%k_weight )

            !write data
            IF (rank_0) THEN

               OPEN (UNIT=26,FILE='LDOS_HF_'// TRIM(ADJUSTL(label)) //'_U.dat', STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
               DO E=1,n_E
                  WRITE(26,'(1X,F19.15)',ADVANCE='NO') rE(E)
                  DO orbital=1,giw%n_orbitals
                     IF ( orbital /= giw%n_orbitals ) THEN
                        WRITE(26,'(1X,F19.15)',ADVANCE='NO') orbital_dos(E,orbital,1)
                     ELSE
                        WRITE(26,'(1X,F19.15)') orbital_dos(E,orbital,1)
                     ENDIF
                  ENDDO
               ENDDO
               CLOSE(26)

               OPEN (UNIT=26,FILE='LDOS_HF_'// TRIM(ADJUSTL(label)) //'_D.dat', STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
               DO E=1,n_E
                  WRITE(26,'(1X,F19.15)',ADVANCE='NO') rE(E)
                  DO orbital=1,giw%n_orbitals
                     IF ( orbital /= giw%n_orbitals ) THEN
                        WRITE(26,'(1X,F19.15)',ADVANCE='NO') orbital_dos(E,orbital,2)
                     ELSE
                        WRITE(26,'(1X,F19.15)') orbital_dos(E,orbital,2)
                     ENDIF
                  ENDDO
               ENDDO
               CLOSE(26)

            ENDIF

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_SET_H0()" and "gf_iw_SET_SIGMA" need to be called prior to "gf_iw_CALC_EIGEN()" '
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_CALC_EXPORT_DOS_HF

   SUBROUTINE gf_iw_CALC_EXPORT_DOS_HF_T0( giw, label, idelta,n_E,Emin,Emax)
      !
      !  Purpose:
      !    Calculate density of states for Hartree-Fock solution using real-energy G(E), MPI version
      !
      !  Record of revisions:
      !      Date       Programmer    Description of change
      !      ====       ==========    =====================
      !    23/09/15     S. Barthel    Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE(gf_iw), INTENT(INOUT) :: giw
      CHARACTER(4), INTENT(IN) :: label
      REAL, INTENT(IN) :: idelta, Emin, Emax
      INTEGER, INTENT(IN) :: n_E

      INTEGER :: spin, kpoint, iw, orbital

      COMPLEX, DIMENSION(giw%n_orbitals, giw%n_orbitals) :: ev
      REAL, DIMENSION(giw%n_orbitals) :: ew

      REAL :: dE

      REAL, DIMENSION(n_E, giw%n_orbitals,giw%n_spins) :: orbital_dos

      INTEGER :: E                              !loop indices
      REAL, DIMENSION(n_E) :: rE                        !real energy axis
      COMPLEX, DIMENSION(n_E) :: zE                     !complex energy axis

      INTEGER :: allocerror, ioerror                        !Error code for allocation
      INTEGER :: dim_work_opt

      !=============================================================================================================================

      IF ( giw%is_ALLOCATED ) THEN
         IF ( giw%has_H0 .AND. giw%has_SIGMA ) THEN


            dE = (Emax-Emin) / (n_E-1)
            DO E=1,n_E
               rE(E) = dE*(E-1)+Emin
               zE(E) = CMPLX(rE(E),idelta)
            ENDDO

            orbital_dos = 0.

            !MPI: superindex loop over kpoints and spins
            CALL parallel_range(1, giw%n_kpoints*giw%n_spins, nprocs, myrank, ista, iend)
            DO ilin=ista,iend

               CALL parallel_lin2sub_double(ilin, giw%n_kpoints, giw%n_spins, kpoint, spin)

               !ev = giw%H0(:,:,kpoint) + giw%Sigma(:,:,spin,1)    !no frequency dependence, thus just use first frequency
               !ew = 0.

               ev = giw%EV(:,:,spin,kpoint)    !no frequency dependence, thus just use first frequency
               ew = giw%EW(:,spin,kpoint)

               !dim_work_opt = -1  !optimal work array size
               !CALL hermitian_matrix_diagonalization(giw%n_orbitals, ev, ew, dim_work_opt)
               !CALL hermitian_matrix_diagonalization(giw%n_orbitals, ev, ew, dim_work_opt)        !we get <a|n,k> ?

               DO orbital=1,giw%n_orbitals
                  DO E=1,n_E
                     orbital_dos(E,orbital,spin) = orbital_dos(E,orbital,spin) &
                        -giw%k_weight(kpoint)*AIMAG( SUM( ( ev(orbital,:)*CONJG(ev(orbital,:)) ) / (zE(E)+giw%mu-ew) ) )/PI   ! sum_n <a|n,k><n,k|a>
                  ENDDO
               ENDDO

            ENDDO

            !MPI
            srcnt = n_E*giw%n_orbitals*giw%n_spins
            CALL MPI_ALLREDUCE(MPI_IN_PLACE, orbital_dos, srcnt, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD, i_error)

            !orbital_dos = orbital_dos / giw%n_kpoints    !normalize wrt. k-points

            orbital_dos = orbital_dos / SUM( giw%k_weight )

            !write data
            IF (rank_0) THEN

               OPEN (UNIT=26,FILE='LDOS_HF_'// TRIM(ADJUSTL(label)) //'_U.dat', STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
               DO E=1,n_E
                  WRITE(26,'(1X,F19.15)',ADVANCE='NO') rE(E)
                  DO orbital=1,giw%n_orbitals
                     IF ( orbital /= giw%n_orbitals ) THEN
                        WRITE(26,'(1X,F19.15)',ADVANCE='NO') orbital_dos(E,orbital,1)
                     ELSE
                        WRITE(26,'(1X,F19.15)') orbital_dos(E,orbital,1)
                     ENDIF
                  ENDDO
               ENDDO
               CLOSE(26)

               OPEN (UNIT=26,FILE='LDOS_HF_'// TRIM(ADJUSTL(label)) //'_D.dat', STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
               DO E=1,n_E
                  WRITE(26,'(1X,F19.15)',ADVANCE='NO') rE(E)
                  DO orbital=1,giw%n_orbitals
                     IF ( orbital /= giw%n_orbitals ) THEN
                        WRITE(26,'(1X,F19.15)',ADVANCE='NO') orbital_dos(E,orbital,2)
                     ELSE
                        WRITE(26,'(1X,F19.15)') orbital_dos(E,orbital,2)
                     ENDIF
                  ENDDO
               ENDDO
               CLOSE(26)

            ENDIF

         ELSE
            IF ( rank_0 ) THEN
               WRITE(6,*) 'INFO: "gf_iw_SET_H0()" and "gf_iw_SET_SIGMA" need to be called prior to "gf_iw_CALC_EIGEN()" '
            ENDIF
         ENDIF

      ELSE
         IF ( rank_0 ) THEN
            WRITE(6,*) 'STOP: "gf_iw_ALLOCATE()" not called.'
         ENDIF
         CALL MPI_FINALIZE(i_error)
         STOP
      ENDIF

   END SUBROUTINE gf_iw_CALC_EXPORT_DOS_HF_T0

END MODULE greens_functions
