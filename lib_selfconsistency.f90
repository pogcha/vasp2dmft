MODULE selfconsistency
!
!  Purpose:
!    ...
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    07/12/15     S. Barthel	Original code
!

   USE greens_functions

   IMPLICIT NONE
!SAVE !we do not need to access this module more than once, variables will be save if only used from main.f90

!PRIVATE						!no access granted to member variables

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units

   COMPLEX, ALLOCATABLE, DIMENSION(:,:,:) :: H0_loc	!Hamiltonian <cf|H|cf´>(k) in static (w->infty) crystal field basis
   COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: S_loc	!Matsubara self-energy <cr|S|cr´>(iw)  in the full localized basis

   TYPE( gf_iw ) :: G0_loc			!Non-Interacting Matsubara Green's function <cr|G0|cr´>(iw) in the full localized basis
   TYPE( gf_iw ) :: G_loc			!Interacting Matsubara Green's function <cr|G|cr´>(iw) in the full localized basis

!dev k-resolved (single k-point)
   TYPE( gf_iw ) :: G0_k_loc		!Non-Interacting Matsubara Green's function <cr|G0|cr´>(iw,k) in the full localized basis
   TYPE( gf_iw ) :: G_k_loc		!Interacting Matsubara Green's function <cr|G|cr´>(iw,k) in the full localized basis

   TYPE( gf_iw ) :: delta_G_G0_k_loc

!-------------------------------------------------------------------------------------------------------------------------------

!quantities read from file SIGMA
   REAL :: file_beta			!inverse temperature
   INTEGER :: file_n_iw			!number of matsubara frequencies
   INTEGER :: file_n_spins
   INTEGER :: file_n_kpoints
   INTEGER :: file_n_sub_orbitals
   INTEGER :: file_n_orbitals
   REAL :: file_Ne				!total number of electrons
   REAL :: file_mu				!chemical potential
   INTEGER :: file_fit_start

!===============================================================================================================================

CONTAINS

   SUBROUTINE copy_from_memory_H0_loc( proj, outcar )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    31/08/15     S. Barthel	Original code
      !

      USE type_locproj
      USE type_file_outcar

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(IN) :: proj
      TYPE( file_outcar ), INTENT(IN) :: outcar

      INTEGER :: allocerror			!error codes for allocation
      INTEGER :: kpoint, kpoint_			!loop index

      !-------------------------------------------------------------------------------------------------------------------------------

      !Allocate Hamiltonian
      ALLOCATE ( H0_loc(file_n_orbitals,file_n_orbitals,file_n_kpoints), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "H0_loc" could not be allocated'
         STOP
      ELSE
         !H0_loc = SUM(proj%H,4) / proj%n_spins !hamiltonian <L|H|L'>(k,s)

         !H0_loc = SUM(proj%H(:,:,1:file_n_kpoints,:),4) / proj%n_spins !hamiltonian <L|H|L'>(k,s)

         !proj%H( proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins )

         DO kpoint=1,outcar%n_kpoints_ibz
            kpoint_ = outcar%idx_ibz_hf(kpoint)
            H0_loc(:,:,kpoint) = SUM(proj%H(:,:,kpoint_,:),3) / proj%n_spins !hamiltonian <L|H|L'>(k,s)
         ENDDO

      ENDIF

      CLOSE(26)
      IF ( myrank .EQ. 0 ) THEN
         WRITE(6,*) '  Hamiltonian <cr|H(k)|cr´> copied (paramagnetic) from memory.'
         WRITE(6,*) '  Using k-points in irreducible BZ.'
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE copy_from_memory_H0_loc

   !===============================================================================================================================

   SUBROUTINE import_SIGMA( proj, outcar )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    31/08/15     S. Barthel	Original code
      !

      USE type_locproj
      USE type_file_outcar

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(IN) :: proj
      TYPE( file_outcar ), INTENT(IN) :: outcar

      INTEGER :: ioerror, allocerror			!error codes for allocation and file IO

      INTEGER :: iw, spin, orbital_, orbital		!loop indices

      !temporary read in variables
      INTEGER :: iw_read, spin_read, orbital__read, orbital_read
      REAL :: S_loc_real_read, S_loc_imag_read
      S_loc_imag_read = 0.0
      S_loc_real_read = 0.0

      !-------------------------------------------------------------------------------------------------------------------------------

      !read header
      IF ( myrank .EQ. 0 ) THEN

         WRITE(6,*) '  rank 0: Reading header of SIGMA.DAT ...'

         !export self-energy to interface
         OPEN (UNIT=26,FILE='./SIGMA.DAT',STATUS='OLD', ACTION='READ', IOSTAT=ioerror)

         !check if file actually exists ...

         IF ( ioerror /= 0 ) THEN
            WRITE(6,*) 'ERROR: File "SIGMA.DAT" missing !'
            CALL MPI_FINALIZE(i_error)
            STOP
         ELSE

            !header
            READ(26,*) file_beta
            READ(26,*) file_n_iw
            READ(26,*) file_n_spins
            READ(26,*) file_n_kpoints		!compare
            READ(26,*) file_n_sub_orbitals		!compare
            READ(26,*) file_n_orbitals      !compare
            READ(26,*) file_Ne
            READ(26,*) file_mu
            READ(26,*) file_fit_start

            IF ( file_n_kpoints == outcar%n_kpoints_ibz ) THEN
               WRITE(6,*) 'INFO: "file_n_kpoints" matches "outcar%n_kpoints_ibz"'

            ELSEIF ( file_n_kpoints == outcar%n_kpoints_ibz_hf ) THEN
               WRITE(6,*) 'INFO: "file_n_kpoints" matches "outcar%n_kpoints_ibz_hf"'

               IF ( file_n_kpoints /= proj%n_kpoints ) THEN
                  WRITE(6,*) 'ERROR: "file_n_kpoints" differs from "proj%n_kpoints"'
                  CALL MPI_FINALIZE(i_error)
                  STOP
               ENDIF

            ELSE
               WRITE(6,*) file_n_kpoints, outcar%n_kpoints_ibz, outcar%n_kpoints_ibz_hf, proj%n_kpoints
               WRITE(6,*) 'ERROR: "file_n_kpoints" differs from proj% and outcar%"'
               CALL MPI_FINALIZE(i_error)
               STOP
            ENDIF

            !compare to actual projector data as a minimal check
            IF ( file_n_kpoints /= proj%n_kpoints ) THEN
               WRITE(6,*) 'ERROR: "file_n_kpoints" differs from "proj%n_kpoints"'
               !CALL MPI_FINALIZE(i_error)
               !STOP
            ELSEIF ( file_n_orbitals /= proj%n_bands ) THEN
               WRITE(6,*) 'ERROR: "file_n_orbitals" differs from "proj%n_bands"'
               CALL MPI_FINALIZE(i_error)
               STOP
            ELSE

               WRITE(6,*) '  file_beta: ', file_beta
               WRITE(6,*) '  file_n_iw: ', file_n_iw
               WRITE(6,*) '  file_n_spins: ', file_n_spins
               WRITE(6,*) '  file_n_kpoints: ', file_n_kpoints		!compared
               WRITE(6,*) '  file_n_sub_orbitals: ', file_n_sub_orbitals		!compared
               WRITE(6,*) '  file_n_orbitals: ', file_n_orbitals     !compared
               WRITE(6,*) '  file_Ne: ', file_Ne
               WRITE(6,*) '  file_mu: ', file_mu
               WRITE(6,*) '  file_fit_start: ', file_fit_start

            ENDIF

         ENDIF

      ENDIF

      !broadcast header
      srcnt = 1
      CALL MPI_BCAST(file_beta,       srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(file_n_iw,       srcnt, MPI_INTEGER,        0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(file_n_spins,    srcnt, MPI_INTEGER,        0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(file_n_kpoints,  srcnt, MPI_INTEGER,        0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(file_n_sub_orbitals, srcnt, MPI_INTEGER,        0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(file_n_orbitals, srcnt, MPI_INTEGER,  0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(file_Ne,         srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(file_mu,         srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(file_fit_start,      srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      !-------------------------------------------------------------------------------------------------------------------------------

      !allocate and read data
      ALLOCATE ( S_loc( file_n_orbitals, file_n_orbitals, file_n_spins, file_n_iw ), STAT=allocerror )
      S_loc(:, :, :, :) = 0.0
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "S_loc" could not be allocated'
         CALL MPI_FINALIZE(i_error)
         STOP
      ELSE

         IF ( myrank .EQ. 0 ) THEN

            WRITE(6,*) '  rank 0: Reading data of SIGMA.DAT ...'

            !data
            DO iw=1,file_n_iw
               DO spin=1,file_n_spins
                  DO orbital_=1,file_n_sub_orbitals
                     DO orbital=1,file_n_sub_orbitals
                        READ(26,*) orbital_read, orbital__read, spin_read, iw_read
                        READ(26,*) S_loc_real_read, S_loc_imag_read
                        S_loc(orbital_read, orbital__read, spin_read, iw_read) = CMPLX(S_loc_real_read, S_loc_imag_read)
                     ENDDO
                  ENDDO
               ENDDO
            ENDDO

            CLOSE(26)

         ENDIF
         !broadcast sigma
         srcnt = file_n_orbitals*file_n_orbitals*file_n_spins*file_n_iw
         CALL MPI_BCAST(S_loc, srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)
         CALL check_mpi_success

!       !debug
!       IF ( myrank .EQ. 0 ) THEN
! 	OPEN (UNIT=26,FILE='./debug.txt',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
! 	  DO iw=1,file_n_iw
! 	    WRITE(26,*) iw, REAL(S_loc(1,1,1,iw)), AIMAG(S_loc(1,1,1,iw))
! 	  ENDDO
! 	CLOSE(26)
!       ENDIF
!
!       CALL MPI_FINALIZE(i_error)
!       STOP

         IF ( rank_0 ) THEN
            WRITE(6,*) '-'
         ENDIF

      ENDIF

   END SUBROUTINE import_SIGMA

   !=================================================================================================================================

   SUBROUTINE calc_GAMMA( proj, locproj, subspace, outcar )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    07/12/15     S. Barthel	Original code
      !

      USE timings
      !USE ifport

      USE type_locproj
      USE type_file_locproj
      USE type_file_subspace_locproj
      USE type_file_outcar

      IMPLICIT NONE

      !###############################################################################################################################
      !TODO: how do we decide if we used PROCAR or LOCPROJ ? simply put some quantities into proj-class ?
      !###############################################################################################################################

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(IN) :: proj
      TYPE( file_locproj ), INTENT(IN) :: locproj
      TYPE( file_subspace_locproj ), INTENT(IN) :: subspace	!is needed to point to correct vasp bands if different
      TYPE( file_outcar ) :: outcar
      LOGICAL :: is_static				!use eigenbasis for static self-energy

      INTEGER :: ioerror, allocerror				!error codes for allocation and file IO
      INTEGER :: kpoint, kpoint_, iw, spin, orbital_, orbital		!loop indices
      INTEGER :: band, band_, band_vasp, band_vasp_
      INTEGER :: ilin_par

      !REAL :: mu_dft

      REAL, DIMENSION(3) :: mu_interval

      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: dens_G0_k_loc
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: dens_G0_k_ext_vasp, dens_G0_k_loc_vasp
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: dens_G_k_loc
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: delta_loc, delta_ext

      COMPLEX :: check_trace

      ! CHARACTER*14 filename
      character (len=90) :: filename
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: gfdata        !orbital,orbital_prime,spin,iw
      character (len=90) :: orbstring

      !-------------------------------------------------------------------------------------------------------------------------------
      !get the correct chemical potentials
      !-------------------------------------------------------------------------------------------------------------------------------

      !CALL import_vasp_doscar_mu_dft( mu_dft )	!USE type_locproj

         IF (subspace%is_T0) then
            WRITE(6,*) '   Doing T=0 calculation.'
         else
            WRITE(6,*) '   Doing finite T calculation.'
         END IF

      CALL import_SIGMA( proj, outcar )		!HF

      !here we have to decide: read hamiltonian from disk / copy directly from memory ?
      CALL copy_from_memory_H0_loc( proj, outcar )

      !init G0(iw)
      CALL gf_iw_ALLOCATE( G0_loc, file_n_iw, file_n_spins, file_n_kpoints, file_n_orbitals)	!allocate memory
      IF ( subspace%is_T0 ) THEN
         CALL gf_iw_SET_MESH_T0( G0_loc,file_beta, file_fit_start )					!init matsubara mesh
      ELSE
         CALL gf_iw_SET_MESH( G0_loc, file_beta, file_fit_start )					!init matsubara mesh
      ENDIF
      CALL gf_iw_SET_H0( G0_loc, H0_loc, outcar%k_weight_ibz )					!init hamiltonian
      CALL gf_iw_SET_SIGMA_ZERO( G0_loc )								!init self-energy

      !use eigenbasis for G0
      !WRITE(6,*) 'deactivated G0 in eigenbasis for debugging!!!'
      CALL gf_iw_CALC_DIAG( G0_loc )

      CALL gf_iw_SET_NE( G0_loc, file_Ne )							!init number of electrons
      CALL gf_iw_SET_MU( G0_loc, file_mu )							!init chemical potential

      !---

      !init G(iw)
      CALL gf_iw_ALLOCATE( G_loc, file_n_iw, file_n_spins, file_n_kpoints, file_n_orbitals)	!allocate memory
      IF ( subspace%is_T0 ) THEN
         CALL gf_iw_SET_MESH_T0( G_loc,file_beta, file_fit_start )						!init matsubara mesh
      ELSE
         CALL gf_iw_SET_MESH( G_loc, file_beta, file_fit_start )						!init matsubara mesh
      ENDIF
      CALL gf_iw_SET_H0( G_loc, H0_loc, outcar%k_weight_ibz )					!init hamiltonian

      !-----------------------------------------------------------------------------------------------------------------------------
      IF ( file_n_iw == 1 ) THEN
         is_static = .TRUE.
      ENDIF
      !CALL gf_iw_SET_SIGMA_ZERO( G_loc )								!init self-energy
      IF ( subspace%is_T0 ) THEN
         CALL gf_iw_SET_SIGMA_T0( G_loc, S_loc )							!init self-energy
      ELSE
         CALL gf_iw_SET_SIGMA( G_loc, S_loc )							!init self-energy
      ENDIF

      !-----------------------------------------------------------------------------------------------------------------------------
      ! check if self-energy is static. It is static for only one matsubara freq.
      
      !use eigenbasis for static self-energy
      
      IF ( is_static ) THEN
         IF ( rank_0 ) THEN
            WRITE(6,*) "  Using eigenbasis for G ..."
         ENDIF
         CALL gf_iw_CALC_DIAG( G_loc )
      ENDIF

      CALL gf_iw_SET_NE( G_loc, file_Ne )								!init number of electrons

      CALL gf_iw_SET_MU( G_loc, file_mu )								!init chemical potential


      !chemical potentials
      mu_interval(1) = -50.
      mu_interval(2) = file_mu
      mu_interval(3) = 50.

      IF ( rank_0 ) THEN
         WRITE(6,*) 'adjusting chemical potential for G0'
      ENDIF
      IF ( subspace%is_T0 ) THEN
         ! WRITE(6,*) "Use functions for T == 0"
         WRITE(6,*) 'execute `gf_iw_ADJUST_MU_T0`'
         CALL gf_iw_ADJUST_MU_T0( G0_loc, mu_interval, .FALSE., .TRUE., .FALSE.  )			!only diagonal matrix elements needed
      ELSE
         ! WRITE(6,*) "Use functions for T > 0"
         WRITE(6,*) 'execute `gf_iw_ADJUST_MU`'
         CALL gf_iw_ADJUST_MU( G0_loc, mu_interval, .FALSE., .TRUE., .FALSE.  )			!only diagonal matrix elements needed
      ENDIF
      IF ( rank_0 ) THEN
         WRITE(6,*) 'adjusting chemical potential for G'
      ENDIF
      IF ( subspace%is_T0 ) THEN
         WRITE(6,*) 'execute `gf_iw_ADJUST_MU_T0`'
         CALL gf_iw_ADJUST_MU_T0( G_loc, mu_interval, .FALSE., .TRUE., .FALSE.  )
      ELSE
         WRITE(6,*) 'execute `gf_iw_ADJUST_MU`'
         CALL gf_iw_ADJUST_MU( G_loc, mu_interval, .FALSE., .TRUE., .FALSE.   )
      ENDIF

      IF ( rank_0 ) THEN
         !WRITE(6,*) 'mu G0', gf_iw_RETURN_MU( G0_loc )
         WRITE(6,*) 'mu G', gf_iw_RETURN_MU( G_loc )
         WRITE(6,*) 'file mu', file_mu
         WRITE(6,*) 'INFO: For magnetic calculations chemical potentials for G may differ'
      ENDIF

      !-------------------------------------------------------------------------------------------------------------------------------
      !density matrices for each kpoint
      !-------------------------------------------------------------------------------------------------------------------------------

      IF ( rank_0 ) THEN
         WRITE(6,*) " allocate G0"
      ENDIF
      !init G0(iw,k)
      CALL gf_iw_ALLOCATE( G0_k_loc, file_n_iw, file_n_spins, 1, file_n_orbitals)			!allocate memory
      IF ( subspace%is_T0 ) THEN
         CALL gf_iw_SET_MESH_T0( G0_k_loc ,file_beta, file_fit_start)					!init matsubara mesh
      ELSE
         CALL gf_iw_SET_MESH( G0_k_loc, file_beta, file_fit_start )					!init matsubara mesh
      ENDIF

      IF ( rank_0 ) THEN
         WRITE(6,*) " allocate G"
      ENDIF
      !init G(iw)
      CALL gf_iw_ALLOCATE( G_k_loc, file_n_iw, file_n_spins, 1, file_n_orbitals)			!allocate memory
      IF ( subspace%is_T0 ) THEN
         CALL gf_iw_SET_MESH_T0( G_k_loc ,file_beta, file_fit_start)					!init matsubara mesh
      ELSE
         CALL gf_iw_SET_MESH( G_k_loc, file_beta, file_fit_start )					!init matsubara mesh
      ENDIF

      IF ( rank_0 ) THEN
         WRITE(6,*) " allocate a lot ..."
      ENDIF

      ALLOCATE( dens_G0_k_loc(file_n_orbitals,file_n_orbitals,file_n_kpoints,file_n_spins), &
         dens_G0_k_ext_vasp(file_n_orbitals,file_n_orbitals,file_n_kpoints,file_n_spins), &
         dens_G0_k_loc_vasp(file_n_orbitals,file_n_orbitals,file_n_kpoints,file_n_spins), &
         dens_G_k_loc(file_n_orbitals,file_n_orbitals,file_n_kpoints,file_n_spins), &
         delta_loc(file_n_orbitals,file_n_orbitals,file_n_kpoints,file_n_spins), &
         delta_ext(file_n_orbitals,file_n_orbitals,file_n_kpoints,file_n_spins), STAT=allocerror)

      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "dens?" could not be allocated'
         CALL MPI_FINALIZE(i_error)
         STOP
      ELSE
         dens_G0_k_loc = CMPLX(0.,0.)
         dens_G0_k_ext_vasp = CMPLX(0.,0.)
         dens_G0_k_loc_vasp = CMPLX(0.,0.)
         dens_G_k_loc = CMPLX(0.,0.)
         delta_loc = CMPLX(0.,0.)
         delta_ext = CMPLX(0.,0.)
      ENDIF

      !DO kpoint=1,file_n_kpoints

      IF ( rank_0 ) THEN
         WRITE(6,*) "  Calculating correction to density matrix <L|ΔG0(k,σ)|L'> ..."
         !OPEN(UNIT=6, CARRIAGECONTROL='FORTRAN')
         OPEN(UNIT=6)
         WRITE(6,*) ''
      ENDIF



      DO kpoint=1,file_n_kpoints
         DO spin=1,file_n_spins
            DO orbital=1,file_n_orbitals
               dens_G0_k_ext_vasp(orbital,orbital,kpoint,spin) = locproj%N(orbital, kpoint, spin)
            END DO
         END DO
      END DO


      CALL locproj_TRANSFORM_EXT2LOC( proj, dens_G0_k_ext_vasp, dens_G0_k_loc_vasp )

      DO kpoint=1,file_n_kpoints
         CALL gf_iw_ZERO_G( G_k_loc )
         CALL gf_iw_SET_H0( G_k_loc, H0_loc(:,:,kpoint), outcar%k_weight_ibz_hf(kpoint) )

         IF ( subspace%is_T0 ) THEN
            CALL gf_iw_SET_SIGMA_T0( G_k_loc, S_loc )
         ELSE
            CALL gf_iw_SET_SIGMA( G_k_loc, S_loc )
         ENDIF

         CALL gf_iw_SET_NE( G_k_loc, file_Ne )
         CALL gf_iw_SET_MU( G_k_loc, gf_iw_RETURN_MU( G_loc ) )

         IF ( subspace%is_T0 ) THEN
            CALL gf_iw_CALC_DIAG( G_k_loc )
            CALL gf_iw_CALC_DENSITY_T0( G_k_loc, .FALSE., .TRUE. )
         ELSE
            CALL gf_iw_CALC_G( G_k_loc )
            CALL gf_iw_CALC_DENSITY_TAIL( G_k_loc, .FALSE. )
         ENDIF

         dens_G_k_loc(:,:,kpoint,:) = gf_iw_RETURN_DENSITY( G_k_loc, .FALSE., .FALSE., .FALSE., 0 )


      ENDDO
      dens_G0_k_loc = dens_G0_k_loc_vasp
      IF ( rank_0 ) THEN
         CLOSE(6)
      ENDIF

      IF ( rank_0 ) THEN
         WRITE(6,*)
         WRITE(6,'(3X,"Total elapsed time:",1X,F8.2,1X,"sec.")') timesum
         WRITE(6,*) '-'
      ENDIF

      !correction to density-matrix
      delta_loc = (dens_G_k_loc - dens_G0_k_loc)

      !trace checks
      IF ( rank_0 ) THEN
         OPEN (UNIT=26,FILE='./TRACE_loc',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)

         DO spin=1,file_n_spins
            check_trace = CMPLX(0.,0.)
            DO orbital=1,file_n_orbitals
               check_trace = check_trace + SUM( delta_loc(orbital,orbital,:,spin)*outcar%k_weight_ibz(:) ) /&
                                                            SUM(outcar%k_weight_ibz)
            ENDDO
            WRITE(6,*) "Tr{<n|ΔG0(σ)|n'>} : ", spin, REAL(check_trace), AIMAG(check_trace)
            WRITE(26,*) spin, REAL(check_trace), AIMAG(check_trace)
         ENDDO

         CLOSE(26)
      ENDIF

      !transform to extended-basis (mind, that proj is internally mapped to irreducible k-points)
      CALL locproj_TRANSFORM_LOC2EXT( proj, outcar, delta_loc, delta_ext )	!will transform only spins present in proj ...

      IF (  proj%n_spins == 1  .AND.  file_n_spins == 2  ) THEN
         delta_ext(:,:,:,2) = delta_ext(:,:,:,1)	!copy first spin to second one
      ENDIF

      !TODO: trace check, undo orthonormalization, compare vasp density matrix with interface one of G0 for single k-point

      !trace checks
      IF ( rank_0 ) THEN
         OPEN (UNIT=26,FILE='./TRACE_ext',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)

         DO spin=1,file_n_spins
            check_trace = CMPLX(0.,0.)
            DO orbital=1,file_n_orbitals
               check_trace = check_trace + SUM( delta_ext(orbital,orbital,:,spin)*outcar%k_weight_ibz(:) ) / &
                                                                    SUM(outcar%k_weight_ibz)
            ENDDO
            WRITE(6,*) "Tr{<L|ΔG0(σ)|L'>} : ", spin, REAL(check_trace), AIMAG(check_trace)
            WRITE(26,*) spin, REAL(check_trace), AIMAG(check_trace)
         ENDDO

         CLOSE(26)
         DO spin=1,file_n_spins
            check_trace = CMPLX(0.,0.)
            DO orbital=1,file_n_orbitals
               check_trace = check_trace + SUM( dens_G0_k_loc(orbital,orbital,:,spin)*outcar%k_weight_ibz(:) ) &
                                                            / SUM(outcar%k_weight_ibz)
            ENDDO
            WRITE(6,*) "Tr{<L|G0(σ)|L'>} : ", spin, REAL(check_trace), AIMAG(check_trace)

         ENDDO
         DO spin=1,file_n_spins
            check_trace = CMPLX(0.,0.)
            DO orbital=1,file_n_orbitals
               check_trace = check_trace + SUM( dens_G_k_loc(orbital,orbital,:,spin)*outcar%k_weight_ibz(:) ) &
                                                / SUM(outcar%k_weight_ibz)
            ENDDO
            WRITE(6,*) "Tr{<L|G(σ)|L'>} : ", spin, REAL(check_trace), AIMAG(check_trace)

         ENDDO
      ENDIF

      !-------------------------------------------------------------------------------------------------------------------------------
      2000     FORMAT(2(2X,F19.16))
      !write GAMMA
      IF ( rank_0 ) THEN
         WRITE(6,*) 'writing GAMMA to disk'
! 	! write starting band, and final band
!         NB_START = 1
!         NB_END = NB_TOT
!         WRITE(IU_GAMMA, '(3I10," ! k-point index, start and end band")') NK, NB_START, NB_END
!
!         DO N1=NB_START, NB_END
!            WRITE(IU_GAMMA,'(16E15.7)') CHAM(N1, NB_START:NB_END)
!         ENDDO

         OPEN (UNIT=26,FILE='./GAMMA',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)

         !header: number of kpoints and number of bands in the LOCPROJ, i.e. initial VASP542 calculation
         !WRITE(26,*) outcar%n_kpoints_ibz, locproj%n_bands
         WRITE(26,'(2I10," ! number of k-points and bands")') outcar%n_kpoints_ibz, locproj%n_bands	!VASP542 source

         !loops corresponding to ADD_GAMMA_FROM_FILE, subrot.F
         DO spin=1,file_n_spins	!mind: locproj and proj may have only one spin-channel

            !DEV: lets write GAMMA for irreducible k-points (which are stored first in LOCPROJ apparently)
            !DO kpoint=1,locproj%n_kpoints
            DO kpoint=1,outcar%n_kpoints_ibz		!irr. BZ, i.e. GAMMA and PROCAR

               !WRITE(6,*) 'debug: iBZ k-point indices', outcar%idx_ibz_hf(kpoint)

               !write k-point index, pointer to starting VASP band, pointer to ending VASP band ONLY CONTINOUS !!!
               WRITE(26, '(3I10," ! k-point index, start and end band")') kpoint, &
                                subspace%band_indices(1), subspace%band_indices(subspace%n_bands)

               DO band=1,subspace%n_bands
                  !WRITE(26,'(16E15.7)') delta_ext(band, 1:subspace%n_bands, outcar%idx_ibz_hf(kpoint), spin)	!VASP542 source
                  !WRITE(26,'(16E19.16)') delta_ext(band, 1:subspace%n_bands, outcar%idx_ibz_hf(kpoint), spin)
                  DO band_=1,subspace%n_bands
                     WRITE(26,2000) REAL( delta_ext(band, band_, kpoint, spin)&
                         & + 1E-18&
                         & ), AIMAG( delta_ext(band, band_, kpoint, spin) ) +&
                         & 1E-18
                  ENDDO
               ENDDO

            ENDDO
         ENDDO
         CLOSE(26)
      ENDIF

      !-------------------------------------------------------------------------------------------------------------------------------

      !write chemical potentials
      IF ( rank_0 ) THEN
         OPEN (UNIT=26,FILE='./MU_INTERFACE.TXT',STATUS='OLD', ACTION='WRITE', ACCESS='APPEND', IOSTAT=ioerror)
         IF ( ioerror /= 0 ) THEN
            OPEN (UNIT=26,FILE='./MU_INTERFACE.TXT',STATUS='NEW', ACTION='WRITE', IOSTAT=ioerror)
         ENDIF
         !WRITE(26,'(3X,F19.16,1X,F19.16,1X,F19.16)') mu_dft, gf_iw_RETURN_MU(G0_loc), gf_iw_RETURN_MU(G_loc)
             WRITE(26,'(3X,F20.16,1X,F20.16)') gf_iw_RETURN_MU(G0_loc), gf_iw_RETURN_MU(G_loc)
             CLOSE(26)
          ENDIF

          !-------------------------------------------------------------------------------------------------------------------------------

          !write debug
          IF ( rank_0 ) THEN
             WRITE(6,*) 'Not writing debug information to disk'
          ENDIF
          IF ( rank_0 .AND. .TRUE.) THEN

    1000     FORMAT(4(2X,I6),2(2X,F19.16))

             OPEN (UNIT=26,FILE='./dens_G0_k_loc_MPI.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
             DO spin=1,file_n_spins
                DO kpoint=1,file_n_kpoints
                   DO orbital_=1,file_n_orbitals
                      DO orbital=1,file_n_orbitals
                         WRITE(26,1000) orbital, orbital_, kpoint, spin, &
                            REAL(dens_G0_k_loc(orbital,orbital_,kpoint,spin)), AIMAG(dens_G0_k_loc(orbital,orbital_,kpoint,spin))
                      ENDDO
                   ENDDO
                ENDDO
             ENDDO

             OPEN (UNIT=26,FILE='./dens_G_k_loc_MPI.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
             DO spin=1,file_n_spins
                DO kpoint=1,file_n_kpoints
                   DO orbital_=1,file_n_orbitals
                      DO orbital=1,file_n_orbitals
                         WRITE(26,1000) orbital, orbital_, kpoint, spin, &
                            REAL(dens_G_k_loc(orbital,orbital_,kpoint,spin)), AIMAG(dens_G_k_loc(orbital,orbital_,kpoint,spin))
                      ENDDO
                   ENDDO
                ENDDO
             ENDDO

             OPEN (UNIT=26,FILE='./delta_loc.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
             DO spin=1,file_n_spins
                DO kpoint=1,file_n_kpoints
                   DO orbital_=1,file_n_orbitals
                      DO orbital=1,file_n_orbitals
                         WRITE(26,1000) orbital, orbital_, kpoint, spin, &
                            REAL(delta_loc(orbital,orbital_,kpoint,spin)), AIMAG(delta_loc(orbital,orbital_,kpoint,spin))
                      ENDDO
                   ENDDO
                ENDDO
             ENDDO

             OPEN (UNIT=26,FILE='./delta_loc_diag.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
             DO spin=1,file_n_spins
                DO kpoint=1,file_n_kpoints
                  DO orbital=1,file_n_orbitals
                     WRITE(26,1000) orbital, orbital, kpoint, spin, &
                        REAL(delta_loc(orbital,orbital,kpoint,spin)), AIMAG(delta_loc(orbital,orbital,kpoint,spin))
                   ENDDO
                ENDDO
             ENDDO

             OPEN (UNIT=26,FILE='./delta_ext.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
             DO spin=1,file_n_spins
                DO kpoint=1,file_n_kpoints
                   DO orbital_=1,file_n_orbitals
                      DO orbital=1,file_n_orbitals
                         WRITE(26,1000) orbital, orbital_, kpoint, spin, &
                            REAL(delta_ext(orbital,orbital_,kpoint,spin)), AIMAG(delta_ext(orbital,orbital_,kpoint,spin))
                      ENDDO
                   ENDDO
                ENDDO
             ENDDO

             OPEN (UNIT=26,FILE='./delta_ext_diag.TXT',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
             DO spin=1,file_n_spins
                DO kpoint=1,file_n_kpoints
                  DO orbital=1,file_n_orbitals
                     WRITE(26,1000) orbital, orbital, kpoint, spin, &
                        REAL(delta_ext(orbital,orbital,kpoint,spin)), AIMAG(delta_ext(orbital,orbital,kpoint,spin))
                   ENDDO
                ENDDO
             ENDDO


         ALLOCATE ( gfdata(file_n_orbitals, file_n_orbitals, file_n_spins, file_n_iw), STAT=allocerror )
         gfdata = gf_iw_RETURN_DATA(G_loc)

         ! write
         WRITE(*,*) 'Export green function'

            DO spin=1,file_n_spins
               if (spin > 1) then
                  WRITE(*,*) 'WARNING: when writing greens functions, only one spin is considered'
               end if
               DO orbital=1,file_n_orbitals
                  ! DO orbital_=1,file_n_orbitals

                  ! WRITE(*,*) orbital
                  WRITE(orbstring, '(I2.2)') orbital
                  ! WRITE(*,*) orbstring
                  WRITE(filename, '( "./green_",I2.2,"_",I2.2,".dat" )') orbital, orbital
                  ! WRITE(*,*) filename
                  OPEN (UNIT=26,FILE=filename, STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)

    ! 1000     FORMAT(4(2X,I6),2(2X,F19.16))
                  do iw=1,file_n_iw
                     WRITE(26, '( 2(2X,F19.16) )' ) &
                        REAL(gfdata(orbital,orbital,spin,iw)), AIMAG(gfdata(orbital,orbital,spin,iw))
              end do
              ! ENDDO
           ENDDO
        ENDDO

      ENDIF

   END SUBROUTINE calc_GAMMA

END MODULE selfconsistency
