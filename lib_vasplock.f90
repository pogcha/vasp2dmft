MODULE vasplock
!
!  Purpose:
!    MPI parallelization
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!

   USE parallel

   IMPLICIT NONE

CONTAINS

   SUBROUTINE create_vasplock

      IMPLICIT NONE

      IF ( rank_0 ) THEN
         OPEN(26,FILE='./vasp.lock',STATUS='UNKNOWN')
         WRITE(26,*) 'vasp2dmft is running happily'
         CLOSE(26)

         WRITE(6,*) '  rank 0: vasp.lock created'
         WRITE(6,*)
      ENDIF

   END SUBROUTINE create_vasplock

   !---------------------------------------------------------------------------------------------------------------------------------

   SUBROUTINE wait_no_vasplock

      IMPLICIT NONE

      INTEGER :: ioerror, ioerror_finish, ioerror_eof

      CHARACTER(22) :: f_finished

      LOGICAL :: is_finished_vasp

      IF ( rank_0 ) THEN
         WRITE(6,*) '  rank 0: waiting for removal of vasp.lock'
      ENDIF

      is_finished_vasp = .FALSE.
      ioerror = 0
      DO WHILE ( ioerror == 0 )	!wait until vasp.lock got removed, then proceed, maybe errorcode is not correct ?

         IF ( rank_0 ) THEN
            OPEN(26,FILE='./vasp.lock',STATUS='OLD',IOSTAT=ioerror)
            !WRITE(6,*)  '  while:', myrank, ioerror
            CLOSE(26)
         ENDIF

         !broadcast
         srcnt = 1
         CALL MPI_BCAST(ioerror, srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
         CALL check_mpi_success

         !WRITE(6,*)  '  while done:', myrank, ioerror

         !DEV: 21.01.16 - STOP if we have vasp.lock and the string 'writing wavefunctions is present'

         IF ( rank_0 ) THEN

            OPEN(26,FILE='./vasp.log',STATUS='OLD',IOSTAT=ioerror_finish)

            IF ( ioerror_finish == 0 ) THEN	!check file for string only if file is present

               DO
                  READ(26,*,IOSTAT=ioerror_eof)

                  !WRITE(6,*)  '  vaso log:', myrank, ioerror_eof

                  !read until EOF
                  IF ( ioerror_eof < 0) THEN

                     !rewind two lines
                     BACKSPACE(UNIT=26)
                     BACKSPACE(UNIT=26)

                     !check if vasp has stopped running
                     READ(26,'(A)') f_finished
                     IF ( INDEX(f_finished,' writing wavefunctions') == 1 ) THEN
                        CLOSE(26)
                        is_finished_vasp = .TRUE.
                        WRITE(6,*) '  rank 0: VASP is writing wavefunctions'
                     ENDIF

                     EXIT

                  ENDIF
               ENDDO

            ENDIF

            CLOSE(26)
         ENDIF

         !broadcast
         srcnt = 1
         CALL MPI_BCAST(is_finished_vasp, srcnt, MPI_LOGICAL, 0, MPI_COMM_WORLD, i_error)
         CALL check_mpi_success

         CALL SLEEP(2)

         !WRITE(6,*) '  waiting:', myrank, ioerror

         CALL MPI_BARRIER(MPI_COMM_WORLD, i_error)

         !normal stop
         IF ( is_finished_vasp ) THEN
            IF ( rank_0 ) THEN
               WRITE(6,*) 'Done. VASP has finished.'
            ENDIF
            CALL MPI_FINALIZE(i_error)
            STOP
         ENDIF

      ENDDO

      IF ( rank_0 ) THEN
         WRITE(6,*) '  rank 0: vasp.lock got removed'
         WRITE(6,*)
      ENDIF

      CALL MPI_BARRIER(MPI_COMM_WORLD, i_error)

   END SUBROUTINE wait_no_vasplock

   !---------------------------------------------------------------------------------------------------------------------------------

   SUBROUTINE remove_vasplock

      IMPLICIT NONE

      INTEGER :: ioerror

      IF ( rank_0 ) THEN
         OPEN(26,FILE='./vasp.lock',STATUS='UNKNOWN')
         WRITE(26,*) 'file will be removed'
         CLOSE(26,STATUS='DELETE')

         WRITE(6,*) '  rank 0: vasp.lock removed'
      ENDIF

      CALL SLEEP(2)

      CALL MPI_BARRIER(MPI_COMM_WORLD, i_error)

   END SUBROUTINE remove_vasplock

END MODULE vasplock
