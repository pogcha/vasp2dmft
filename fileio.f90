MODULE fileio
!
!  Purpose:
!    Collection of routines to assist with file IO
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    11/11/13     S. Barthel	Original code
!

   IMPLICIT NONE

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units
   CHARACTER(len=2048) :: line_readin, line_readin_more
   INTEGER :: n_elements
   INTEGER :: idx_seperator

CONTAINS

   !=============================================================================================================================

   FUNCTION numel_string(string_readin, max_numel)
      !
      !  Purpose:
      !    Determine the number of elements in a string read from a file.
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    07/02/14     S. Barthel	Original code
      !
      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      CHARACTER(len=*), INTENT(IN) :: string_readin	!input string to examine
      INTEGER, INTENT(IN) :: max_numel			!maximum number of elements in the string
      INTEGER :: numel_string

      INTEGER :: ioerror					!error code file IO
      INTEGER :: i					!loop index

      REAL, DIMENSION(max_numel) :: tmp			!temporary read in

      DO i=1,max_numel
         !Free fromat read in
         READ(string_readin,*,IOSTAT=ioerror) tmp(1:i)
         !WRITE(6,*) ioerror, i, tmp(1:i)
         IF (ioerror /= 0) THEN
            !There are i-1 elements in that string
            numel_string = i-1
            RETURN
!       ELSE
! 	WRITE(6,*) '  ERROR: "numel_string" expected real value.'
         ENDIF
      ENDDO

      !There are at least max_numel elements in that string
      numel_string = max_numel

   END FUNCTION numel_string



   !=============================================================================================================================

END MODULE fileio
