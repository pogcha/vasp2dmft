MODULE type_file_locproj
   !
   !  Purpose:
   !    routines to process locproj file
   !  Record of revisions:
   !      Date       Programmer	  Description of change
   !      ====       ==========	  =====================
   !    01/12/15     S. Barthel	  Original code
   !

   USE parallel

   IMPLICIT NONE

   ! Data dictionary: declare constants

   ! Data dictionary: type definitions

   !=================================================================================================================================

   TYPE :: file_locproj

      !PRIVATE                   				!no access granted to member variables

      !header
      INTEGER :: n_spins
      INTEGER :: n_kpoints
      INTEGER :: n_bands
      INTEGER :: n_orbitals     				!this is a combined linear index of L, µ

      !data

      INTEGER, ALLOCATABLE, DIMENSION(:) :: ion_type
      INTEGER, ALLOCATABLE, DIMENSION(:) :: orbital_type

      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: E		!dft-eigenvalues E(n,k,s)
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: N		!dft-occupations N(n,k,s)

      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: P	!projector matrix <p:L|n,k,s>

   END TYPE file_locproj

   !=================================================================================================================================

CONTAINS


   SUBROUTINE file_locproj_READER( file )
      !
      !  Purpose:
      !    read locproj, MPI safe
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    01/12/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_locproj ), INTENT(INOUT) :: file
      INTEGER :: ioerror	!error code for file IO

      INTEGER :: n_spins
      INTEGER :: n_kpoints
      INTEGER :: n_bands
      INTEGER :: n_orbitals

      character(100) :: orb_char
      character(LEN=10) :: labels(16) = &
      &(/"s         ",&
         "py        ",&
         "pz        ",&
         "px        ",&
         "dxy       ",&
         "dyz       ",&
         "dz2       ",&
         "dxz       ",&
         "dx2-y2    ",&
         "fy(3x2-y2)",&
         "fxyz      ",&
         "fyz2      ",&
         "fz3       ",&
         "fxz2      ",&
         "fz(x2-y2) ",&
         "fx(x2-3y2)" /)

      INTEGER spin, kpoint, band, orbital, label_	!loop indices
      INTEGER spin_, kpoint_, band_, orbital_	!read indices

      REAL :: E_read, N_read, dummy1, dummy2, dummy3
      REAL :: P_real_read, P_imag_read

      !-----------------------------------------------------------------------------------------------------------------------------

      !-----------------------------------------------------------------------------------------------------------------------------
      !Format descriptors, vasp.5.4.2 (build 02Dez)
      !-----------------------------------------------------------------------------------------------------------------------------
      !9001 FORMAT(4I6,"  # of spin, # of k-points, # of bands, # of proj")	!header
      !9002 FORMAT("orbital",3I6,2F20.10)					!data
      !9003 FORMAT(1I6,2F20.10)							!data

9001  FORMAT(4I6,1X)							!header
9002  FORMAT(7X,3I6,2F20.10)						!data
9003  FORMAT(1I6,2F20.10)							!data
9004  FORMAT(9X,1I6,11X,3F14.7,2X,A,2X)
      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( rank_0 ) THEN

         WRITE(6,*) '  rank 0: Reading header of LOCPROJ ...'

         OPEN (UNIT=1001,FILE='./LOCPROJ',STATUS='OLD', ACTION='READ', IOSTAT=ioerror)

         !check if file actually exists
         IF ( ioerror /= 0 ) THEN
            WRITE(6,*) 'ERROR: File "LOCPROJ" missing'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ELSE

            !read header
            READ(1001,9001,IOSTAT=ioerror) n_spins, n_kpoints, n_bands, n_orbitals
            IF ( ioerror /= 0 ) THEN
               WRITE(6,*) 'ERROR: header of "LOCPROJ" corrupt'
               CALL MPI_FINALIZE(i_error)
               STOP
            ELSE

               WRITE(6,*) '  n_spins    : ', n_spins
               WRITE(6,*) '  n_kpoints  : ', n_kpoints
               WRITE(6,*) '  n_bands    : ', n_bands
               WRITE(6,*) '  n_orbitals : ', n_orbitals


            ENDIF

         ENDIF

      ENDIF

      !broadcast header
      srcnt = 1
      CALL MPI_BCAST(n_spins,    srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(n_kpoints,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(n_bands,    srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(n_orbitals, srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      !allocate
      CALL file_locproj_ALLOCATE( file, n_spins, n_kpoints, n_bands, n_orbitals )

      IF ( rank_0 ) THEN


         WRITE(6,*) '  orbital    :    ion  character'
         !---
         !read orbital description
         DO orbital=1,file%n_orbitals
            !READ(1001,9004,IOSTAT=ioerror) ion_,dummy1, dummy2, dummy3
            READ(1001,'(9X,I6,7X,3F14.7,20X,A)',IOSTAT=ioerror) file&
                &%ion_type(orbital), dummy1, dummy2,dummy3, orb_char
            ! assign orbital type integer according to label in LOCPROJ (e.g. s = 1, py=2, ...)
            DO label_=1,16
               IF (trim(adjustl(orb_char))== trim(adjustl(labels(label_)))) then
                  file%orbital_type(orbital) = label_
               END IF
            END DO
            IF ( (ioerror /= 0) ) THEN
               WRITE(6,*) 'ERROR: data of "LOCPROJ" corrupt, orb'
               CALL MPI_Abort(MPI_COMM_WORLD, i_error)
               STOP
            ENDIF
            WRITE(6,'(4X,I4,A,1I6,5X,A)') orbital,'      :', file%ion_type(orbital), &
                labels(file%orbital_type(orbital))
         ENDDO

         WRITE(6,*) '  rank 0: Reading data of LOCPROJ ...'
         !skip blank line
         READ(1001,*,IOSTAT=ioerror)
         IF ( (ioerror /= 0) ) THEN
            WRITE(6,*) 'ERROR: data of "LOCPROJ" corrupt'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         !data
         DO spin=1,file%n_spins
            DO kpoint=1,file%n_kpoints
               DO band=1,file%n_bands

                  READ(1001,9002,IOSTAT=ioerror) spin_, kpoint_, band_, E_read, N_read

                  IF ( (ioerror /= 0) .OR. (spin /= spin_) .OR. (kpoint /= kpoint_) .OR. (band /= band_) ) THEN
                     WRITE(6,*) 'ERROR: data of "LOCPROJ" corrupt'
                     CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                     STOP
                  ELSE
                     file%E(band_, kpoint_, spin_) = E_read	!E(n,k,s)
                     file%N(band_, kpoint_, spin_) = N_read	!N(n,k,s)
                  ENDIF

                  DO orbital=1,file%n_orbitals

                     READ(1001,9003,IOSTAT=ioerror) orbital_, P_real_read, P_imag_read

                     IF ( (ioerror /= 0) .OR. (orbital /= orbital_) ) THEN
                        WRITE(6,*) 'ERROR: data of "LOCPROJ" corrupt'
                        CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                        STOP
                     ELSE
                        file%P(orbital_, band_, kpoint_, spin_) = CMPLX(P_real_read, P_imag_read)	!P := <p:L|n,k,s>
                     ENDIF

                  ENDDO

                  !skip blank line after each orbital loop
                  READ(1001,*,IOSTAT=ioerror)
                  IF ( (ioerror /= 0) ) THEN
                     WRITE(6,*) 'ERROR: data of "LOCPROJ" corrupt'
                     CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                     STOP
                  ENDIF

               ENDDO
            ENDDO
         ENDDO

         CLOSE(1001)

      ENDIF

      !broadcast
      srcnt = file%n_bands*file%n_kpoints*file%n_spins
      CALL MPI_BCAST(file%E, srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      CALL MPI_BCAST(file%N, srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      srcnt = file%n_orbitals*file%n_bands*file%n_kpoints*file%n_spins
      CALL MPI_BCAST(file%P, srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      IF ( rank_0 ) THEN
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE file_locproj_READER

   !===============================================================================================================================

   !=================================================================================================================================

   SUBROUTINE file_locproj_SYM_P_COORDS( file, coords )
      !
      !  Purpose:
      !    Rotate s,p,d-orbitals using point-group symmetries, VASP order assumed
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    03/11/15     S. Barthel	Original code
      !

      USE type_file_coords
      USE math
      USE symmetries

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_locproj ), INTENT(INOUT) :: file
      TYPE( file_coords ), INTENT(IN) :: coords

      INTEGER :: allocerror				!error code for allocation
      INTEGER :: orbital,orbital_, ion, ion_, band, kpoint, spin		!loop indices

      REAL, DIMENSION(3,3) :: inv_T_local
      REAL, DIMENSION(9,9) :: inv_L_spd       !Inverse transformation matrix of spd-orbitals into new local coordinate system

      COMPLEX, DIMENSION(file%n_orbitals, file%n_bands, file%n_kpoints, file%n_spins) :: PWORK

      !-------------------------------------------------------------------------------------------------------------------------------

      PWORK = CMPLX(0.,0.)
      !IF ( rank_0 ) THEN
      !   WRITE(6,*) coords%n_ions
      !   DO orbital=1,coords%n_ions
      !      WRITE(6,*) coords%T_local(1,:,orbital)
      !      WRITE(6,*) coords%T_local(2,:,orbital)
      !      WRITE(6,*) coords%T_local(3,:,orbital)
      !      WRITE(6,*) 
      !   END DO
      !ENDIF
      !calculate inverse T^{-1}
      !inv_T_local = coords%T_local(:,:,3)	!storage in columns
      !CALL real_matrix_inverse(3, inv_T_local )	!storage in rows

      !calculate inverse L^{-1}
      !inv_L_spd = inv_T_spd( inv_T_local )
      IF ( rank_0 ) THEN
      !   WRITE(6,*) 'sp'
      !   WRITE(6,*) inv_L_spd(1,1:4)
      !   WRITE(6,*) inv_L_spd(2,1:4)
      !   WRITE(6,*) inv_L_spd(3,1:4)
      !   WRITE(6,*) inv_L_spd(4,1:4)
      !   WRITE(6,*) 'd'
      !   WRITE(6,*) inv_L_spd(5,5:9)
      !   WRITE(6,*) inv_L_spd(6,5:9)
      !   WRITE(6,*) inv_L_spd(7,5:9)
      !   WRITE(6,*) inv_L_spd(8,5:9)
      !   WRITE(6,*) inv_L_spd(9,5:9)

         !loops (local)
         DO spin=1,file%n_spins
            DO kpoint=1,file%n_kpoints
               DO band=1,file%n_bands

                  DO orbital=1,file%n_orbitals
                     !calculate inverse T^{-1}
                     inv_T_local = coords%T_local(:,:,file%ion_type(orbital))	!storage in columns
                     !WRITE(6,*) 'doing inverting'
                     CALL real_matrix_inverse(3, inv_T_local )	!storage in rows
                     !WRITE(6,*) 'done inverting'
                     !calculate inverse L^{-1}
                     inv_L_spd = inv_T_spd( inv_T_local )

                     DO orbital_=1,file%n_orbitals
                        IF ( file%ion_type(orbital_) == file%ion_type(orbital) ) THEN
                           PWORK(orbital,band,kpoint,spin) = PWORK(orbital,band,kpoint,spin) &
                              + ( inv_L_spd(file%orbital_type(orbital),&
                              file%orbital_type(orbital_)) * file%P(orbital_,band,kpoint,spin) )
                        ENDIF
                     ENDDO ! orbital_
                     !x' = T^{-1}x wrt. orbitals
                  ENDDO ! orbital
               ENDDO ! band
            ENDDO ! kpoint
         ENDDO ! spin

         file%P = PWORK
      ENDIF

      srcnt = file%n_orbitals*file%n_bands*file%n_kpoints*file%n_spins
      CALL MPI_BCAST(file%P, srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      IF ( rank_0 ) THEN
         WRITE(6,*) '   P rotated to COORDS'
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE file_locproj_SYM_P_COORDS

   SUBROUTINE file_locproj_ALLOCATE( file, n_spins, n_kpoints, n_bands, n_orbitals )
      !
      !  Purpose:
      !    allocate file locproj
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    01/12/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      !-----------------------------------------------------------------------------------------------------------------------------

      TYPE( file_locproj ), INTENT(INOUT) :: file

      INTEGER, INTENT(IN) :: n_spins
      INTEGER, INTENT(IN) :: n_kpoints
      INTEGER, INTENT(IN) :: n_bands
      INTEGER, INTENT(IN) :: n_orbitals

      INTEGER :: allocerror	!error code for allocation

      !-----------------------------------------------------------------------------------------------------------------------------

      file%n_spins = n_spins
      file%n_kpoints = n_kpoints
      file%n_bands = n_bands
      file%n_orbitals = n_orbitals

      !dft-eigenvalues
      ALLOCATE ( file%E( file%n_bands, file%n_kpoints, file%n_spins ), STAT=allocerror )	!E(n,k,s)
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "file%E" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         file%E = 0.
      ENDIF

      !dft-occupations
      ALLOCATE ( file%N( file%n_bands, file%n_kpoints, file%n_spins ), STAT=allocerror )	!N(n,k,s)
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "file%N" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         file%N = 0.
      ENDIF

      !ion-types
      ALLOCATE ( file%ion_type( file%n_orbitals ), STAT=allocerror )	!N(n,k,s)
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "file%ion_type" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         file%ion_type = -1
      ENDIF

      !orbital-types
      ALLOCATE ( file%orbital_type( file%n_orbitals ), STAT=allocerror )	!N(n,k,s)
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "file%orbital_type" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         file%orbital_type = 0
      ENDIF

      !projector matrices
      ALLOCATE ( file%P( file%n_orbitals, file%n_bands, file%n_kpoints, file%n_spins ), STAT=allocerror )	!P := <p:L|n,k,s>
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "file%P" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         file%P = CMPLX(0.,0.)
      ENDIF

   END SUBROUTINE file_locproj_ALLOCATE

   !===============================================================================================================================


END MODULE type_file_locproj
