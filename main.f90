PROGRAM main
!
!  Purpose:
!    Main program
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    30/08/15     S. Barthel	Original code
!

   USE parallel
   USE vasplock

   USE shared_data

   USE selfconsistency


   IMPLICIT NONE

! Data dictionary: declare variable types, definitions, units

   CHARACTER(len=32) :: arg
   LOGICAL :: charge_sc

   INTEGER :: ioerror

   CHARACTER(8)  :: date
   CHARACTER(10) :: time

!-----------------------------------------------------------------------------------------------------------------------------------
!Initialize MPI variables
!-----------------------------------------------------------------------------------------------------------------------------------
   CALL MPI_INIT(i_error)
   CALL MPI_COMM_SIZE(MPI_COMM_WORLD, nprocs, i_error)
   CALL MPI_COMM_RANK(MPI_COMM_WORLD, myrank, i_error)

   CALL parallel_set_rank_0

   IF ( rank_0 ) THEN
      WRITE(6,*) 'dft++ interface vasp2dmft 16.02, 02-02-2016'
      WRITE(6,*) 'vasp.5.4.2 (build 02Dez), PROCAR/LOCPROJ -version'
      WRITE(6,*) 'developed by Stefan Barthel, Malte Schüler & Tim Wehling'
      WRITE(6,'(1X,"MPI: running on",1X,I3,1X,"cores ...")') nprocs
      CALL date_and_time(DATE=date)
      CALL date_and_time(TIME=time)
      WRITE(6,'(1X,A,".",A,".",A,"-",A,":",A,":",A)') date(1:4),date(5:6),date(7:8), time(1:2),time(3:4),time(5:6)
      WRITE(6,*) '-'
   ENDIF

!only run if vasp.lock is absent
   !CALL wait_no_vasplock


!-----------------------------------------------------------------------------------------------------------------------------------
!File IO
!-----------------------------------------------------------------------------------------------------------------------------------

! get charge-self-consistency  flag as command line argument
   CALL get_command_argument(1, arg)

   IF (LEN_TRIM(arg) == 0) THEN
      charge_sc = .FALSE.
   ELSE
      READ(arg,*,IOSTAT=ioerror) charge_sc
      IF (ioerror /= 0) THEN
         WRITE(6,*) 'ERROR: "charge_sc=" has to be of type logical.'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ENDIF
   ENDIF

! get use_procar flag as command line argument

   CALL get_command_argument(2, arg)

   IF (LEN_TRIM(arg) == 0) THEN
      use_procar = .FALSE.
   ELSE
      READ(arg,*,IOSTAT=ioerror) use_procar
      IF (ioerror /= 0) THEN
         WRITE(6,*) 'ERROR: "use_procar=" has to be of type logical.'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ENDIF
   ENDIF

! sanity check of user input: charge self consistency does not work with PROCAR

   IF ( ( use_PROCAR ) .AND. ( charge_sc ) ) THEN
      WRITE(6,*) 'ERROR: "use_procar=T" with "charge_sc=T" is currently not implemented'
      CALL MPI_Abort(MPI_COMM_WORLD, i_error)
      STOP
   END IF

   IF ( rank_0 ) THEN
        IF ( charge_sc ) THEN
            WRITE(6,*) "  Projecting onto localized orbitals and calculating H(k) and calculating charge update from self energy"
        else
            WRITE(6,*) "  Projecting onto localized orbitals and calculating H(k). No Charge update"
        ENDIF
    ENDIF
   IF ( rank_0 ) THEN
    IF ( use_procar ) THEN
        WRITE(6,*) "  using PROCAR to read raw VASP projectors"
    else
        WRITE(6,*) "  using LOCPROJ to read raw VASP projectors"
    ENDIF
    WRITE(6,*) ""
   ENDIF

   vasp_version = 542

!MIND: the matlab  script does not return the initial band-indices

   IF ( use_procar ) THEN	!does not work with charge slef-consistency, because PROCAR will not be written during VASP iteration

      !---------------------------------------------------------------------------------------------------------------------------------
      !Projectors: PROCAR
      !---------------------------------------------------------------------------------------------------------------------------------

      !init POSCAR file
      CALL file_poscar_READ( poscar_01 )

      !init COORDS file
      CALL file_coords_SET_FILENAME( coords_01, 'COORDS          ' )
      CALL file_coords_SET( coords_01, poscar_01 )

      !init PROCAR file
      CALL file_procar_READ( procar_01, poscar_01, vasp_version, .FALSE. )
      CALL file_procar_SET_P( procar_01 )
      CALL file_procar_SET_PPHASE( procar_01, poscar_01 )


      !CALL file_procar_SYM_P_COORDS( procar_01, coords_01 )			!phases are not relevant for local coordinate systems ?

      !init SUBSPACE file
      CALL file_subspace_procar_SET_FILENAME( subspace_procar_01, 'SUBSPACE        ' )
      CALL file_subspace_procar_INIT( subspace_procar_01, procar_01 )

      CALL class_locproj_SET_P_FROM_PROCAR( proj_01, procar_01, subspace_procar_01 )	!NO reninit

   ELSE

      !---------------------------------------------------------------------------------------------------------------------------------
      !Projectors: LOCPROJ
      !---------------------------------------------------------------------------------------------------------------------------------
      !init locproj file
      CALL file_locproj_READER( locproj_01 )
      CALL file_poscar_READ( poscar_01 )
      CALL file_coords_SET_FILENAME( coords_01, 'COORDS          ' )
      CALL file_coords_SET( coords_01, poscar_01 )
      !init subspace file
      CALL file_subspace_locproj_INIT( subspace_locproj_01, locproj_01 )

      CALL file_locproj_SYM_P_COORDS( locproj_01, coords_01 )
      !init projectors based on locproj-file
      CALL class_locproj_SET_P_FROM_LOCPROJ( proj_01, locproj_01, subspace_locproj_01 )
   ENDIF

!-----------------------------------------------------------------------------------------------------------------------------------
!Orthonormalization & Complement
!-----------------------------------------------------------------------------------------------------------------------------------

!orthonormalize projectors
   CALL locproj_CALC_ORTH_SVD( proj_01 )

!calculate complement
   CALL locproj_CALC_COMPL( proj_01 )

!-----------------------------------------------------------------------------------------------------------------------------------
!Checking
!-----------------------------------------------------------------------------------------------------------------------------------

!check overlap matrices
   CALL locproj_CHECK_OVRLP( proj_01 )


!-----------------------------------------------------------------------------------------------------------------------------------
!Hamiltonian
!-----------------------------------------------------------------------------------------------------------------------------------

!construct hamiltonian
   CALL locproj_SET_H( proj_01 )

!read OUTCAR, we have kpoints this way, always!!!!
   IF (.NOT. use_procar) THEN
      CALL file_outcar_READ( outcar_01 )
   ENDIF
!CALL file_outcar_READ( outcar_01 )
!export hamiltonian
   IF ( .NOT. charge_sc ) THEN
      IF ( use_procar) THEN
         CALL locproj_EXPORT_H_PROCAR( proj_01, procar_01 )	!this will create
         !Hamiltonians on full BZ.
      ELSE
         CALL locproj_EXPORT_H( proj_01, outcar_01 )   !this will create
         !Hamiltonians on full BZ
         CALL locproj_EXPORT_N( proj_01 )   !this will create
         !fermi weights on full BZ
      ENDIF

      CALL locproj_EXPORT_P( proj_01 )

      !TODO: export hamiltonian on kpath

      !1) read eigenval file to have kpoint coordinates of locproj, we have to add k-vectors to eigenval
      !2) read eigenval file of kpath OR calculate k-points according to some file

      !3) this is how we will do it: read KPATH file and calculate path ( difference vector / (n-1) ), just pick the points and copy H(k)

   ENDIF


!charge-selfconsistency

   IF ( charge_sc ) THEN
      CALL calc_GAMMA( proj_01, locproj_01, subspace_locproj_01, outcar_01 )
      !IF ( subspace_locproj_01%is_T0 ) THEN
      !  CALL calc_GAMMA( proj_01, locproj_01, subspace_locproj_01, outcar_01, .TRUE. )	!this works on irreducible BZ only, no static self-energy
      !ELSE
      !  CALL calc_GAMMA( proj_01, locproj_01, subspace_locproj_01, outcar_01, .FALSE. )	!this works on irreducible BZ only, no static self-energy
      !ENDIF
      CALL create_vasplock	!create vasp.lock to tell vasp542 to continue


   ENDIF
!-----------------------------------------------------------------------------------------------------------------------------------

   IF ( rank_0 ) THEN
      WRITE(6,*) 'Waiting for MPI tasks to finish ...'
   ENDIF

   CALL MPI_Barrier(MPI_COMM_WORLD, i_error)
   IF ( rank_0 ) THEN
      WRITE(6,*) 'Done.'
   ENDIF

   CALL MPI_FINALIZE(i_error)

END PROGRAM main
