MODULE parallel
!
!  Purpose:
!    MPI parallelization
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    29/09/11     S. Barthel	Original code
!    28/08/15     S. Barthel	+MPI shared data variables, +double & triple loop parallelization
!    31/08/15     S. Barthel	+MPI subroutine "parallel_set_rank_0"
!

   IMPLICIT NONE

   INCLUDE 'mpif.h'

   INTEGER, DIMENSION(MPI_STATUS_SIZE) :: istatus

   INTEGER :: ilin					!linear index
   INTEGER :: ista, iend
   INTEGER :: ilin_tmp
   INTEGER :: srcnt				!send/receive count

   INTEGER, ALLOCATABLE, DIMENSION(:) :: ircnt, idisp

!===================================================================================================================================
!MPI shared data
!===================================================================================================================================
   INTEGER, SAVE :: nprocs					!total number of processes
   INTEGER, SAVE :: myrank					!rank of each process
   INTEGER, SAVE :: i_error				!mpi error code

   LOGICAL, SAVE :: rank_0

CONTAINS

   SUBROUTINE check_mpi_success

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      IF ( i_error /= 0 ) THEN
         WRITE(6,*) myrank, i_error
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ENDIF

   END SUBROUTINE check_mpi_success

   !=================================================================================================================================

   SUBROUTINE parallel_set_rank_0
      !
      !	Purpose:
      !	  get lazy and safe five key strokes while coding ...
      !
      !	Record of revisions:
      !	    Date       Programmer	Description of change
      !	    ====       ==========	=====================
      !	  31/08/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      IF ( myrank == 0 ) THEN
         rank_0 = .TRUE.
      ELSE
         rank_0 = .FALSE.
      ENDIF

   END SUBROUTINE parallel_set_rank_0

   !=================================================================================================================================

   SUBROUTINE parallel_range(n1, n2, nprocs, irank, ista, iend)
      !
      !	Purpose:
      !	  Calculate range of iterations of a particular process
      !
      !	Record of revisions:
      !	    Date       Programmer	Description of change
      !	    ====       ==========	=====================
      !	  29/09/11     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      INTEGER, INTENT(IN) :: n1		!The lowest value of the iteration variable
      INTEGER, INTENT(IN) :: n2		!The highest value of the iteration variable
      INTEGER, INTENT(IN) :: nprocs		!The number of processes
      INTEGER, INTENT(IN) :: irank		!The rank for which you want to know the range of iterations

      INTEGER, INTENT(OUT) :: ista		!The lowest value of the iteration variable that processes irank executes
      INTEGER, INTENT(OUT) :: iend		!The highest value of the iteration variable that processes irank executes

      INTEGER :: iwork1, iwork2

      iwork1 = (n2 - n1 + 1) / nprocs
      iwork2 = MOD(n2 - n1 + 1, nprocs)
      ista = irank * iwork1 + n1 + MIN(irank, iwork2)
      iend = ista + iwork1 - 1

      IF (iwork2 > irank) THEN
         iend = iend + 1
      ENDIF
    !WRITE(6,*) 'here is par range:', ista, iend,irank
   END SUBROUTINE parallel_range

   !=================================================================================================================================

   SUBROUTINE parallel_lin2sub_double(lin, dim_i, dim_j,  i, j)

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      INTEGER, INTENT(IN) :: lin
      INTEGER, INTENT(IN) :: dim_i, dim_j
      INTEGER, INTENT(OUT) :: i, j

      !map linear index to subscript
      j = INT((lin-1)/dim_i)+1	!slow index (outer loop)
      i = lin - (j-1)*dim_i	!fast index (inner loop)

   END SUBROUTINE parallel_lin2sub_double

   !=================================================================================================================================

   SUBROUTINE parallel_lin2sub_triple(lin, dim_i, dim_j, dim_k, i, j, k)

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units
      INTEGER, INTENT(IN) :: lin
      INTEGER, INTENT(IN) :: dim_i, dim_j, dim_k
      INTEGER, INTENT(OUT) :: i, j, k
      INTEGER :: lin_tmp

      !map linear index to subscript
      k = INT((lin-1)/(dim_i*dim_j))+1		!slowest index(outmost loop)
      lin_tmp = (lin-(k-1)*(dim_i*dim_j))
      j = INT((lin_tmp-1)/dim_i)+1		!slow index (outer loop)
      i = lin_tmp-(j-1)*dim_i			!fast index (inner loop)

   END SUBROUTINE parallel_lin2sub_triple

END MODULE parallel
