MODULE type_file_procar
!
!  Purpose:
!    Import external input
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    10/10/15     S. Barthel	Original code
!

   USE parallel

   IMPLICIT NONE

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units

!===================================================================================================================================

   TYPE :: file_procar !data container for PROCAR

      !PRIVATE                                                 !no access granted to member variables

      INTEGER :: version

      INTEGER :: n_kpoints                                    !Number of k-points, *
      INTEGER :: n_bands                                      !Number of bands, *
      INTEGER :: n_ions                                       !Number of ions, *
      INTEGER :: n_orbitals                                   !Number of orbitals sum_L (2*L+1) (s,p,d,f), *
      INTEGER :: n_spins                                      !Number of spins in the PROCAR

      LOGICAL :: is_reciprocal                                !k-point coordinates are given wrt. rec. basis-vectors

      REAL, ALLOCATABLE, DIMENSION(:,:) :: k_vector           !Coordinates of k-points k(xyz,k) in units of VASP k-mesh, see POSCAR
      REAL, ALLOCATABLE, DIMENSION(:) :: k_weight             !weight of k-points
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: E_nks            !Eigenenergies E(n,k,s) in eV
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: N_nks            !Occupancies N(n,k,s)

      REAL, ALLOCATABLE, DIMENSION(:,:,:,:,:) :: magnitude_sum      !Absolute value of the projectors |<L,µ|n,k,s>|²
      REAL, ALLOCATABLE, DIMENSION(:,:,:,:,:) :: phase_sum_real     !Re{ <L,µ|n,k,s> } of phase vector
      REAL, ALLOCATABLE, DIMENSION(:,:,:,:,:) :: phase_sum_imag     !Im{ <L,µ|n,k,s> } of phase vector

      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:,:) :: P       !projector matrix <L,µ|n,k,s> WITHOUT phase-factors (VASP)
      COMPLEX, ALLOCATABLE, DIMENSION(:,:) :: PPHASE        !phase-factors which are missing in VASP

      REAL :: tol_proj = 1.0D-15                            !tolerance: zero projectors

   END TYPE file_procar

!===================================================================================================================================

CONTAINS

   SUBROUTINE file_procar_READ( procar, poscar, version, is_reciprocal )
      !
      !  Purpose:
      !    Load a PROCAR file
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    28/10/15     S. Barthel	Original code
      !    10.12.15     -"-               added support for vasp.5.4.2 (build 02Dez)
      !

      USE type_file_poscar

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_procar ), INTENT(INOUT) :: procar
      TYPE( file_poscar ), INTENT(IN) :: poscar

      INTEGER,  INTENT(IN) :: version

      LOGICAL, INTENT(IN) :: is_reciprocal

      INTEGER :: spin, kpoint, band, ion, orbital			!loop-indices

      INTEGER :: ioerror, allocerror				!error codes for allocation and file IO

      CHARACTER(35) :: f_procartype				!PROCAR file type
      CHARACTER(122) :: f_orbitalbasis				!orbital description

      REAL :: buffer						!buffer to read unused data
      INTEGER :: kpoint_read, band_read, ion_read			!loop indices read from PROCAR

      INTEGER :: nmax_kpoints, nmax_bands, nmax_ions

      CHARACTER(len=32), DIMENSION(6) :: fmt_procar

      !===============================================================================================================================

      !original format descriptors as of vasp.5.4.1 24Jun15, copied 11-12-2015

      !3200 FORMAT('# of k-points:',I5,9X,'# of bands:',I4,9X,'# of ions:',I4)	!ok, is used
      !3201 FORMAT(/' k-point ',I4,' :',3X,3F11.8,'     weight = ',F10.8/)	!ok, is used
      !3203 FORMAT('band ',I3,' # energy',F14.8,' # occ.',F12.8)			!ok, is used

      !unused format descriptors (either skipped or read in free format)

      !3204 FORMAT(I3,17(1X,F6.3))
      !3205 FORMAT('tot',17(1X,F6.3)/)

      !used format descriptors with text replaced by X (ensures compatibility with GNU compilers), 11-12-2015

54100 FORMAT(14X,I5,9X,11X,I4,9X,10X,I4)	!ok	!kpoints, bands, ions
54101 FORMAT(/9X,I4,5X,3F11.8,14X,F10.8/)	!ok	!kpoint, kvec, weight
54103 FORMAT(5X,I3,9X,F14.8,7X,F12.8)	!ok	!band, energy, occ

      ! Stefans version
      fmt_procar(1) = '(14X,I5,9X,11X,I4,9X,10X,I4)'
      fmt_procar(2) = '(/9X,I4,5X,3F11.8,14X,F10.8/)'
      fmt_procar(3) = '(5X,I3,9X,F14.8,7X,F12.8)'

      ! Bins version (should be standard)
      !fmt_procar(1) = '(14X,I5,9X,11X,I5,9X,10X,I4)'
      !fmt_procar(2) = '(/9X,I4,5X,3F13.8,14X,F10.8/)'
      !fmt_procar(3) = '(5X,I3,9X,F14.8,7X,F12.8)'

      !-------------------------------------------------------------------------------------------------------------------------------

      !original format descriptors as of vasp.5.4.2 (build 02Dez), copied 10-12-2015

      !3200 FORMAT('# of k-points:',I5,9X,'# of bands:',I5,9X,'# of ions:',I5)	!ok, is used
      !3201 FORMAT(/' k-point ',I5,' :',3X,3F11.8,'     weight = ',F10.8/)	!ok, is used
      !3203 FORMAT('band ',I5,' # energy',F14.8,' # occ.',F12.8)			!ok, is used

      !unused format descriptors (either skipped or read in free format)

      !3204 FORMAT(I5,17(1X,F6.3))
      !3214 FORMAT(I5,17(1X,F6.3,1X,F6.3,1X))
      !3205 FORMAT('tot  ',17(1X,F6.3)/)
      !3215 FORMAT('charge',17(F6.3,1X,'      ',2X)/)

      !used format descriptors with text replaced by X (ensures compatibility with GNU compilers), 10-12-2015

54200 FORMAT(14X,I5,9X,11X,I5,9X,10X,I5)	!ok	!kpoints, bands, ions
54201 FORMAT(/9X,I5,5X,3F11.8,14X,F10.8/)	!ok	!kpoint, kvec, weight
54203 FORMAT(5X,I5,9X,F14.8,7X,F12.8)	!ok	!band, energy, occ

      fmt_procar(4) = '(14X,I5,9X,11X,I5,9X,10X,I5)'
      fmt_procar(5) = '(/9X,I5,5X,3F11.8,14X,F10.8/)'
      fmt_procar(6) = '(5X,I5,9X,F14.8,7X,F12.8)'

      !===============================================================================================================================

2200  FORMAT(3X,'PROCAR lm decomposed + phase data present:'/ &
         ,3X,'->',2X,I5,17X,'k-points  (k)'/&
         ,3X,'->',3X,I4,17X,'bands     (n)'/&
         ,3X,'->',3X,I4,17X,'ions      (µ)'/&
         ,3X,'->',3X,I4,17X,'orbitals  (L)'/&
         ,3X,'->',3X,I4,17X,'spins     (s)')

      !===============================================================================================================================

      !do we use reciprocal or cartesian k-points ?
      procar%is_reciprocal = is_reciprocal
      IF ( procar%is_reciprocal ) THEN
         WRITE(6,*) '   k-points in reciprocal coordinates (units of b1,b2,b3)'
      ELSE
         WRITE(6,*) '   k-points in cartesian coordinates (units of 2pi)'
      ENDIF

      !choose format
      procar%version = version
      IF ( procar%version == 541 ) THEN
         procar%version = 0
      ELSEIF( procar%version == 542 ) THEN
         procar%version = 3
      ELSE
         WRITE(6,*) 'ERROR: procar%version not supported'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ENDIF

      !broadcast header
      srcnt = 1
      CALL MPI_BCAST(procar%version,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      !---

      IF ( rank_0 ) THEN

         WRITE(6,*) '   rank 0: READING header of PROCAR ...'

         !check if PROCAR exists
         OPEN (UNIT=26,FILE='PROCAR',STATUS='OLD', ACTION='READ', IOSTAT=ioerror)

         IF ( ioerror /= 0 ) THEN
            WRITE(6,*) 'ERROR: PROCAR file missing'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         !check if PROCAR is of correct type
         READ(26,'(A)') f_procartype
         IF ( INDEX(f_procartype,'PROCAR lm decomposed + phase') /= 1 ) THEN
            WRITE(6,*) 'ERROR: PROCAR lm decomposed + phase data missing'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         !read complete file to see if we have magnetic PROCAR
         DO spin=1,2 !assume magnetic PROCAR

            !---------------------------------------------------------------------------------------------------------------------------
            !header
            !---------------------------------------------------------------------------------------------------------------------------
            READ(26,fmt_procar(1+procar%version),IOSTAT=ioerror) procar%n_kpoints, procar%n_bands, procar%n_ions


            !check if we have magnetic PROCAR (can we get ISPIN=2 without LSDA, i.e. LDA+U but magnetism ?)
            IF ( ioerror == 0 ) THEN
               procar%n_spins = spin
            ELSE
               EXIT
            ENDIF

            !check actual limits due to format descriptors
            IF ( procar%version == 0 ) THEN
               nmax_kpoints = 99999
               nmax_bands = 9999
               nmax_ions = 9999
            ELSEIF ( procar%version == 3 ) THEN
               nmax_kpoints = 99999
               nmax_bands = 99999
               nmax_ions = 99999
            ELSE
               WRITE(6,*) 'ERROR: PROCAR incompatible wrt. VASP version'
               CALL MPI_Abort(MPI_COMM_WORLD, i_error)
               STOP
            ENDIF

            IF ( (procar%n_kpoints > nmax_kpoints) .OR. (procar%n_bands >= nmax_bands) .OR. (procar%n_ions >= nmax_ions) ) THEN
               WRITE(6,*) 'ERROR: Too many kpoints/bands/ions to correctly read in !'
               CALL MPI_Abort(MPI_COMM_WORLD, i_error)
               STOP
            ENDIF

            DO kpoint=1,procar%n_kpoints

               !-------------------------------------------------------------------------------------------------------------------------
               !k-vector and k-weight
               !-------------------------------------------------------------------------------------------------------------------------
               READ(26,*)		!skip empty line
               READ(26,*)		!skip k-vector and k-weight
               READ(26,*)		!skip empty line

               DO band=1,procar%n_bands

                  !-----------------------------------------------------------------------------------------------------------------------
                  !band-index, eigenvalue, occupation
                  !-----------------------------------------------------------------------------------------------------------------------
                  READ(26,*)		!skip band-index, eigenvalue, occupation
                  READ(26,*)		!skip empty line

                  IF ( (kpoint == 1) .AND. (band == 1) ) THEN
                     READ(26,'(A)',IOSTAT=ioerror) f_orbitalbasis	  !read s-p-d-f header

                     IF ( (ioerror /= 0) ) THEN
                        WRITE(6,*) 'ERROR: f_orbitalbasis'
                        WRITE(6,*) 'ERROR: PROCAR file corrupted/incompatible !'
                        CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                        STOP
                     ENDIF

                     !according to Vasp source code each orbital has a char width of 7 chars - 10 offset
                     procar%n_orbitals = (len_trim(f_orbitalbasis)-10)/7

                  ELSE
                     READ(26,*)	!skip s-p-d-f header
                  ENDIF

                  !-----------------------------------------------------------------------------------------------------------------------
                  !magnitudes
                  !-----------------------------------------------------------------------------------------------------------------------
                  DO ion=1,procar%n_ions
                     READ(26,*)	!skip orbital magnitudes
                  ENDDO
                  READ(26,*)		!skip total magnitude
                  IF ( procar%n_orbitals == 16 ) THEN	!In case of f-electrons an additional line has to be skipped manually here
                     READ(26,*)
                  ENDIF

                  !-----------------------------------------------------------------------------------------------------------------------
                  !phases: this is different for vasp.5.4.1 vs. vasp 5.4.2
                  !-----------------------------------------------------------------------------------------------------------------------
                  READ(26,*)  	!skip s-p-d-f header

                  IF ( procar%version == 0 ) THEN

                     DO ion=1,procar%n_ions
                        READ(26,*)	!skip real
                        READ(26,*)	!skip imag
                     ENDDO

                  ELSEIF ( procar%version == 3 ) THEN

                     DO ion=1,procar%n_ions
                        READ(26,*)	!skip real, imag, total
                     ENDDO
                     READ(26,*)	!skip total charge
                     IF ( procar%n_orbitals == 16 ) THEN	!In case of f-electrons an additional line has to be skipped manually here
                        READ(26,*)
                     ENDIF

                  ELSE
                     WRITE(6,*) 'ERROR: PROCAR incompatible wrt. VASP version'
                     CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                     STOP
                  ENDIF

                  READ(26,*)		!skip empty line

               ENDDO		!band

            ENDDO		!kpoint

         ENDDO		!spin

         !close file
         CLOSE(26)

      ENDIF

      !broadcast header
      srcnt = 1
      CALL MPI_BCAST(procar%n_kpoints,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(procar%n_bands,    srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(procar%n_ions,     srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(procar%n_orbitals, srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(procar%n_spins,    srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      !stop if data is unreasonable
      IF ( (procar%n_kpoints == 0) .OR. (procar%n_bands == 0) .OR. (procar%n_ions == 0) .OR. (procar%n_spins == 0) ) THEN
         WRITE(6,*) 'ERROR: PROCAR file corrupted/incompatible !'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSEIF ( rank_0 ) THEN
         WRITE(6,2200) procar%n_kpoints, procar%n_bands, procar%n_ions, procar%n_orbitals, procar%n_spins
      ENDIF

      !-------------------------------------------------------------------------------------------------------------------------------

      !allocate
      ALLOCATE ( procar%k_vector(3,procar%n_kpoints), procar%k_weight(procar%n_kpoints), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "k_vector,k_weight" could not be allocated'
         STOP
      ELSE
         procar%k_vector = 0.
         procar%k_weight = 0.
      ENDIF

      ALLOCATE ( procar%E_nks(procar%n_bands,procar%n_kpoints,procar%n_spins), &
         procar%N_nks(procar%n_bands,procar%n_kpoints,procar%n_spins), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "E_nks,N_nks" could not be allocated'
         STOP
      ELSE
         procar%E_nks = 0.
         procar%N_nks = 0.
      ENDIF

      ALLOCATE ( procar%magnitude_sum(procar%n_orbitals, procar%n_ions, procar%n_bands, procar%n_kpoints, procar%n_spins), &
         procar%phase_sum_real(procar%n_orbitals, procar%n_ions, procar%n_bands, procar%n_kpoints, procar%n_spins), &
         procar%phase_sum_imag(procar%n_orbitals, procar%n_ions, procar%n_bands, procar%n_kpoints, procar%n_spins),&
          STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "magnitude_sum,phase_sum_real,phase_sum_imag" could not be allocated'
         STOP
      ELSE
         procar%magnitude_sum = 0.
         procar%phase_sum_real = 0.
         procar%phase_sum_imag = 0.
      ENDIF

      !read data
      IF ( rank_0 ) THEN

         WRITE(6,*) '   rank 0: READING data of PROCAR ...'

         !check if PROCAR exists
         OPEN (UNIT=26,FILE='PROCAR',STATUS='OLD', ACTION='READ', IOSTAT=ioerror)

         IF ( ioerror /= 0 ) THEN
            WRITE(6,*) 'ERROR: PROCAR file missing'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         READ(26,*)	!skip PROCAR-type

         DO spin=1,procar%n_spins

            READ(26,*)	!skip header

            DO kpoint=1,procar%n_kpoints

               !-------------------------------------------------------------------------------------------------------------------------
               !k-vector and k-weight
               !-------------------------------------------------------------------------------------------------------------------------
               READ(26,fmt_procar(2+procar%version),IOSTAT=ioerror) kpoint_read, procar%k_vector(:,kpoint), procar%k_weight(kpoint)	!read k-vector and k-weight
               IF ( (ioerror /= 0) .OR. (kpoint /= kpoint_read) ) THEN
                  WRITE(6,*) 'ERROR: kpoint_read, procar%k_vector(:,kpoint), procar%k_weight(kpoint)'
                  WRITE(6,*) 'ERROR: PROCAR file corrupted/incompatible'
                  CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                  STOP
               ENDIF

               IF ( procar%is_reciprocal ) THEN

                  !Evaluate reciprocal coordinates (units of b1,b2,b3, no 2*pi in B !)
                  procar%k_vector(:,kpoint) = procar%k_vector(1,kpoint)*poscar%lattice_B(:,1) &
                     + procar%k_vector(2,kpoint)*poscar%lattice_B(:,2) &
                     + procar%k_vector(3,kpoint)*poscar%lattice_B(:,3)

               ENDIF

               DO band=1,procar%n_bands

                  !-----------------------------------------------------------------------------------------------------------------------
                  !band-index, eigenvalue, occupation
                  !-----------------------------------------------------------------------------------------------------------------------
                  READ(26,fmt_procar(3+procar%version),IOSTAT=ioerror) band_read, procar%E_nks(band,kpoint,spin), &
                     procar%N_nks(band,kpoint,spin)	!read band-index, eigenvalue, occupation
                  READ(26,*)		!skip empty line
                  IF ( (ioerror /= 0) .OR. (band /= band_read) ) THEN
                     WRITE(6,*) 'ERROR: band_read, procar%E_nks(band,kpoint,spin), procar%N_nks(band,kpoint,spin))'
                     WRITE(6,*) 'ERROR: PROCAR file corrupted/incompatible'
                     CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                     STOP
                  ENDIF

                  !TODO: check if procar%N_nks(band,kpoint,spin) is reasonable

                  READ(26,*)	!skip s-p-d-f header

                  !-----------------------------------------------------------------------------------------------------------------------
                  !magnitudes
                  !-----------------------------------------------------------------------------------------------------------------------
                  DO ion=1,procar%n_ions

                     READ(26,*,IOSTAT=ioerror) ion_read, procar%magnitude_sum(1:procar%n_orbitals,ion,band,kpoint,spin), buffer !read orbital magnitudes
                     IF ( (ioerror /= 0) .OR. (ion /= ion_read) ) THEN
                        WRITE(6,*) 'ERROR: ion_read, procar%magnitude_sum(1:procar%n_orbitals,ion,band,kpoint,spin), buffer'
                        WRITE(6,*) 'ERROR: PROCAR file corrupted/incompatible'
                        CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                        STOP
                     ENDIF

                  ENDDO

                  READ(26,*)		!skip total magnitude
                  IF ( procar%n_orbitals == 16 ) THEN	!In case of f-electrons an additional line has to be skipped manually here
                     READ(26,*)
                  ENDIF

                  !TODO: check if there is band with zero total orbital charcter

                  !-----------------------------------------------------------------------------------------------------------------------
                  !!phases: this is different for vasp.5.4.1 vs. vasp 5.4.2
                  !-----------------------------------------------------------------------------------------------------------------------
                  READ(26,*)  	!skip s-p-d-f header

                  IF ( procar%version == 0 ) THEN

                     DO ion=1,procar%n_ions
                        READ(26,*,IOSTAT=ioerror) ion_read, ( procar%phase_sum_real(orbital,ion,band,kpoint,spin),&
                         orbital=1,procar%n_orbitals )
                        IF ( (ioerror /= 0) .OR. (ion /= ion_read) ) THEN
                           WRITE(6,*) 'ERROR: ion_read, ( procar%phase_sum_real(orbital,ion,band,kpoint,spin)'
                           WRITE(6,*) 'orbital=1,procar%n_orbitals )'
                           WRITE(6,*) 'ERROR: PROCAR file corrupted/incompatible'
                           CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                           STOP
                        ENDIF
                        READ(26,*,IOSTAT=ioerror) ion_read, ( procar%phase_sum_imag(orbital,ion,band,kpoint,spin),&
                         orbital=1,procar%n_orbitals )
                        IF ( (ioerror /= 0) .OR. (ion /= ion_read) ) THEN
                           WRITE(6,*) 'ERROR: ion_read, ( procar%phase_sum_imag(orbital,ion,band,kpoint,spin),'
                           WRITE(6,*) 'orbital=1,procar%n_orbitals )'
                           WRITE(6,*) 'ERROR: PROCAR file corrupted/incompatible'
                           CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                           STOP
                        ENDIF
                     ENDDO

                  ELSEIF ( procar%version == 3 ) THEN

                     DO ion=1,procar%n_ions
                        READ(26,*,IOSTAT=ioerror) ion_read, &
                           ( procar%phase_sum_real(orbital,ion,band,kpoint,spin), &
                           procar%phase_sum_imag(orbital,ion,band,kpoint,spin), orbital=1,procar%n_orbitals ), buffer
                        IF ( (ioerror /= 0) .OR. (ion /= ion_read) ) THEN
                           WRITE(6,*) 'ERROR: ion_read, (pK_sum_real(orbital,ion,band,kpoint),'
                           WRITE(6,*) ' pK_sum_imag(orbital,ion,band,kpoint) ,orbital=1,n_orbitals), buffer'
                           WRITE(6,*) 'ERROR: PROCAR file corrupted/incompatible'
                           CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                           STOP
                        ENDIF
                     ENDDO
                     READ(26,*)		!skip total charge
                     IF ( procar%n_orbitals == 16 ) THEN	!In case of f-electrons an additional line has to be skipped manually here
                        READ(26,*)
                     ENDIF

                  ELSE
                     WRITE(6,*) 'ERROR: PROCAR incompatible wrt. VASP version'
                     CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                     STOP
                  ENDIF

                  READ(26,*)		!skip empty line

               ENDDO		!band

            ENDDO		!kpoint

         ENDDO		!spin

         !close file
         CLOSE(26)

      ENDIF

      !broadcast data
      srcnt = 3*procar%n_kpoints
      CALL MPI_BCAST(procar%k_vector,           srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      srcnt = procar%n_kpoints
      CALL MPI_BCAST(procar%k_weight,           srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      srcnt = procar%n_bands*procar%n_kpoints*procar%n_spins
      CALL MPI_BCAST(procar%E_nks,              srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(procar%N_nks,              srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      srcnt = procar%n_orbitals*procar%n_ions*procar%n_bands*procar%n_kpoints*procar%n_spins
      CALL MPI_BCAST(procar%magnitude_sum,      srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(procar%phase_sum_real,     srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(procar%phase_sum_imag,     srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      IF ( rank_0 ) THEN
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE file_procar_READ

   !=================================================================================================================================

   SUBROUTINE file_procar_SET_P( procar )
      !
      !  Purpose:
      !    Load a PROCAR file
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    28/10/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_procar ), INTENT(INOUT) :: procar

      INTEGER :: allocerror                                     !error code for allocation
      INTEGER :: orbital, ion, band, kpoint, spin               !loop indices

      REAL :: norm_P                                            !norm of <L,µ|n,k,s>
      REAL :: norm_phase                                        !norm of <L,µ|n,k,s> phase vectors

      !-------------------------------------------------------------------------------------------------------------------------------

      ALLOCATE ( procar%P(procar%n_orbitals, procar%n_ions, procar%n_bands, procar%n_kpoints, procar%n_spins), &
         STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "PRAW" could not be allocated'
         STOP
      ELSE
         procar%P = CMPLX(0.,0.)
      ENDIF

      !build raw projectors
      DO spin=1,procar%n_spins
         DO kpoint=1,procar%n_kpoints
            DO band=1,procar%n_bands
               DO ion=1,procar%n_ions
                  DO orbital=1,procar%n_orbitals

                     IF ( procar%version == 0 ) THEN		!phase-vector is mingled and EXP(ikR) is missing

                        norm_P = SQRT(procar%magnitude_sum(orbital,ion,band,kpoint,spin))

                        norm_phase = SQRT(procar%phase_sum_real(orbital,ion,band,kpoint,spin)**2 &
                           + procar%phase_sum_imag(orbital,ion,band,kpoint,spin)**2)

                        IF ( (norm_P>procar%tol_proj) .AND. (norm_phase>procar%tol_proj) ) THEN
                           procar%P(orbital,ion,band,kpoint,spin) = norm_P &
                              * CMPLX( procar%phase_sum_real(orbital,ion,band,kpoint,spin),&
                               procar%phase_sum_imag(orbital,ion,band,kpoint,spin) ) &
                              / norm_phase
                        ENDIF

                     ELSEIF ( procar%version == 3 ) THEN	!phase-vector is LCAO coefficient, but EXP(ikR) is still missing )

                        procar%P(orbital,ion,band,kpoint,spin) = &
                           CMPLX( procar%phase_sum_real(orbital,ion,band,kpoint,spin), &
                           procar%phase_sum_imag(orbital,ion,band,kpoint,spin) )

                     ELSE
                        WRITE(6,*) 'ERROR: file_procar_SET_P incompatible wrt. VASP version'
                        CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                        STOP
                     ENDIF

                  ENDDO
               ENDDO
            ENDDO
         ENDDO
      ENDDO

      IF ( rank_0 ) THEN
         WRITE(6,*) '   P set from PROCAR'
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE file_procar_SET_P

   !=================================================================================================================================

   SUBROUTINE file_procar_EXPORT_P( procar )
      !
      !  Purpose:
      !    Load a PROCAR file
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    28/10/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_procar ), INTENT(IN) :: procar

      INTEGER :: ioerror
      INTEGER :: orbital, ion, band, kpoint, spin               !loop indices

      REAL :: tmp_real, tmp_imag, abs_val2

      !-------------------------------------------------------------------------------------------------------------------------------

1000  FORMAT(1X,'spin:',1X,I4,1X,'k-point:',1X,I4)
1001  FORMAT(1X,'band:',1X,I4)
1002  FORMAT(1X,'ion:',1X,I4)
1003  FORMAT(1X,I4,4X,F19.15,4X,F19.15,4X,F19.15)

      IF ( rank_0 ) THEN

         WRITE(6,*) '  Exporting _PROCAR_P ...'

         OPEN (UNIT=26,FILE='./_PROCAR_P.txt',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)

         !build raw projectors
         DO spin=1,procar%n_spins
            DO kpoint=1,procar%n_kpoints
               WRITE(26,1000) spin, kpoint
               DO band=1,procar%n_bands
                  WRITE(26,1001) band
                  DO ion=1,procar%n_ions
                     WRITE(26,1002) ion
                     DO orbital=1,procar%n_orbitals

                        tmp_real = REAL( procar%P(orbital,ion,band,kpoint,spin) )
                        tmp_imag = AIMAG( procar%P(orbital,ion,band,kpoint,spin) )
                        abs_val2 = tmp_real**2 + tmp_imag**2

                        WRITE(26,1003) orbital, tmp_real, tmp_imag, abs_val2

                     ENDDO
                  ENDDO
               ENDDO
            ENDDO
         ENDDO

         CLOSE(26)

      ENDIF

   END SUBROUTINE file_procar_EXPORT_P

   !=================================================================================================================================

   SUBROUTINE file_procar_SET_PPHASE( procar, poscar )
      !
      !  Purpose:
      !    Load a PROCAR file
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    28/10/15     S. Barthel	Original code
      !

      USE constants
      USE type_file_poscar

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_procar ), INTENT(INOUT) :: procar
      TYPE( file_poscar ), INTENT(IN) :: poscar

      INTEGER :: ioerror, allocerror                !error code for allocation

      INTEGER :: ion, kpoint               !loop indices

      !-------------------------------------------------------------------------------------------------------------------------------

      !
!     IF ( procar%is_reciprocal ) THEN
!       WRITE(6,*) 'ERROR: Cannot calculate correct phase-vectors, TODO ...'
!       WRITE(6,*) '       k-points are reciprocal, atomic positions are cartesian'
!       CALL MPI_Abort(MPI_COMM_WORLD, i_error)
!       STOP
!     ENDIF

      ALLOCATE ( procar%PPHASE(procar%n_ions, procar%n_kpoints), &
         STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "PPHASE" could not be allocated'
         STOP
      ELSE
         procar%PPHASE = CMPLX(0.,0.)
      ENDIF

      DO kpoint=1,procar%n_kpoints
         DO ion=1,procar%n_ions

            procar%PPHASE(ion,kpoint) = EXP( CMPLX(0.,2*PI*SUM(procar%k_vector(:,kpoint)*poscar%atomic_positions(:,ion))) )

         ENDDO
      ENDDO

      IF ( rank_0 ) THEN
         WRITE(6,*) '   PPHASE set from PROCAR & POSCAR'
         WRITE(6,*) '-'

         !write kpoints
         !OPEN (UNIT=26,FILE='./_PHAS_PROCAR.txt',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
         !DO kpoint=1,procar%n_kpoints
         !   DO ion=1,procar%n_ions
! 	  WRITE(26,"(3X,2(2X,I5),7(2X,F14.10))") ion, kpoint, procar%k_vector(:,kpoint), poscar%atomic_positions(:,ion), &
! 	    SUM(procar%k_vector(:,kpoint)*poscar%atomic_positions(:,ion))

         !      WRITE(26,"(3X,2(2X,I5),8(2X,F14.10))") ion, kpoint, procar%k_vector(:,kpoint), poscar%atomic_positions(:,ion), &
         !         REAL(procar%PPHASE(ion,kpoint)), AIMAG(procar%PPHASE(ion,kpoint))
         !   ENDDO
         !ENDDO
         !CLOSE(26)

      ENDIF

   END SUBROUTINE file_procar_SET_PPHASE

   !=================================================================================================================================

   SUBROUTINE file_procar_SYM_P_COORDS( procar, coords )
      !
      !  Purpose:
      !    Rotate s,p,d-orbitals using point-group symmetries, VASP order assumed
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    03/11/15     S. Barthel	Original code
      !

      USE type_file_coords
      USE math
      USE symmetries

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_procar ), INTENT(INOUT) :: procar
      TYPE( file_coords ), INTENT(IN) :: coords

      INTEGER :: allocerror				!error code for allocation
      INTEGER :: orbital, ion, ion_, band, kpoint, spin		!loop indices

      REAL, DIMENSION(3,3) :: inv_T_local
      REAL, DIMENSION(9,9) :: inv_L_spd       !Inverse transformation matrix of spd-orbitals into new local coordinate system

      COMPLEX, DIMENSION(procar%n_orbitals, procar%n_ions, procar%n_bands, procar%n_kpoints, procar%n_spins) :: PWORK

      !-------------------------------------------------------------------------------------------------------------------------------

      PWORK = CMPLX(0.,0.)

      !loops (local)
      DO spin=1,procar%n_spins
         DO kpoint=1,procar%n_kpoints
            DO band=1,procar%n_bands
               DO ion=1,procar%n_ions

                  !calculate inverse T^{-1}
                  inv_T_local = coords%T_local(:,:,ion)	!storage in columns
                  CALL real_matrix_inverse(3, inv_T_local )	!storage in rows

                  !calculate inverse L^{-1}
                  inv_L_spd = inv_T_spd( inv_T_local )

                  DO orbital=1,procar%n_orbitals
                     !x' = T^{-1}x wrt. orbitals
                     PWORK(orbital,ion,band,kpoint,spin) = PWORK(orbital,ion,band,kpoint,spin) &
                        + SUM( inv_L_spd(orbital,:) * procar%P(:,ion,band,kpoint,spin) )
                  ENDDO

               ENDDO
            ENDDO
         ENDDO
      ENDDO

      procar%P = PWORK

      IF ( rank_0 ) THEN
         WRITE(6,*) '   P rotated to COORDS (untested, there is a bug concerning the orbital ordering ... check for unit matrix)'
         WRITE(6,*) '   Did we consider the vasp ordering in all equations ?)'
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE file_procar_SYM_P_COORDS


END MODULE type_file_procar
