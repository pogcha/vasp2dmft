MODULE type_locproj
   !
   !  Purpose:
   !    Class implementing Projectors
   !  Record of revisions:
   !      Date       Programmer	  Description of change
   !      ====       ==========	  =====================
   !    01/12/15     S. Barthel	  Original code
   !

   !USE constants
   USE math
   USE parallel

   IMPLICIT NONE

   ! Data dictionary: declare constants

   ! Data dictionary: type definitions

   !---------------------------------------------------------------------------------------------------------------------------------

   TYPE :: class_locproj

      !PRIVATE                   !no access granted to member variables

      INTEGER :: n_spins
      INTEGER :: n_kpoints
      INTEGER :: n_bands
      INTEGER :: n_orbitals     !this is a combined linear index of L, µ

      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: P			!projector matrix <p:L|n,k,s>

      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: ovrlp_L			!overlap matrix <L|L'>(k,s)
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: ovrlp_N			!overlap matrix <n|n'>(k,s)

      LOGICAL :: is_unity_L = .TRUE.
      LOGICAL :: is_unity_N = .TRUE.

      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: inv_sqrt_ovrlp_L	!inverse square-root of overlap-matrix: (<L|L'>(k,s))^{-1/2}
      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: sqrt_ovrlp_L		!square-root of overlap-matrix: (<L|L'>(k,s))^{1/2}

      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: E			!dft-eigenvalues E(n,k,s) on band-window
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: N				!dft-occupations N(n,k,s) on band-window

      INTEGER :: N_tot							!total number of electrons (both spins)

      COMPLEX, ALLOCATABLE, DIMENSION(:,:,:,:) :: H			!hamiltonian <L|H|L'>(k,s)

      REAL :: tol_svd = 1.0D-14					!tolerance: singular values
      REAL :: tol_ovl = 1.0D-13					!tolerance: overlap matrices

      !needed for charge self-consistency (avoid to pass procar,locproj and subspace classes)
      INTEGER :: n_bands_dft					!number of bands in original VASP calculation
      INTEGER, ALLOCATABLE, DIMENSION(:) :: band_indices		!list of selected band indices in band-subspace, INIT from subspace

      CHARACTER(7) :: hk_fmt


      INTEGER :: n_shells                                       ! number of atomic shells
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: shell_info		! information about shells (which atom, type, L, dimension)

      INTEGER :: n_corr_shells                                  ! number of correlated atomic shells
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: corr_shells_info	! information about shells (which atom, type, L, dimension, SO, interactions)

      INTEGER :: n_irreps
      INTEGER, ALLOCATABLE, DIMENSION(:) :: irrep_info		    ! information about irreps (number of irreducible representations, and the dimensions of the irreps)



   END TYPE class_locproj

   !---------------------------------------------------------------------------------------------------------------------------------

CONTAINS

   !===============================================================================================================================
   !High-level routines
   !===============================================================================================================================

   !PROCAR

   SUBROUTINE class_locproj_SET_P_FROM_PROCAR( proj, procar, subspace )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    29/10/15     S. Barthel	Original code
      !

      USE type_file_subspace_procar
      USE type_file_procar

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(INOUT) :: proj

      TYPE( file_procar ), INTENT(IN) :: procar

      TYPE( file_subspace_procar ), INTENT(IN) :: subspace


      INTEGER :: ioerror, allocerror                      !error codes for allocation and file IO
      INTEGER :: orbital, band, kpoint, spin              !loop indices
      INTEGER :: orbital_, orbital__, ion_, ion__, band_, kpoint_, spin_        !mapped loop indices

      REAL :: norm_mag, norm_phas

      !-----------------------------------------------------------------------------------------------------------------------------

      !allocate

      IF ( subspace%hk_fmt == 'triqs' ) THEN
         CALL class_locproj_INIT_triqs( proj, subspace%n_orbitals, subspace%n_bands,&
                                        procar%n_kpoints,  procar%n_spins, subspace%hk_fmt,&
                                        subspace%n_shells, subspace%n_corr_shells, &
                                        subspace%shell_info, subspace%corr_shells_info, &
                                        subspace%n_irreps, subspace%irrep_info )!square P
      ELSE

         CALL class_locproj_INIT_wannier( proj, subspace%n_orbitals, subspace%n_bands,&
                                          procar%n_kpoints,  procar%n_spins, subspace%hk_fmt )!square P

      ENDIF


      !fill in data
      DO spin=1,proj%n_spins
         DO kpoint=1,proj%n_kpoints
            DO band=1,proj%n_bands

               !band-map
               band_ = subspace%band_indices(band)

               !dft-eigenvalues
               proj%E(band, band, kpoint, spin) = CMPLX(procar%E_nks(band_,kpoint,spin),0.)

               !dft-occupations
               proj%N(band, kpoint, spin) = CMPLX(procar%N_nks(band_,kpoint,spin),0.)

               !projectors
               orbital = 0	!linear indexing wrt- orbitals and ions
               DO ion__=1,subspace%n_ions
                  DO orbital__=1,subspace%L(ion__)%n_orbitals

                     ion_ = subspace%L(ion__)%ion
                     orbital_ = subspace%L(ion__)%orbital(orbital__)
                     orbital = orbital + 1

                     !full
                     proj%P( orbital, band, kpoint, spin ) =   procar%PPHASE(ion_,kpoint) * procar%P(orbital_,ion_,&
                                                                                                        band_,kpoint,spin)

                     !proj%P( orbital, band, kpoint, spin ) =   procar%P(orbital_,ion_,band_,kpoint,spin)

                  ENDDO
               ENDDO

            ENDDO
         ENDDO
      ENDDO

      proj%N_tot = NINT( REAL( SUM(proj%N) ) / ( proj%n_kpoints*proj%n_spins ) )

      IF ( rank_0 ) THEN
         WRITE(6,*) '   PROJ set from PROCAR & SUBSPACE'
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE class_locproj_SET_P_FROM_PROCAR

   !LOCPROJ

   SUBROUTINE class_locproj_SET_P_FROM_LOCPROJ( proj, locproj, subspace )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    01/12/15     S. Barthel	Original code
      !

      USE type_file_locproj
      USE type_file_subspace_locproj

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(INOUT) :: proj
      TYPE( file_locproj ), INTENT(IN) :: locproj
      TYPE( file_subspace_locproj ), INTENT(IN) :: subspace

      INTEGER :: ioerror, allocerror                      !error codes for allocation and file IO
      INTEGER :: orbital, band, kpoint, spin              !loop indices
      INTEGER :: orbital_, orbital__, ion_, ion__, band_, kpoint_, spin_        !mapped loop indices

      !-----------------------------------------------------------------------------------------------------------------------------

 
      !allocate
      if ( subspace%hk_fmt == 'triqs' ) THEN
         CALL class_locproj_INIT_triqs( proj, locproj%n_orbitals, subspace%n_bands,&
                                        locproj%n_kpoints,  locproj%n_spins, subspace%hk_fmt, &
                                        subspace%n_shells, subspace%n_corr_shells,&
                                        subspace%shell_info, subspace%corr_shells_info, &
                                        subspace%n_irreps, subspace%irrep_info )	!square P
      ELSE
         CALL class_locproj_INIT_wannier( proj, locproj%n_orbitals, subspace%n_bands, &
                                          locproj%n_kpoints,  locproj%n_spins, subspace%hk_fmt )!square P
      END IF

      !fill in data
      DO spin=1,proj%n_spins
         DO kpoint=1,proj%n_kpoints

            DO band=1,proj%n_bands

               !band-map
               band_ = subspace%band_indices(band)

               !dft-eigenvalues
               proj%E(band, band, kpoint, spin) = CMPLX(locproj%E(band_,kpoint,spin),0.)

               !dft-occupations
               proj%N(band, kpoint, spin) = CMPLX(locproj%N(band_,kpoint,spin),0.)

               !projectors
               DO orbital=1,proj%n_orbitals
                  proj%P( orbital, band, kpoint, spin ) = locproj%P(orbital,band_,kpoint,spin)
               ENDDO

            ENDDO
         ENDDO
      ENDDO


      !number of n_electrons
      IF (proj%n_spins == 1) THEN
         proj%N = 2 * proj%N
      ENDIF
      proj%N_tot = NINT( SUM(proj%N) / ( proj%n_kpoints ) )
      IF ( rank_0 ) THEN
         WRITE(6,*) '   PROJ set from LOCPROJ & SUBSPACE'
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE class_locproj_SET_P_FROM_LOCPROJ

   !general

   SUBROUTINE class_locproj_INIT_wannier( proj, n_orbitals, n_bands, n_kpoints, n_spins, hk_fmt  )
      !
      !  Purpose:
      !    Allocation
      !
      !  Record of revisions:
     !      Date       ProgrammerDescription of change
     !      ====       ===============================
     !    19/10/15     S. BarthelOriginal code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(INOUT) :: proj

      INTEGER, INTENT(IN) :: n_orbitals
      INTEGER, INTENT(IN) :: n_bands
      INTEGER, INTENT(IN) :: n_kpoints
      INTEGER, INTENT(IN) :: n_spins

      character(7), INTENT(IN) :: hk_fmt

      INTEGER :: allocerror   !error codes for allocation

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( n_bands < n_orbitals ) THEN
         WRITE(6,*) 'ERROR: n_bands < n_orbitals !'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ENDIF



      proj%n_orbitals = n_orbitals
      proj%n_bands = n_bands
      proj%n_kpoints = n_kpoints
      proj%n_spins = n_spins

      proj%hk_fmt = hk_fmt

      proj%is_unity_L = .TRUE.
      proj%is_unity_N = .TRUE.

      !deallocate possibly
      IF ( ALLOCATED(proj%E) ) THEN
         DEALLOCATE(proj%E, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "proj%E" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !dft-eigenvalues
      ALLOCATE ( proj%E( proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins ), STAT=allocerror )!square
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%E" could not be allocated !'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%E = CMPLX(0.,0.)
      ENDIF

      !---

      !deallocate possibly
      IF ( ALLOCATED(proj%N) ) THEN
         DEALLOCATE(proj%N, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "proj%N" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF


      ALLOCATE ( proj%band_indices ( proj%n_orbitals ), STAT=allocerror  )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%band_indices" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)                                  
         STOP
         proj%band_indices = 0
      ENDIF

      !ALLOCATE ( proj%orbital_indices( proj%n_orbitals ), STAT=allocerror )
      !IF ( allocerror > 0 ) THEN
      !   WRITE(6,*) 'ERROR: Memory for "proj%band_indices" could not be allocated'
      !   CALL MPI_Abort(MPI_COMM_WORLD, i_error)
      !   STOP
      !ELSE
      !   !init list of orbital indices from LOCPROJ                                                                                                                                
     !    proj%orbital_indices = band_indices
     ! ENDIF

      !dft-occupations
      ALLOCATE ( proj%N( proj%n_bands, proj%n_kpoints, proj%n_spins ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%N" could not be allocated !'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%N = CMPLX(0.,0.)
      ENDIF

      !---

      !deallocate possibly
      IF ( ALLOCATED(proj%P) ) THEN
         DEALLOCATE(proj%P, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "proj%P" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !projector matrices
      ALLOCATE ( proj%P( proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins ), STAT=allocerror )!square
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%P" could not be allocated !'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%P = CMPLX(0.,0.)
      ENDIF

      !deallocate possibly
      IF ( ALLOCATED(proj%inv_sqrt_ovrlp_L) ) THEN
         DEALLOCATE(proj%inv_sqrt_ovrlp_L, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "proj%inv_sqrt_ovrlp_L" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !allocate
      ALLOCATE ( proj%inv_sqrt_ovrlp_L( proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%inv_sqrt_ovrlp_L" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%inv_sqrt_ovrlp_L = CMPLX(0.,0.)
      ENDIF

   END SUBROUTINE class_locproj_INIT_wannier

   SUBROUTINE class_locproj_INIT_triqs( proj, n_orbitals, n_bands, n_kpoints, n_spins, hk_fmt,&
                                n_shells, n_corr_shells,shell_info, corr_shells_info,&
                                n_irreps, irrep_info  )
      !
      !  Purpose:
      !    Allocation
      !
      !  Record of revisions:
     !      Date       ProgrammerDescription of change
     !      ====       ===============================
     !    19/10/15     S. BarthelOriginal code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(INOUT) :: proj

      INTEGER, INTENT(IN) :: n_orbitals
      INTEGER, INTENT(IN) :: n_bands
      INTEGER, INTENT(IN) :: n_kpoints
      INTEGER, INTENT(IN) :: n_spins
      INTEGER, INTENT(IN) :: n_shells
      INTEGER, INTENT(IN) :: n_corr_shells
      INTEGER, INTENT(IN) :: n_irreps

                                       ! number of atomic shells
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: shell_info! information about shells (which atom, type, L, dimension)
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: corr_shells_info! information about shells (which atom, type, L, dimension, SO, interactions)
      INTEGER, ALLOCATABLE, DIMENSION(:) :: irrep_info    ! information about irreps (number of irreducible representations, and the dimensions of the irreps)


      character(7), INTENT(IN) :: hk_fmt

      INTEGER :: allocerror   !error codes for allocation

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( n_bands < n_orbitals ) THEN
         WRITE(6,*) 'ERROR: n_bands < n_orbitals !'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ENDIF



      proj%n_orbitals = n_orbitals
      proj%n_bands = n_bands
      proj%n_kpoints = n_kpoints
      proj%n_spins = n_spins
      proj%n_shells = n_shells
      proj%n_corr_shells = n_corr_shells
      proj%n_irreps = n_irreps

      proj%hk_fmt = hk_fmt

      proj%is_unity_L = .TRUE.
      proj%is_unity_N = .TRUE.

      !deallocate possibly
      IF ( ALLOCATED(proj%E) ) THEN
         DEALLOCATE(proj%E, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "proj%E" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !dft-eigenvalues
      ALLOCATE ( proj%E( proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins ), STAT=allocerror )!square
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%E" could not be allocated !'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%E = CMPLX(0.,0.)
      ENDIF

      !---

      !deallocate possibly
      IF ( ALLOCATED(proj%N) ) THEN
         DEALLOCATE(proj%N, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "proj%N" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF


      ALLOCATE ( proj%band_indices ( proj%n_orbitals ), STAT=allocerror  )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%band_indices" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)                                  
         STOP
         proj%band_indices = 0
      ENDIF

      !ALLOCATE ( proj%orbital_indices( proj%n_orbitals ), STAT=allocerror )
      !IF ( allocerror > 0 ) THEN
      !   WRITE(6,*) 'ERROR: Memory for "proj%band_indices" could not be allocated'
      !   CALL MPI_Abort(MPI_COMM_WORLD, i_error)
      !   STOP
      !ELSE
      !   !init list of orbital indices from LOCPROJ                                                                                                                                
     !    proj%orbital_indices = band_indices
     ! ENDIF

      !dft-occupations
      ALLOCATE ( proj%N( proj%n_bands, proj%n_kpoints, proj%n_spins ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%N" could not be allocated !'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%N = CMPLX(0.,0.)
      ENDIF

      !---

      !deallocate possibly
      IF ( ALLOCATED(proj%P) ) THEN
         DEALLOCATE(proj%P, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "proj%P" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !projector matrices
      ALLOCATE ( proj%P( proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins ), STAT=allocerror )!square
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%P" could not be allocated !'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%P = CMPLX(0.,0.)
      ENDIF

      !deallocate possibly
      IF ( ALLOCATED(proj%inv_sqrt_ovrlp_L) ) THEN
         DEALLOCATE(proj%inv_sqrt_ovrlp_L, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "proj%inv_sqrt_ovrlp_L" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !allocate
      ALLOCATE ( proj%inv_sqrt_ovrlp_L( proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%inv_sqrt_ovrlp_L" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%inv_sqrt_ovrlp_L = CMPLX(0.,0.)
      ENDIF

      ALLOCATE ( proj%shell_info(proj%n_shells, 4), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%inv_sqrt_ovrlp_L" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%shell_info = shell_info
      ENDIF

      ALLOCATE ( proj%corr_shells_info(proj%n_corr_shells, 6), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%inv_sqrt_ovrlp_L" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%corr_shells_info = corr_shells_info
      ENDIF

      ALLOCATE ( proj%irrep_info(proj%n_irreps), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%inv_sqrt_ovrlp_L" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%irrep_info = irrep_info
      ENDIF

   END SUBROUTINE class_locproj_INIT_triqs

   !===============================================================================================================================
   !Orthonormalization and complement
   !===============================================================================================================================

   SUBROUTINE locproj_CALC_ORTH_SVD( proj )
      !
      !  Purpose:
      !    Orthogonalize projectors using the root of the inverse overlap matrix
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    08/10/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(INOUT) :: proj

      INTEGER :: orbital, band, kpoint, spin
      INTEGER :: orbital_

      COMPLEX, DIMENSION(proj%n_bands, proj%n_bands) :: PWORK !P^{+} !square

      !dev
      INTEGER :: dim_work_opt							!Optimal work array length for SVD

      REAL, DIMENSION(proj%n_bands) :: sw					!Singular-values
      COMPLEX, DIMENSION(proj%n_bands,proj%n_bands) :: sv_V			!Right singular-vectors
      COMPLEX, DIMENSION(proj%n_bands,proj%n_bands) :: sv_V_hc			!Hermitian-conjugate of right singular vectors

      INTEGER :: allocerror							!Error code for allocation

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( rank_0 ) THEN

         WRITE(6,*) '  rank 0: Calling SVD for proj%P ...'

! 	std_sw = 0.
! 	n_std = 0

         DO spin=1,proj%n_spins
            DO kpoint=1,proj%n_kpoints

               !init
               sw = 0.
               sv_V = CMPLX(0.,0.)
               sv_V_hc = CMPLX(0.,0.)

               !P^{+} = <p:n|L,k,s>
               PWORK = CONJG(TRANSPOSE(proj%P(:,:,kpoint,spin)))
               sv_V = PWORK

               !Determine optimal work array size for SVD (maybe only do once)
               dim_work_opt = -1
               CALL complex_square_matrix_svd(proj%n_bands, sv_V, sw, sv_V_hc, dim_work_opt)	!sv_V will be destroyed
               !WRITE(6,*) spin, kpoint, dim_work_opt

               !Perform actual singular-value decomposition: M = U * S * V^{+}, note: U is not computed
               CALL complex_square_matrix_svd(proj%n_bands, sv_V, sw, sv_V_hc, dim_work_opt)
               !WRITE(6,*) 'svd done'

               !Calculate right singular-vectors
               sv_V = CONJG(TRANSPOSE(sv_V_hc))

               !Invert singular values: S^{-} = 1/S, S is diagonal
               DO orbital=1,proj%n_bands
                  IF (sw(orbital) > proj%tol_svd ) THEN
                     sw(orbital) = 1./sw(orbital)
                     !sw(orbital) = 1.			!Georg K. told me to check this ... this yields the initial PROCAR projectors

! 	      !lets check the std. deviation
! 	      std_sw = std_sw + ( sw(orbital) -1.0 )**2
! 	      n_std = n_std + 1
! 	    ELSE
! 	      sw(orbital) = 0.
                  ENDIF

! 	    !maybe only n_orbitals ?
! 	    IF ( orbital <= proj%n_orbitals ) THEN
! 	      !lets check the std. deviation
! 	      std_sw = std_sw + ( sw(orbital) -1.0 )**2
! 	      n_std = n_std + 1
! 	    ENDIF


               ENDDO
               !WRITE(6,*) 'Inverse of singular values: ', sw

               !Multiply inverted diagonal singular-value matrix with hermitian-conjugate of right singular-vectors: V^{+}' = S^{-} * V^{+}
               DO orbital_=1,proj%n_bands
                  sv_V_hc(:,orbital_) = sw(:) * sv_V_hc(:,orbital_)	!S^{-}_{ii} * V^{+}_{ik}
               ENDDO

               !Calculate square root of the overlap matrix: O^{-1/2} = V * V^{+}' = V * S^{-} * V^{+}
               DO orbital_=1,proj%n_bands
                  DO orbital=1,proj%n_bands

                     !V_{ij} * V^{+}'_{jk}, sum over j
                     proj%inv_sqrt_ovrlp_L(orbital,orbital_,kpoint,spin) = SUM( sv_V(orbital,:)*sv_V_hc(:,orbital_) )

                     !WRITE(6,*) orbital, orbital_, proj%inv_sqrt_ovrlp_L(orbital,orbital_,kpoint,spin)

                  ENDDO
               ENDDO

               !Multiply square root of the overlap matrix with initial projectors, see Eq.(18) of PRB 77, 205112 (2008)
               DO orbital=1,proj%n_bands
                  DO band=1,proj%n_bands

                     !P = <p:L|n,k,s>, CONJG() because we used P^{+} = <p:n|L,k,s> as SVD input in PWORK
                     proj%P(orbital,band,kpoint,spin) = CONJG( SUM( PWORK(band,:) * proj%inv_sqrt_ovrlp_L(:,orbital,kpoint,spin) ) )

                  ENDDO
               ENDDO

            ENDDO
         ENDDO

! 	std_sw = SQRT( std_sw / (n_std-1) )

      ENDIF

      !broadcast
      srcnt = proj%n_bands*proj%n_bands*proj%n_kpoints*proj%n_spins
      CALL MPI_BCAST( proj%inv_sqrt_ovrlp_L, srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      srcnt = proj%n_bands*proj%n_bands*proj%n_kpoints*proj%n_spins
      CALL MPI_BCAST(proj%P, srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      IF ( rank_0 ) THEN
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE locproj_CALC_ORTH_SVD

   SUBROUTINE locproj_CALC_COMPL( proj )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    08/10/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(INOUT) :: proj

      INTEGER :: orbital, band, kpoint, spin
      INTEGER :: orbital_

      COMPLEX, DIMENSION(proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins) :: PWORK
      REAL, DIMENSION(proj%n_bands) :: norm_PWORK		!Norm of orthogonal complement candidates
      INTEGER :: idx_PWORK					!Index of best orthogonal complement candidate

      INTEGER :: n_orbitals_done				!Number of processed orbitals

      !-----------------------------------------------------------------------------------------------------------------------------
      IF ( rank_0 ) THEN
         WRITE(6,*) '  rank 0: Calculating complement for proj%P ...'
      ENDIF

      n_orbitals_done = proj%n_orbitals

      !initial
      CALL locproj_OVRLP_N( proj )	!mind: this is MPI

      DO WHILE ( n_orbitals_done < proj%n_bands )
         n_orbitals_done = n_orbitals_done+1

         !rank 0 only
         IF ( rank_0 ) THEN
         !WRITE(6,'("  orbital =", I5 )'), n_orbitals_done

            !Construct orthogonal complement candidates for all k-points and spins
            PWORK = -proj%ovrlp_N
            DO band=1,proj%n_bands
               PWORK(band,band,:,:) = PWORK(band,band,:,:) + 1. !diagonal in bands
            ENDDO

            !loops
            DO spin=1,proj%n_spins
               DO kpoint=1,proj%n_kpoints

                  !Calculate norm of orthogonal complement candidates (wrt. to bands?)
                  norm_PWORK = 0.
                  DO band=1,proj%n_bands
                     norm_PWORK(band) = SQRT( DOT_PRODUCT( PWORK(band,:,kpoint,spin), PWORK(band,:,kpoint,spin) ) )
                  ENDDO

                  !Find index of best orthogonal complement candidate (criteria is maximum length)
                  idx_PWORK = MAXLOC(norm_PWORK,1)

                  !Append best orthogonal complement candidate to existing projectors P = <p:L|n,k,s>
                  proj%P(n_orbitals_done,:,kpoint,spin) = PWORK(idx_PWORK,:,kpoint,spin) / norm_PWORK(idx_PWORK)

               ENDDO
            ENDDO

         ENDIF

         !broadcast
         srcnt = proj%n_bands*proj%n_bands*proj%n_kpoints*proj%n_spins
         CALL MPI_BCAST(proj%P, srcnt, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD, i_error)

         !recalculate overlaps
         CALL locproj_OVRLP_N( proj )	!mind: this is MPI

      ENDDO

      IF ( rank_0 ) THEN
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE locproj_CALC_COMPL


   !===============================================================================================================================
   !Overlap matrices and checks of orthonormalization
   !===============================================================================================================================

   SUBROUTINE locproj_CHECK_OVRLP( proj )
      !
      !  Purpose:
      !    Calculate overlap matrices and test for unity, MPI safe
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    10/11/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(INOUT) :: proj			!proj%P( orbital, band, kpoint, spin )

      INTEGER :: orbital, orbital_, band, band_, kpoint, spin	!loop indices

      REAL :: tmp_real, tmp_imag

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( rank_0 ) THEN
         WRITE(6,*) '  rank 0: Checking overlap matrices ...'
      ENDIF

      !calc
      CALL locproj_OVRLP_L( proj )
      CALL locproj_OVRLP_N( proj )

      !test
      IF ( rank_0 ) THEN

         !L
         DO spin=1,proj%n_spins
            DO kpoint=1,proj%n_kpoints
               DO orbital_=1,proj%n_bands
                  DO orbital=1,proj%n_bands

                     tmp_real = REAL(proj%ovrlp_L(orbital,orbital_,kpoint,spin))
                     tmp_imag = AIMAG(proj%ovrlp_L(orbital,orbital_,kpoint,spin))

                     !diagonal
                     IF ( orbital == orbital_ ) THEN

                        IF ( ( ABS(tmp_real-1.0) > proj%tol_ovl ) .OR. ( ABS(tmp_imag) > proj%tol_ovl ) ) THEN
                           proj%is_unity_L = .FALSE.
                           ! WRITE(6,*) 'o: ', orbital,orbital_,kpoint,spin
                        ENDIF

                        !offdiagonal
                     ELSE
                        IF ( ( ABS(tmp_real) > proj%tol_ovl) .OR. (ABS(tmp_imag) > proj%tol_ovl) ) THEN
                           proj%is_unity_L = .FALSE.
                           ! WRITE(6,*) 'o: ', orbital, orbital_, kpoint, spin
                        ENDIF
                     ENDIF

                  ENDDO
               ENDDO
            ENDDO
         ENDDO

         !N
         DO spin=1,proj%n_spins
            DO kpoint=1,proj%n_kpoints
               DO band_=1,proj%n_bands
                  DO band=1,proj%n_bands

                     tmp_real = REAL(proj%ovrlp_N(band,band_,kpoint,spin))
                     tmp_imag = AIMAG(proj%ovrlp_N(band,band_,kpoint,spin))

                     !diagonal
                     IF ( band == band_ ) THEN
                        IF ( ( ABS(tmp_real-1.0) > proj%tol_ovl ) .OR. ( ABS(tmp_imag) > proj%tol_ovl ) ) THEN
                           proj%is_unity_N = .FALSE.
                           ! WRITE(6,*) 'n: ', band, band_, kpoint, spin
                        ENDIF

                        !offdiagonal
                     ELSE
                        IF ( ( ABS(tmp_real) > proj%tol_ovl ) .OR. ( ABS(tmp_imag) > proj%tol_ovl ) ) THEN
                           proj%is_unity_N = .FALSE.
                           ! WRITE(6,*) 'n: ', band, band_, kpoint, spin
                        ENDIF
                     ENDIF

                  ENDDO
               ENDDO
            ENDDO
         ENDDO


         IF ( ( proj%is_unity_L ) .AND.  ( proj%is_unity_N ) ) THEN
            WRITE(6,*) "  Overlap matrices are fine."
         ENDIF

      ENDIF

      srcnt = 1
      CALL MPI_BCAST(proj%is_unity_L, srcnt, MPI_LOGICAL, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      CALL MPI_BCAST(proj%is_unity_N, srcnt, MPI_LOGICAL, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      IF ( rank_0 ) THEN
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE locproj_CHECK_OVRLP

   SUBROUTINE locproj_OVRLP_L( proj )
      !
      !  Purpose:
      !    ovrlp_L = P * P^{+} with P := <p:L|n,k,s>, proj%P( orbital, band, kpoint, spin )
      !    ovrlp_L will be overwritten
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    10/11/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(INOUT) :: proj

      INTEGER :: allocerror
      INTEGER :: orbital, orbital_, band, kpoint, spin	!loop indices

      !-----------------------------------------------------------------------------------------------------------------------------

!       IF ( rank_0 ) THEN
! 	WRITE(6,*) '  MPI: Calculating ovrlp_L ...'
!       ENDIF

      !deallocate possibly
      IF ( ALLOCATED(proj%ovrlp_L) ) THEN
         DEALLOCATE(proj%ovrlp_L, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "proj%ovrlp_L" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !allocate
      ALLOCATE ( proj%ovrlp_L( proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%ovrlp_L" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%ovrlp_L = CMPLX(0.,0.)
      ENDIF

      !-----------------------------------------------------------------------------------------------------------------------------

      !MPI: superindex loop over kpoints and spins
      CALL parallel_range(1, proj%n_kpoints*proj%n_spins, nprocs, myrank, ista, iend)
      DO ilin=ista,iend

         CALL parallel_lin2sub_double(ilin,proj%n_kpoints,proj%n_spins,kpoint,spin)

         !ovrlp_L = P * P^{+}
         DO orbital_=1,proj%n_bands
            DO orbital=1,proj%n_bands

               DO band=1,proj%n_bands

                  proj%ovrlp_L(orbital,orbital_,kpoint,spin) = proj%ovrlp_L(orbital,orbital_,kpoint,spin) &

                     + proj%P(orbital,band,kpoint,spin)*CONJG(proj%P(orbital_,band,kpoint,spin))

               ENDDO

            ENDDO
         ENDDO

      ENDDO

      !broadcast
      srcnt = proj%n_bands*proj%n_bands*proj%n_kpoints*proj%n_spins
      CALL MPI_ALLREDUCE(MPI_IN_PLACE, proj%ovrlp_L, srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)

   END SUBROUTINE locproj_OVRLP_L

   SUBROUTINE locproj_OVRLP_N( proj )
      !
      !  Purpose:
      !    ovrlp_N = P^{+} * P with P := <p:L|n,k,s>, proj%P( orbital, band, kpoint, spin )
      !    ovrlp_N will be overwritten
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    16/10/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(INOUT) :: proj

      INTEGER :: allocerror
      INTEGER :: orbital, orbital_, band, band_, kpoint, spin	!loop indices

      !-----------------------------------------------------------------------------------------------------------------------------

!       IF ( rank_0 ) THEN
! 	WRITE(6,*) '  MPI: Calculating ovrlp_N ...'
!       ENDIF

      !deallocate possibly
      IF ( ALLOCATED(proj%ovrlp_N) ) THEN
         DEALLOCATE(proj%ovrlp_N, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "proj%ovrlp_N" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !allocate
      ALLOCATE ( proj%ovrlp_N( proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%ovrlp_N" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%ovrlp_N = CMPLX(0.,0.)
      ENDIF

      !MPI: superindex loop over kpoints and spins
      CALL parallel_range(1, proj%n_kpoints*proj%n_spins, nprocs, myrank, ista, iend)
      DO ilin=ista,iend

         CALL parallel_lin2sub_double(ilin,proj%n_kpoints,proj%n_spins,kpoint,spin)

         !ovrlp_N = P^{+} * P
         DO band_=1,proj%n_bands
            DO band=1,proj%n_bands

               DO orbital=1,proj%n_bands

                  proj%ovrlp_N(band, band_, kpoint, spin) = proj%ovrlp_N(band, band_, kpoint, spin) &

                     + CONJG(proj%P(orbital,band,kpoint,spin))*proj%P(orbital,band_,kpoint,spin)

               ENDDO

            ENDDO
         ENDDO

      ENDDO

      !broadcast
      srcnt = proj%n_bands*proj%n_bands*proj%n_kpoints*proj%n_spins
      CALL MPI_ALLREDUCE(MPI_IN_PLACE, proj%ovrlp_N, srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)

   END SUBROUTINE locproj_OVRLP_N

   !===============================================================================================================================
   !Hamiltonian
   !===============================================================================================================================

   SUBROUTINE locproj_SET_H( proj )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    20/11/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(INOUT) :: proj

      INTEGER :: allocerror   !error codes for allocation

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( rank_0 ) THEN
         WRITE(6,*) '  Transforming dispersion to hamiltonian ...'
      ENDIF

      !deallocate possibly
      IF ( ALLOCATED(proj%H) ) THEN
         DEALLOCATE(proj%H, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "proj%proj%H" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !projector matrices
      ALLOCATE ( proj%H( proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins ), STAT=allocerror )	!square
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "proj%H" could not be allocated !'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         proj%H = CMPLX(0.,0.)
      ENDIF

      CALL locproj_TRANSFORM_EXT2LOC( proj, proj%E, proj%H )

      IF ( rank_0 ) THEN
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE


    SUBROUTINE export_H_wannier(proj, k_points)

    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units

    TYPE( class_locproj ), INTENT(IN) :: proj
    REAL,  DIMENSION(3,proj%n_kpoints), INTENT(IN) :: k_points

    INTEGER :: ioerror
    INTEGER :: orbital, orbital_, band, band_, kpoint, kpoint_, spin	!loop indices

    REAL :: tmp_real, tmp_imag, abs_val2


    2001  FORMAT(4(2X,I6),2X,'# of spins, # of k-points, # of orbitals, # of electrons. Generated from PROCAR.')
    2002  FORMAT(2(2X,I6),2X,F19.15)
    2003  FORMAT(2(2X,F19.15))

    2004  FORMAT(3(2X,F19.15),2X)	!export of kpoints

     OPEN (UNIT=26,FILE='./H_fBZ.proj',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
     WRITE(26,2001) proj%n_spins, proj%n_kpoints, proj%n_bands, proj%N_tot

     DO spin=1,proj%n_spins
        DO kpoint=1,proj%n_kpoints
           WRITE(26,2004) k_points(:,kpoint)	!outcar%k_points_ibz_hf(:,kpoint)	!outcar%k_weight_ibz_hf(kpoint)	!k-points in LOCPROJ should be ordered as listed in OUTCAR ?

           DO orbital=1,proj%n_bands
              DO orbital_=1,proj%n_bands


                 IF ( orbital > orbital_ ) THEN
                    tmp_real = REAL(proj%H( orbital, orbital_, kpoint, spin ))
                    tmp_imag = AIMAG(proj%H( orbital, orbital_, kpoint, spin ))
                 ELSE
                    tmp_real = REAL(proj%H( orbital_, orbital, kpoint, spin ))
                    tmp_imag = -AIMAG(proj%H( orbital_, orbital, kpoint, spin ))
                 ENDIF
                 IF ( orbital_ == proj%n_bands ) THEN
                    WRITE(26, 2003) tmp_real+1E-18, tmp_imag+1E-18
                 ELSE
                    WRITE(26, 2003, ADVANCE='NO') tmp_real+1E-18, tmp_imag+1E-18
                 ENDIF

              ENDDO
           ENDDO

        ENDDO
     ENDDO
     CLOSE(26)


    END SUBROUTINE export_H_wannier


 SUBROUTINE export_H_triqs(proj)


    IMPLICIT NONE

    ! Data dictionary: declare constants

    ! Data dictionary: declare variable types, definitions, units

    TYPE( class_locproj ), INTENT(IN) :: proj


    INTEGER :: ioerror
    INTEGER :: orbital, orbital_, band, band_, kpoint, kpoint_, spin, shell	!loop indices

    REAL :: tmp_real, tmp_imag, abs_val2

    2003 FORMAT(2(2X,F19.15))
    2004 FORMAT((I6))
    2005 FORMAT((I3))
    2006 FORMAT((F19.15))
    2007 FORMAT (4(I3))
    2008 FORMAT (6(I3))
    OPEN (UNIT=26,FILE='./H_fBZ.proj',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
    WRITE(26,2004) proj%n_kpoints

    WRITE(26,2006) float(proj%N_tot)
    WRITE(26,2005) proj%n_shells
    DO shell = 1, proj%n_shells
        WRITE(26,2007) proj%shell_info(shell,:)
    ENDDO

    WRITE(26,2005) proj%n_corr_shells
    DO shell = 1, proj%n_corr_shells
        WRITE(26,2008) proj%corr_shells_info(shell,:)
    ENDDO

    WRITE(26,'(I3)',ADVANCE='NO') proj%n_irreps
    DO shell = 1, proj%n_irreps
        WRITE(26,'(I3)',ADVANCE='NO') proj%irrep_info(shell)
    ENDDO
    WRITE(26,*)



    DO spin=1,proj%n_spins
        DO kpoint=1,proj%n_kpoints
            DO orbital=1,proj%n_bands
              DO orbital_=1,proj%n_bands
                IF ( orbital > orbital_ ) THEN
                    tmp_real = REAL(proj%H( orbital, orbital_, kpoint, spin ))
                ELSE
                    tmp_real = REAL(proj%H( orbital_, orbital, kpoint, spin ))
                ENDIF
                IF ( orbital_ == proj%n_bands ) THEN
                    WRITE(26, 2003) tmp_real + 1E-18
                 ELSE
                    WRITE(26, 2003, ADVANCE='NO') tmp_real + 1E-18
                 ENDIF
                ENDDO
            ENDDO

            DO orbital=1,proj%n_bands
              DO orbital_=1,proj%n_bands
                IF ( orbital > orbital_ ) THEN
                    tmp_imag = AIMAG(proj%H( orbital, orbital_, kpoint, spin ))
                ELSE
                    tmp_imag = -AIMAG(proj%H( orbital_, orbital, kpoint, spin ))
                ENDIF
                IF ( orbital_ == proj%n_bands ) THEN
                    WRITE(26, 2003) tmp_imag + 1E-18
                 ELSE
                    WRITE(26, 2003, ADVANCE='NO') tmp_imag + 1E-18
                 ENDIF
                ENDDO
            ENDDO
        enddo
    END DO
    CLOSE(26)

    END SUBROUTINE export_H_triqs

   SUBROUTINE locproj_EXPORT_H_PROCAR( proj, procar )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    10/11/15     S. Barthel	Original code
      !

      USE type_file_procar

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(IN) :: proj
      TYPE( file_procar ), INTENT(IN) :: procar

      INTEGER :: ioerror
      INTEGER :: orbital, orbital_, band, band_, kpoint, kpoint_, spin	!loop indices

      REAL :: tmp_real, tmp_imag, abs_val2

      !-----------------------------------------------------------------------------------------------------------------------------

!       1000 FORMAT(1X,'spin:',1X,I5,1X,'k-point:',1X,I5)
!       1001 FORMAT(1X,'orbital_:',1X,I5)
!       1002 FORMAT(1X,I5,4X,F19.14,4X,F19.14,4X,F19.14)



      IF ( rank_0 ) THEN

         WRITE(6,*) '  Exporting H ...'
         WRITE(6,*) proj%hk_fmt
        IF (proj%hk_fmt == 'triqs') then
            CALL export_H_triqs(proj)
        else
            CALL export_H_wannier(proj, procar%k_vector(:,:))
        END IF

         WRITE(6,*) '-'

      ENDIF

   END SUBROUTINE locproj_EXPORT_H_PROCAR

   SUBROUTINE locproj_EXPORT_H( proj, outcar )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    10/11/15     S. Barthel	Original code
      !

      USE type_file_outcar

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(IN) :: proj
      TYPE( file_outcar ), INTENT(IN) :: outcar

      INTEGER :: ioerror
      INTEGER :: orbital, orbital_, band, band_, kpoint, kpoint_, spin, shell	!loop indices

      REAL :: tmp_real, tmp_imag, abs_val2

      !-----------------------------------------------------------------------------------------------------------------------------




      IF ( rank_0 ) THEN

         WRITE(6,*) '  Exporting H ...'

         !---
         !full BZ.

         IF (proj%hk_fmt == 'triqs') then
            CALL export_H_triqs(proj)
         ELSE
            CALL export_H_wannier(proj, outcar%k_points_ibz_hf(:,:))
         ENDIF


         WRITE(6,*) '-'

      ENDIF

   END SUBROUTINE locproj_EXPORT_H


   SUBROUTINE locproj_EXPORT_N( proj )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    10/11/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      TYPE( class_locproj ), INTENT(IN) :: proj

      INTEGER :: ioerror
      INTEGER :: orbital,  kpoint, spin


      !-----------------------------------------------------------------------------------------------------------------------------

    2003 FORMAT(F19.15)

    OPEN (UNIT=26,FILE='./N.proj',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)


      IF ( rank_0 ) THEN

         WRITE(6,*) '  Exporting N ...'


        DO spin=1,proj%n_spins
            DO kpoint=1,proj%n_kpoints
                DO orbital=1,proj%n_bands
                        WRITE(26, 2003) proj%N(orbital,kpoint,spin)*0.5
                ENDDO
            ENDDO
        END DO

         WRITE(6,*) '-'

      ENDIF
    CLOSE(26)
   END SUBROUTINE locproj_EXPORT_N



   SUBROUTINE locproj_EXPORT_P( proj)
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    10/11/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(IN) :: proj

      INTEGER :: ioerror
      INTEGER :: orbital, band, kpoint, spin!loop indices

      REAL :: tmp_real, tmp_imag, abs_val2


        2003 FORMAT(2(2X,F19.15))
      !-----------------------------------------------------------------------------------------------------------------------------

!       1000 FORMAT(1X,'spin:',1X,I5,1X,'k-point:',1X,I5)
!       1001 FORMAT(1X,'orbital_:',1X,I5)
!       1002 FORMAT(1X,I5,4X,F19.14,4X,F19.14,4X,F19.14)





      IF ( rank_0 ) THEN

        WRITE(6,*) '  Exporting P ...'

         !---
         !full BZ.

        OPEN (UNIT=26,FILE='./P_fBZ.proj',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)



        DO spin=1,proj%n_spins
            DO kpoint=1,proj%n_kpoints
                DO orbital=1,proj%n_bands
                  DO band=1,proj%n_bands
                    tmp_real = REAL(proj%P( orbital, band, kpoint, spin ))
                    IF ( band == proj%n_bands ) THEN
                        WRITE(26, 2003) tmp_real + 1E-18
                     ELSE
                        WRITE(26, 2003, ADVANCE='NO') tmp_real + 1E-18
                     ENDIF
                    ENDDO
                ENDDO

                DO orbital=1,proj%n_bands
                  DO band=1,proj%n_bands
                    tmp_imag = AIMAG(proj%P( orbital, band, kpoint, spin ))
                    IF ( band == proj%n_bands ) THEN
                        WRITE(26, 2003) tmp_imag + 1E-18
                     ELSE
                        WRITE(26, 2003, ADVANCE='NO') tmp_imag + 1E-18
                     ENDIF
                    ENDDO
                ENDDO
            enddo
        END DO

        CLOSE(26)

     WRITE(6,*) '-'

      ENDIF

   END SUBROUTINE locproj_EXPORT_P

   !===============================================================================================================================
   !Transformation betwen orbital- <--> band-bases
   !===============================================================================================================================

   SUBROUTINE locproj_TRANSFORM_LOC2EXT( proj, outcar, LOC, EXT )
      !
      !  Purpose:
      !    EXT = P^{+} * LOC * P with P := <p:L|n,k,s>, EXT will be overwritten
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    16/10/15     S. Barthel	Original code
      !

      USE type_file_outcar

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(IN) :: proj
      TYPE( file_outcar ), INTENT(IN) :: outcar

      !COMPLEX, DIMENSION(proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins), INTENT(IN) :: LOC
      !COMPLEX, DIMENSION(proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins), INTENT(OUT) :: EXT

      COMPLEX, DIMENSION(proj%n_bands, proj%n_bands, outcar%n_kpoints_ibz, proj%n_spins), INTENT(IN) :: LOC
      COMPLEX, DIMENSION(proj%n_bands, proj%n_bands, outcar%n_kpoints_ibz, proj%n_spins), INTENT(OUT) :: EXT

      INTEGER :: orbital, orbital_, band, band_, kpoint, kpoint_, spin	!loop indices

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( rank_0 ) THEN
         WRITE(6,*) '  MPI: Transforming to extended basis ...'
         WRITE(6,*) '       Irreducible k-points are mapped to full BZ. for proj%P'
      ENDIF

      EXT = CMPLX(0.,0.)

      !MPI: superindex loop over kpoints and spins
      !CALL parallel_range(1, proj%n_kpoints*proj%n_spins, nprocs, myrank, ista, iend)


      CALL parallel_range(1, outcar%n_kpoints_ibz*proj%n_spins, nprocs, myrank, ista, iend)


      DO ilin=ista,iend

         !CALL parallel_lin2sub_double(ilin,proj%n_kpoints,proj%n_spins,kpoint,spin)
         CALL parallel_lin2sub_double(ilin,outcar%n_kpoints_ibz,proj%n_spins,kpoint,spin)

         !this maps the irreducible kpoints into the full BZ. (maybe not needed), LOC and EXT are given on irreducible k-points
         kpoint_ = outcar%idx_ibz_hf(kpoint)

         !EXT = P^{+} * LOC * P
         DO band_=1,proj%n_bands
            DO band=1,proj%n_bands

               DO orbital_=1,proj%n_bands
                  DO orbital=1,proj%n_bands

                     EXT(band, band_, kpoint, spin) = EXT(band, band_, kpoint, spin) &

                        + CONJG(proj%P(orbital,band,kpoint_,spin))*LOC(orbital,orbital_,kpoint,spin)*proj%P(orbital_,band_,&
                                                                                                            kpoint_,spin)

                  ENDDO
               ENDDO

            ENDDO
         ENDDO

      ENDDO

      !broadcast
      !srcnt = proj%n_bands*proj%n_bands*proj%n_kpoints*proj%n_spins
      srcnt = proj%n_bands*proj%n_bands*outcar%n_kpoints_ibz*proj%n_spins
      CALL MPI_ALLREDUCE(MPI_IN_PLACE, EXT, srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)

   END SUBROUTINE locproj_TRANSFORM_LOC2EXT

   SUBROUTINE locproj_TRANSFORM_EXT2LOC( proj, EXT, LOC )
      !
      !  Purpose:
      !    LOC = P * EXT * P^{+} with P := <p:L|n,k,s>, LOC will be overwritten
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    16/10/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( class_locproj ), INTENT(IN) :: proj

      COMPLEX, DIMENSION(proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins), INTENT(IN) :: EXT
      COMPLEX, DIMENSION(proj%n_bands, proj%n_bands, proj%n_kpoints, proj%n_spins), INTENT(OUT) :: LOC

      INTEGER :: orbital, orbital_, band, band_, kpoint, spin	!loop indices

      !-----------------------------------------------------------------------------------------------------------------------------

      IF ( rank_0 ) THEN
         WRITE(6,*) '  MPI: Transforming to localized basis ...'
      ENDIF

      LOC = CMPLX(0.,0.)

      !MPI: superindex loop over kpoints and spins
      CALL parallel_range(1, proj%n_kpoints*proj%n_spins, nprocs, myrank, ista, iend)
      DO ilin=ista,iend

         CALL parallel_lin2sub_double(ilin,proj%n_kpoints,proj%n_spins,kpoint,spin)

         !LOC = P * EXT * P^{+}
         DO orbital_=1,proj%n_bands
            DO orbital=1,proj%n_bands

               DO band_=1,proj%n_bands
                  DO band=1,proj%n_bands

                     LOC(orbital,orbital_,kpoint,spin) = LOC(orbital,orbital_,kpoint,spin) &

                        + proj%P(orbital,band,kpoint,spin)*EXT(band,band_,kpoint,spin)*CONJG(proj%P(orbital_,band_,kpoint,spin))

                  ENDDO
               ENDDO

            ENDDO
         ENDDO

      ENDDO

      !broadcast
      srcnt = proj%n_bands*proj%n_bands*proj%n_kpoints*proj%n_spins
      CALL MPI_ALLREDUCE(MPI_IN_PLACE, LOC, srcnt, MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, i_error)


   END SUBROUTINE locproj_TRANSFORM_EXT2LOC

END MODULE type_locproj
