MODULE type_file_coords
!
!  Purpose:
!    Import external input
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    10/10/15     S. Barthel	Original code
!

   USE parallel
   USE fileio

   IMPLICIT NONE

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units

!===================================================================================================================================

   TYPE :: file_coords !data container for coords

      !PRIVATE                                                 !no access granted to member variables

      CHARACTER(16) :: filename = 'COORDS'                    !default filename

      INTEGER :: n_ions                                       !number of ions
      REAL, ALLOCATABLE, DIMENSION(:,:,:) :: T_local          !local coordinate systems for each correlated ion

      INTEGER, ALLOCATABLE, DIMENSION(:) :: eqv		!symmetry equivalent ions

   END TYPE file_coords

!===================================================================================================================================

CONTAINS

   SUBROUTINE file_coords_SET_FILENAME( coords, filename )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    30/10/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_coords ), INTENT(INOUT) :: coords
      CHARACTER(16), INTENT(IN) :: filename

      !-------------------------------------------------------------------------------------------------------------------------------

      coords%filename = TRIM(ADJUSTL(filename))

   END SUBROUTINE file_coords_SET_FILENAME

   !=================================================================================================================================

   SUBROUTINE file_coords_SET( coords, poscar )
      !
      !  Purpose:
      !    Read a SUBSPACE file
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    29/10/15     S. Barthel	Original code
      !

      USE type_file_poscar  !this forces the data structure to be non-private

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_coords ), INTENT(INOUT) :: coords
      TYPE( file_poscar ), INTENT(IN) :: poscar

      INTEGER :: ioerror          !error code for file IO

      !-------------------------------------------------------------------------------------------------------------------------------

      IF ( rank_0 ) THEN
         OPEN (UNIT=26,FILE=coords%filename, STATUS='OLD', ACTION='READ', IOSTAT=ioerror)
         CLOSE(26)
      ENDIF

      srcnt = 1
      CALL MPI_BCAST(ioerror,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      IF ( ioerror /= 0 ) THEN

         IF ( rank_0 ) THEN
            WRITE(6,*) '   rank 0: Creating COORDS ...'
         ENDIF

         CALL file_coords_SET_FROM_POSCAR( coords, poscar )	!must be called by all ranks
         CALL file_coords_WRITE( coords )				!called by all ranks, mpi safe
      ELSE

         IF ( rank_0 ) THEN
            WRITE(6,*) '   rank 0: Reading COORDS ...'
         ENDIF

         CALL file_coords_SET_FROM_FILE( coords, poscar )		!must be called by all ranks
      ENDIF

   END SUBROUTINE file_coords_SET

   !=================================================================================================================================

   SUBROUTINE file_coords_SET_FROM_POSCAR( coords, poscar )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    29/10/15     S. Barthel	Original code
      !

      USE type_file_poscar  !this forces the data structure to be non-private

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_coords ), INTENT(INOUT) :: coords
      TYPE( file_poscar ), INTENT(IN) :: poscar

      INTEGER :: allocerror          !error code for allocation

      INTEGER :: ion, i

      !-------------------------------------------------------------------------------------------------------------------------------

      !init
      coords%n_ions = poscar%n_ions

      !deallocate possibly
      IF ( ALLOCATED(coords%T_local) ) THEN
         DEALLOCATE(coords%T_local, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "T_local" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !allocate
      ALLOCATE ( coords%T_local(3,3,coords%n_ions), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "T_local" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE

         !init as standard basis (basis vectors stored in columns written as rows)
         coords%T_local = 0.
         DO ion=1,coords%n_ions
            DO i=1,3
               coords%T_local(i,i,ion) = 1.
            ENDDO
         ENDDO

      ENDIF

!     !allocate
!     ALLOCATE ( coords%eqv(coords%n_ions), STAT=allocerror )
!     IF ( allocerror > 0 ) THEN
!       WRITE(6,*) 'ERROR: Memory for "eqv" could not be allocated'
!       CALL MPI_Abort(MPI_COMM_WORLD, i_error)
!       STOP
!     ELSE
!       coords%eqv = (/(ion, ion=1,coords%n_ions)/)
!     ENDIF

      IF ( rank_0 ) THEN
         WRITE(6,*) '   COORDS set from POSCAR'
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE file_coords_SET_FROM_POSCAR

   !=================================================================================================================================

   SUBROUTINE file_coords_WRITE( coords )
      !
      !  Purpose:
      !    Write a SUBSPACE file
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    29/10/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_coords ), INTENT(IN) :: coords

      INTEGER :: ioerror    !error code for file IO

      CHARACTER(len=32), DIMENSION(3) :: fmt_T_local

      INTEGER :: ion, i         !loop indices

      !-------------------------------------------------------------------------------------------------------------------------------

      !===========================================================================================================================
      !Format descriptors
      !===========================================================================================================================

2006  FORMAT(1X,"#local coordinate systems")

      fmt_T_local(1) = '(1X,"ex_ =")'
      fmt_T_local(2) = '(1X,"ey_ =")'
      fmt_T_local(3) = '(1X,"ez_ =")'

2007  FORMAT(1X,F17.14,1X,F17.14,1X,F17.14)

2008  FORMAT(1X,"#indices of symmetry equivalent ions")
2009  FORMAT(1X,I4)

      IF ( rank_0 ) THEN

         WRITE(6,*) '   rank 0: WRITING COORDS ...'

         !check if COORDS exists
         OPEN (UNIT=26,FILE=coords%filename,STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
         IF ( ioerror /= 0 ) THEN
            WRITE(6,*) 'ERROR: cannot write COORDS'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         !-

         !header
         WRITE(26,2006)

         !basis vectors stored in columns written as rows
         DO ion=1,coords%n_ions
            DO i=1,3
               WRITE(26,TRIM(fmt_T_local(i)),ADVANCE='NO')
               WRITE(26,2007) coords%T_local(:,i,ion)
            ENDDO
            IF (ion < coords%n_ions) THEN
               WRITE(26,*)
            ENDIF
         ENDDO

!       !symmetry equivalent ions
!       WRITE(26,2008)
!       DO ion=1,coords%n_ions-1
! 	WRITE(26, 2009, ADVANCE='NO') coords%eqv(ion)
!       ENDDO
!       WRITE(26, 2009, ADVANCE='NO') coords%eqv(coords%n_ions)

         !-

         CLOSE(26)

         WRITE(6,*) '-'

      ENDIF

   END SUBROUTINE file_coords_WRITE

   !=================================================================================================================================

   SUBROUTINE file_coords_SET_FROM_FILE( coords, poscar )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    29/10/15     S. Barthel	Original code
      !

      USE type_file_poscar  !this forces the data structure to be non-private

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_coords ), INTENT(INOUT) :: coords
      TYPE( file_poscar ), INTENT(IN) :: poscar

      CHARACTER(len=32), DIMENSION(3) :: fmt_T_local

      INTEGER :: ioerror, allocerror          !error codes for allocation and file IO

      INTEGER :: ion, i         !loop indices

      !-------------------------------------------------------------------------------------------------------------------------------

      !===========================================================================================================================
      !Format descriptors
      !===========================================================================================================================

      !2006 FORMAT(1X,"#local coordinate systems")
2006  FORMAT(1X,25X)

!     fmt_T_local(1) = '(1X,"ex_ =")'
!     fmt_T_local(2) = '(1X,"ey_ =")'
!     fmt_T_local(3) = '(1X,"ez_ =")'

      fmt_T_local(1) = '(1X,5X)'
      fmt_T_local(2) = '(1X,5X)'
      fmt_T_local(3) = '(1X,5X)'

2007  FORMAT(1X,F17.14,1X,F17.14,1X,F17.14)

      !2008 FORMAT(1X,"#indices of symmetry equivalent ions")
2008  FORMAT(1X,36X)
2009  FORMAT(1X,I4)

      !init
      coords%n_ions = poscar%n_ions

      IF ( rank_0 ) THEN

         OPEN (UNIT=26,FILE=coords%filename, STATUS='OLD', ACTION='READ', IOSTAT=ioerror)

         IF ( ioerror /= 0 ) THEN
            WRITE(6,*) 'ERROR: cannot read COORDS'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

      ENDIF

      !allocate
      ALLOCATE ( coords%T_local(3,3,coords%n_ions), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "T_local" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ENDIF

!     ALLOCATE ( coords%eqv(coords%n_ions), STAT=allocerror )
!     IF ( allocerror > 0 ) THEN
!       WRITE(6,*) 'ERROR: Memory for "eqv" could not be allocated'
!       CALL MPI_Abort(MPI_COMM_WORLD, i_error)
!       STOP
!     ENDIF

      IF ( rank_0 ) THEN

         READ(26,2006)	!local coordinate systems header

         !read (basis vectors stored in columns read as rows)
         coords%T_local = 0.
         DO ion=1,coords%n_ions
            DO i=1,3
               READ(26,TRIM(fmt_T_local(i)),ADVANCE='NO')  !wow, this did actually work ?
               READ(26,*) coords%T_local(:,i,ion)
            ENDDO
            IF (ion < coords%n_ions) THEN
               READ(26,*)
            ENDIF
         ENDDO

!       !symmetry equivalent ions
!       READ(26,*)
!       READ(26,*) coords%eqv

         CLOSE(26)

      ENDIF

      !broadcast
      srcnt = 3*3*coords%n_ions
      CALL MPI_BCAST(coords%T_local,  srcnt, MPI_DOUBLE, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

!     srcnt = coords%n_ions
!     CALL MPI_BCAST(coords%eqv,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
!     CALL check_mpi_success

      !WRITE(6,*) myrank, coords%eqv

      IF ( rank_0 ) THEN
         WRITE(6,*) '   COORDS set from FILE'
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE file_coords_SET_FROM_FILE

END MODULE type_file_coords
