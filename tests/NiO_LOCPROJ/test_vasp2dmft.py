import unittest
import subprocess
import difflib
import os, sys


class TestVasp2DMFT(unittest.TestCase):

    def setUp(self):
        """
        Makes sure that there is no SUBSPACE and COORDS file in the dir
        """
        try:
            self.executable = os.environ["EXEC"]
        except(KeyError):
            print("")
            print("this test assumes the executable of the interaface to be an")
            print("environment variable 'EXEC'. But 'EXEC' seems not defined! Try")
            print("export EXEC='../../vasp2dmft'")
            print("")
            print("Exiting...")
            print("")
            raise Exception(KeyError)
        #self.file = sys.argv[0]
        #self.path = os.path.abspath(os.path.dirname(self.file))
        #print('running in '+self.path)
        #print(self.executable)
        files = ['SUBSPACE', 'COORDS']
        for file in files:
            if os.path.isfile(file):
                os.remove(file)

    def tearDown(self):
        files = ['SUBSPACE','COORDS','P_fBZ.proj', 'H_fBZ.proj', 'N.proj',
                 'GAMMA', 'TRACE_ext', 'TRACE_loc', 'vasp.lock',
                 'delta_ext.TXT', 'delta_loc.TXT', 'dens_G0_k_loc_MPI.TXT',
                 'dens_G_k_loc_MPI.TXT', 'MU_INTERFACE.TXT','fort.6',
                 'SIGMA.DAT']
        for file in files:
            if os.path.isfile(file):
                os.remove(file)
    
    def assertFileEqual(self, file1, file2):
        """
        Compares two files using difflib.
        Empty lines are ignored.
        Files are assumed to be relatively small;
        the data is truncated for files larger than MAX_SIZE bytes.
        """
        MAX_SIZE = 100000
        with open(file1, 'r') as f1:
            str1 = f1.read(MAX_SIZE)
        with open(file2, 'r') as f2:
            str2 = f2.read(MAX_SIZE)
        #
        # Make a diff
        #
        # Remove empty lines
        lstr1 = filter(lambda s: s.strip() != '', str1.splitlines(True))
        lstr2 = filter(lambda s: s.strip() != '', str2.splitlines(True))
        # diff
        delta = difflib.unified_diff(lstr1, lstr2)
        # combine delta's to a string
        diff = ''.join(delta)
        # if 'diff' is non-empty, files are different
        if diff:
            return self.fail("Files '%s' and '%s' differ"%(file1, file2))
        
    def test_input_guess_and_output_nocsc(self):
        # run the interface with no SUBSPACE and COORDS file present
        # check if the interface guesses these files correctly
        subprocess.call(self.executable)
        self.assertFileEqual('SUBSPACE', 'SUBSPACE.test')
        self.assertFileEqual('COORDS', 'COORDS.test')
        # rerun the interface with SUBSPACE and COORDS present
        subprocess.call(self.executable)
        #subprocess.call(self.executable)
        self.assertFileEqual('P_fBZ.proj','P_fBZ.proj.test')
        self.assertFileEqual('H_fBZ.proj','H_fBZ.proj.test')
        self.assertFileEqual('N.proj','N.proj.test')

    def test_output_GAMMA_T0_static(self):
        # run the interface with no SUBSPACE and COORDS file present
        # check if the interface guesses these files correctly
        subprocess.call("cp COORDS.test COORDS",shell=True)
        subprocess.call("cp SUBSPACE.test SUBSPACE",shell=True)
        subprocess.call("cp SIGMA.DAT_static SIGMA.DAT",shell=True)
        # rerun the interface with SUBSPACE and COORDS present
        # here wich charge self-consistency flag 'T'
        subprocess.call([self.executable,"T"])
        self.assertFileEqual('GAMMA','GAMMA.static_test')

    def test_output_GAMMA_Tfinite_dynamic(self):
        # run the interface with no SUBSPACE and COORDS file present
        # check if the interface guesses these files correctly
        subprocess.call("cp COORDS.test COORDS",shell=True)
        subprocess.call("cp SUBSPACE_Tfinite SUBSPACE",shell=True)
        subprocess.call("cp SIGMA.DAT_dynamic SIGMA.DAT",shell=True)
        # rerun the interface with SUBSPACE and COORDS present
        # here wich charge self-consistency flag 'T'
        subprocess.call([self.executable,"T"])
        with open('MU_INTERFACE.TXT','r') as f:
            mu0, mu = [float(x) for x in f.readline().split()]
        self.assertTrue(abs(mu0 - 5.4590510530543757)<1e-6)
        self.assertTrue(abs(mu - 2.76)<0.1)

if __name__ == '__main__':
    unittest.main()
