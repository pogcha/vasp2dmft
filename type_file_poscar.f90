MODULE type_file_poscar
!
!  Purpose:
!    Import external input
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    10/10/15     S. Barthel	Original code
!

   USE parallel
   USE fileio

   IMPLICIT NONE

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units

!===================================================================================================================================

   TYPE :: file_poscar !data container for POSCAR

      !PRIVATE                                                 !no access granted to member variables

      REAL, DIMENSION(3) :: scaling_A         !Universal scaling factors (lattice constants)
      REAL, DIMENSION(3,3) :: lattice_A       !Lattice vectors defining the unit cell
      REAL, DIMENSION(3,3) :: lattice_B       !Reciprocal lattice vectors ( vasp: exp(i*2pi*A*B)=1 )

      INTEGER :: n_species                                                !number of atomic species
      INTEGER, ALLOCATABLE, DIMENSION(:) :: n_per_species                 !number of atoms per atomic species
      INTEGER :: n_ions                                                   !Total number of ions on the unit cell
      CHARACTER :: lattice_mode                                           !Lattice mode
      REAL, ALLOCATABLE, DIMENSION(:,:) :: atomic_positions               !atomic positions within the unit cell

   END TYPE file_poscar

!===================================================================================================================================

CONTAINS

   SUBROUTINE file_poscar_READ( poscar )
      !
      !  Purpose:
      !    Load a POSCAR file
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    29/10/15     S. Barthel	Original code
      !

      USE math

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_poscar ), INTENT(INOUT) :: poscar

      INTEGER :: ioerror, allocerror          !error codes for allocation and file IO
      INTEGER :: i,j                          !loop index

      !-------------------------------------------------------------------------------------------------------------------------------

      IF ( rank_0 ) THEN

         WRITE(6,"(3X,'rank 0: READING data of POSCAR ...')")

         !check if PROCAR exists
         OPEN (UNIT=26,FILE='POSCAR',STATUS='OLD', ACTION='READ', IOSTAT=ioerror)

         IF ( ioerror /= 0 ) THEN
            WRITE(6,*) 'ERROR: POSCAR file missing'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         !Skip comment line
         READ(26,*)

         !Read in lattice constants
         READ(26,"(A)") line_readin
         n_elements = numel_string(line_readin,3)
         IF (n_elements == 1) THEN
            READ(line_readin,*) poscar%scaling_A(1)
            poscar%scaling_A(2:3) = poscar%scaling_A(1)
         ELSEIF (n_elements == 3) THEN
            READ(line_readin,*) poscar%scaling_A(:)
         ELSE
            WRITE(6,*) 'ERROR: POSCAR file corrupted in line 2 !'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
         ! 	WRITE(6,*) scaling_A
         ! 	WRITE(6,*) ''

         !Read in lattice vectors as stored internally in VASP
         READ(26,*) poscar%lattice_A(:,1)
         READ(26,*) poscar%lattice_A(:,2)
         READ(26,*) poscar%lattice_A(:,3)

         !Apply scaling factors
         DO i=1,3
            poscar%lattice_A(:,i) = poscar%lattice_A(:,i)*poscar%scaling_A(:)
         ENDDO

         WRITE(6,"(3X,'Direct lattice vectors:')")
         WRITE(6,"(3X,'A1:',1X,3(F12.9,1X))"), poscar%lattice_A(:,1)
         WRITE(6,"(3X,'A2:',1X,3(F12.9,1X))"), poscar%lattice_A(:,2)
         WRITE(6,"(3X,'A3:',1X,3(F12.9,1X))"), poscar%lattice_A(:,3)
         WRITE(6,*) ''

         !Calculate reciprocal lattice vectors ( vasp: exp(i*2pi*A*B)=1, crystallographers definition, no 2*pi !!! )
         poscar%lattice_B(:,1) = cross_product( poscar%lattice_A(:,2),poscar%lattice_A(:,3) )
         poscar%lattice_B(:,2) = cross_product( poscar%lattice_A(:,3),poscar%lattice_A(:,1) )
         poscar%lattice_B(:,3) = cross_product( poscar%lattice_A(:,1),poscar%lattice_A(:,2) )

         !Apply normalization
         poscar%lattice_B(:,1) = poscar%lattice_B(:,1) / DOT_PRODUCT( poscar%lattice_A(:,1),poscar%lattice_B(:,1) ) !123
         poscar%lattice_B(:,2) = poscar%lattice_B(:,2) / DOT_PRODUCT( poscar%lattice_A(:,2),poscar%lattice_B(:,2) ) !231
         poscar%lattice_B(:,3) = poscar%lattice_B(:,3) / DOT_PRODUCT( poscar%lattice_A(:,3),poscar%lattice_B(:,3) ) !312

         WRITE(6,"(3X,'Reciprocal lattice vectors:')")
         WRITE(6,"(3X,'B1:',1X,3(F12.9,1X))"), poscar%lattice_B(:,1)
         WRITE(6,"(3X,'B2:',1X,3(F12.9,1X))"), poscar%lattice_B(:,2)
         WRITE(6,"(3X,'B3:',1X,3(F12.9,1X))"), poscar%lattice_B(:,3)
         WRITE(6,*) ''

!       WRITE(6,*) DOT_PRODUCT( poscar%lattice_A(:,1),poscar%lattice_B(1,:) ), &
!         DOT_PRODUCT( poscar%lattice_A(:,2),poscar%lattice_B(:,2) ), &
!         DOT_PRODUCT( poscar%lattice_A(:,3),poscar%lattice_B(:,3) )

         !Number of ions of each type
         READ(26,*)
         READ(26,"(A)") line_readin
         poscar%n_species = numel_string(line_readin,128)

      ENDIF

      !broadcast data
      srcnt = 3
      CALL MPI_BCAST(poscar%scaling_A,  srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      srcnt = 9
      CALL MPI_BCAST(poscar%lattice_A,  srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      CALL MPI_BCAST(poscar%lattice_B,  srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      srcnt = 1
      CALL MPI_BCAST(poscar%n_species,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      !allocate
      ALLOCATE( poscar%n_per_species(poscar%n_species), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "n_per_species" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE

         poscar%n_per_species = 0

         IF ( rank_0 ) THEN
            READ(line_readin,*) poscar%n_per_species(:)
            poscar%n_ions = SUM(poscar%n_per_species)
            IF (poscar%n_ions == 0) THEN
               WRITE(6,*) "ERROR: POSCAR seems corrupt: Number of atoms is zero!"
               WRITE(6,*) "Probably because line with element names is missing."
               CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            endif
            !WRITE(6,*) poscar%n_per_species, poscar%n_ions
            !we can check later if this agrees with PROCAR n_ions
         ENDIF

         !broadcast data
         srcnt = poscar%n_species
         CALL MPI_BCAST(poscar%n_per_species,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
         CALL check_mpi_success
         srcnt = 1
         CALL MPI_BCAST(poscar%n_ions,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
         CALL check_mpi_success

      ENDIF

      IF ( rank_0 ) THEN
         !Selective dynamics switch (unused)
         READ(26,*) poscar%lattice_mode
         !WRITE(6,*) 'Lattice mode: ', poscar%lattice_mode

         IF ( poscar%lattice_mode == 'S' .OR. poscar%lattice_mode == 's' ) THEN
            WRITE(6,*) '  Selective dynamics not supported yet.'
            READ(26,*) poscar%lattice_mode
         ENDIF

         IF ( poscar%lattice_mode == 'C' .OR. poscar%lattice_mode == 'c' .OR. &
            poscar%lattice_mode == 'K' .OR. poscar%lattice_mode == 'k') THEN
            WRITE(6,"(3X,'Atomic positions in cartesian coordinates:')")
         ELSE
            WRITE(6,"(3X,'Atomic positions in direct (fractional) coordinates:')")
         ENDIF

      ENDIF

      srcnt = 1
      CALL MPI_BCAST(poscar%lattice_mode,  srcnt, MPI_CHARACTER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      ALLOCATE( poscar%atomic_positions(3,poscar%n_ions), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "atomic_positions" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE

         poscar%atomic_positions = 0.

         IF ( rank_0 ) THEN

            DO i=1,poscar%n_ions
               READ(26,*) poscar%atomic_positions(:,i)
               WRITE(6,"(3X,3(F12.9,1X))") poscar%atomic_positions(:,i)
            ENDDO
            WRITE(6,*) ''

            !close POSCAR file
            CLOSE(26)

            IF ( poscar%lattice_mode == 'C' .OR. poscar%lattice_mode == 'c' .OR. &
               poscar%lattice_mode == 'K' .OR. poscar%lattice_mode == 'k') THEN

               WRITE(6,"(3X,'Shift of cartesian atomic positions NOT implemented !')")
               WRITE(6,"(3X,'Phase factors may differ.')")

               !Atomic positions in Cartesian-Mode
               DO i=1,poscar%n_ions
                  poscar%atomic_positions(:,i) = poscar%atomic_positions(:,i)*poscar%scaling_A
               ENDDO

            ELSE

               !DEV: Shift tau-coordinates from [0,1] to [-0.5,0.5]
               DO j=1,poscar%n_ions
                  DO i=1,3
                     IF ( poscar%atomic_positions(i,j) >= 0.5 ) THEN
                        poscar%atomic_positions(i,j) = poscar%atomic_positions(i,j) - 1.0
                     ELSEIF ( poscar%atomic_positions(i,j) < -0.5 ) THEN
                        poscar%atomic_positions(i,j) = poscar%atomic_positions(i,j) + 1.0
                     ENDIF
                  ENDDO
               ENDDO

               !WRITE(6,"(3X,'Atomic positions (shifted):')")
               !DO i=1,poscar%n_ions
               !   WRITE(6,"(3X,3(F12.9,1X))") poscar%atomic_positions(:,i)
               !ENDDO
               !WRITE(6,*) ''

               !Atomic positions in Direct/Fractional-Mode
               DO i=1,poscar%n_ions
                  poscar%atomic_positions(:,i) = poscar%atomic_positions(1,i)*poscar%lattice_A(:,1) &
                     + poscar%atomic_positions(2,i)*poscar%lattice_A(:,2) &
                     + poscar%atomic_positions(3,i)*poscar%lattice_A(:,3)
               ENDDO

            ENDIF

            !WRITE(6,"(3X,'Atomic positions (evaluated):')")
            !DO i=1,poscar%n_ions
            !   WRITE(6,"(3X,3(F12.9,1X))") poscar%atomic_positions(:,i)
            !ENDDO
            !WRITE(6,*) ''

         ENDIF

         !broadcast
         srcnt = 3*poscar%n_ions
         CALL MPI_BCAST(poscar%atomic_positions,  srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
         CALL check_mpi_success

      ENDIF

      IF ( rank_0 ) THEN
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE file_poscar_READ

   !=================================================================================================================================

END MODULE type_file_poscar
