MODULE type_file_subspace_procar
!
!  Purpose:
!    Import external input
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    10/10/15     S. Barthel	Original code
!

   USE parallel
   USE fileio

   IMPLICIT NONE

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units

!===================================================================================================================================

   TYPE :: vasp_subspace                             !this type stores the ion and orbital indices of VASP

      INTEGER :: ion                                  !VASP index of correlated ion
      INTEGER :: n_orbitals                           !number of correlated orbitals at atomic-site

      INTEGER, ALLOCATABLE, DIMENSION(:) :: orbital   !VASP indices of correlated orbitals

   END TYPE vasp_subspace

!-----------------------------------------------------------------------------------------------------------------------------------

   TYPE :: file_subspace_procar !data container for SUBSPACE

      !PRIVATE                                                  !no access granted to member variables

      CHARACTER(16) :: filename = 'SUBSPACE'                    !default filename
      CHARACTER(7) :: hk_fmt
      LOGICAL :: is_whitelist = .TRUE.                          !flag to white-/blacklist band selection
      LOGICAL :: is_T0 = .TRUE.                          !flag to white-/blacklist band selection

      INTEGER :: n_bands                                        !number of selected bands in band-subspace
      INTEGER, ALLOCATABLE, DIMENSION(:) :: band_indices        !list of selected band indices in band-subspace

      INTEGER :: n_ions                                         !number of selected ions to be considered as "correlated"
      INTEGER :: n_orbitals                                     !number of selected orbitals to be considered "correlated"

      TYPE ( vasp_subspace ), ALLOCATABLE, DIMENSION(:)  :: L   !correlated subspace (holding VASP indices)

      INTEGER :: n_shells                                       ! number of atomic shells
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: shell_info		! information about shells (which atom, type, L, dimension)

      INTEGER :: n_corr_shells                                  ! number of correlated atomic shells
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: corr_shells_info	! information about shells (which atom, type, L, dimension, SO, interactions)

      INTEGER :: n_irreps
      INTEGER, ALLOCATABLE, DIMENSION(:) :: irrep_info		    ! information about irreps (number of irreducible representations, and the dimensions of the irreps)


   END TYPE file_subspace_procar

!===================================================================================================================================

CONTAINS

   SUBROUTINE file_subspace_procar_SET_FILENAME( subspace, filename )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    30/10/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_subspace_procar ), INTENT(INOUT) :: subspace
      CHARACTER(16), INTENT(IN) :: filename

      !-------------------------------------------------------------------------------------------------------------------------------

      subspace%filename = TRIM(ADJUSTL(filename))

   END SUBROUTINE file_subspace_procar_SET_FILENAME

   !=================================================================================================================================

   SUBROUTINE file_subspace_procar_INIT( subspace, procar )
      !
      !  Purpose:
      !    Read a SUBSPACE file
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    29/10/15     S. Barthel	Original code
      !

      USE type_file_procar  !this forces the data structure to be non-private

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_subspace_procar ), INTENT(INOUT) :: subspace
      TYPE( file_procar ), INTENT(IN) :: procar

      INTEGER :: ioerror          !error code for file IO

      !-------------------------------------------------------------------------------------------------------------------------------

      IF ( rank_0 ) THEN
         OPEN (UNIT=26,FILE=subspace%filename, STATUS='OLD', ACTION='READ', IOSTAT=ioerror)
         CLOSE(26)
      ENDIF

      srcnt = 1
      CALL MPI_BCAST(ioerror,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      IF ( ioerror /= 0 ) THEN

         IF (rank_0) THEN
            WRITE(6,*) '   rank 0: Creating SUBSPACE ...'
         ENDIF

         CALL file_subspace_procar_SET( subspace, procar )
         CALL file_subspace_procar_WRITER( subspace )

         WRITE(6,*) '   rank 0: Generated default SUBSPACE. Check and rerun! Exiting...'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP

      ELSE

         IF (rank_0) THEN
            WRITE(6,*) '   rank 0: Reading SUBSPACE ...'
         ENDIF

         CALL file_subspace_procar_SET_FROM_FILE( subspace, procar )
      ENDIF

   END SUBROUTINE file_subspace_procar_INIT

   !=================================================================================================================================

 FUNCTION l_from_orb_type(orbital_type)
        implicit NONE
        INTEGER :: orbital_type
        INTEGER :: l_from_orb_type
        IF (orbital_type == 1) then
            l_from_orb_type = 0
        elseif ( ( orbital_type > 1 ) .AND. (orbital_type < 5) ) then
            l_from_orb_type = 1
        elseif ( ( orbital_type > 4 ) .AND. (orbital_type < 10) ) then
            l_from_orb_type = 2
        elseif ( orbital_type > 9 ) then
            l_from_orb_type = 3
        END IF

        RETURN
    end function


 SUBROUTINE guess_triqs_header(subspace)


        TYPE( file_subspace_procar ), INTENT(INOUT) :: subspace

        INTEGER :: band, orbital, shell, corr_shell       !loop indices
        INTEGER, DIMENSION(2) :: current, new

        INTEGER :: allocerror          !error code for allocation

        ! go through all orbitals and check to how many shells they belong
        ! starting with first orb, save atom and l

        subspace%n_shells = subspace%n_ions
        subspace%n_corr_shells = subspace%n_shells

        ! if we have spare bands. we dump then into a dummy shell. Thus we add one to the number of shells

        IF (subspace%n_orbitals < subspace%n_bands) THEN
            subspace%n_shells = subspace%n_shells + 1
        ENDIF

        ALLOCATE ( subspace%shell_info( subspace%n_shells, 4 ), STAT=allocerror )
        IF ( allocerror > 0 ) THEN
             WRITE(6,*) 'ERROR: Memory for "shell_info" could not be allocated'
             CALL MPI_Abort(MPI_COMM_WORLD, i_error)
             STOP
        ENDIF

        ALLOCATE ( subspace%corr_shells_info( subspace%n_corr_shells, 6 ), STAT=allocerror )
        IF ( allocerror > 0 ) THEN
             WRITE(6,*) 'ERROR: Memory for "n_corr_shells" could not be allocated'
             CALL MPI_Abort(MPI_COMM_WORLD, i_error)
             STOP
        ENDIF

        DO shell = 1, subspace%n_shells
            subspace%shell_info(shell,1) = shell
            subspace%shell_info(shell,2) = shell
            subspace%shell_info(shell,3) = 2
            subspace%shell_info(shell,4) = 5
        END DO

        IF (subspace%n_orbitals < subspace%n_bands) THEN
            subspace%shell_info(subspace%n_shells,3) = -1
            subspace%shell_info(subspace%n_shells,4) = subspace%n_bands - subspace%n_orbitals
        ENDIF

        subspace%corr_shells_info(:,:) = 0

        DO shell = 1, subspace%n_corr_shells
            subspace%corr_shells_info(shell,1) = shell
            subspace%corr_shells_info(shell,2) = shell
            subspace%corr_shells_info(shell,3) = 2
            subspace%corr_shells_info(shell,4) = 5
            subspace%corr_shells_info(shell,5) = 0
            subspace%corr_shells_info(shell,6) = 0
        END DO

        subspace%n_irreps = 1

        ALLOCATE ( subspace%irrep_info( subspace%n_irreps ), STAT=allocerror )
        IF ( allocerror > 0 ) THEN
             WRITE(6,*) 'ERROR: Memory for "n_corr_shells" could not be allocated'
             CALL MPI_Abort(MPI_COMM_WORLD, i_error)
             STOP
        ENDIF
        subspace%irrep_info(1) = SUM( subspace%corr_shells_info(:,4) )

    END SUBROUTINE guess_triqs_header

   SUBROUTINE file_subspace_procar_SET( subspace, procar )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    29/10/15     S. Barthel	Original code
      !

      USE type_file_procar  !this forces the data structure to be non-private

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_subspace_procar ), INTENT(INOUT) :: subspace
      TYPE( file_procar ), INTENT(IN) :: procar

      INTEGER :: allocerror          !error code for allocation
      INTEGER :: band, ion                    !loop indices

      !-------------------------------------------------------------------------------------------------------------------------------

      !init number of selected bands in band-subspace from PROCAR
      subspace%n_bands = procar%n_bands

      !deallocate possibly
      IF ( ALLOCATED(subspace%band_indices) ) THEN
         DEALLOCATE(subspace%band_indices, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "band_indices" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !allocate
      ALLOCATE ( subspace%band_indices( subspace%n_bands ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "band_indices" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         !init list of selected band indices in band-subspace from PROCAR
         subspace%band_indices = (/(band, band=1,subspace%n_bands)/)

      ENDIF

      !init number of selected ions to be considered as "correlated" from PROCAR
      subspace%n_ions = procar%n_ions

      ! init h(k) format to triqs
       subspace%hk_fmt = 'triqs'

      ALLOCATE ( subspace%L(subspace%n_ions), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "L" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE

         !init number of selected orbitals to be considered "correlated" from PROCAR, all d-orbitals
         subspace%n_orbitals = 5*subspace%n_ions

         DO ion=1,subspace%n_ions
            ALLOCATE ( subspace%L(ion)%orbital(5), STAT=allocerror  )
            IF ( allocerror > 0 ) THEN
               WRITE(6,*) 'ERROR: Memory for "L%orbital" could not be allocated'
               CALL MPI_Abort(MPI_COMM_WORLD, i_error)
               STOP
            ELSE

               subspace%L(ion)%ion = ion
               subspace%L(ion)%n_orbitals = 5
               subspace%L(ion)%orbital(:) = (/5,6,7,8,9/)   !d-orbitals

            ENDIF
         ENDDO

      ENDIF

      IF ( rank_0 ) THEN
         WRITE(6,*) '   Guessing triqs header from PROCAR. This is almost certainly wrong! CHECK SUBSPACE!'
         WRITE(6,*) '-'
      ENDIF
      CALL guess_triqs_header(subspace)


      IF ( rank_0 ) THEN
         WRITE(6,*) '   SUBSPACE set from PROCAR'
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE file_subspace_procar_SET

   !=================================================================================================================================


SUBROUTINE Hk_triqs_writer(subspace,file_id)

    implicit none

    TYPE( file_subspace_procar ), INTENT(IN) :: subspace
    INTEGER, INTENT(IN) :: file_id          !error code for allocation

    INTEGER :: shell, band

    2002  FORMAT(1X,I3)

    2003  FORMAT(/)

    2007  FORMAT(1X,'#H(k) header for triqs'/,1X,'n_shell=',1X,I3)

    2009 FORMAT(1X,'shell_type (',I3,'  )=',1X)

    2010  FORMAT(1X,'n_corr_shell=',1X,I3)

    2011  FORMAT(1X,'irreps=')


         WRITE(file_id,2007) subspace%n_shells

         DO shell = 1, subspace%n_shells
            WRITE(file_id,2009,ADVANCE='NO') shell
            DO band = 1, 4
                WRITE(26,2002,ADVANCE='NO') subspace%shell_info(shell,band)
            END DO
            WRITE(file_id,2003,ADVANCE='NO')
         END DO

         WRITE(file_id,2010) subspace%n_corr_shells

         DO shell = 1, subspace%n_corr_shells
            WRITE(file_id,2009,ADVANCE='NO') shell
            DO band = 1, 6
                WRITE(file_id,2002,ADVANCE='NO') subspace%corr_shells_info(shell,band)
            END DO
            WRITE(file_id,2003,ADVANCE='NO')
         END DO

         WRITE(file_id,2011,ADVANCE='NO')
         WRITE(file_id,2002,ADVANCE='NO') subspace%n_irreps
         DO band = 1, subspace%n_irreps
            WRITE(file_id,2002,ADVANCE='NO') subspace%irrep_info(band)
         END DO
         WRITE(file_id,2003,ADVANCE='NO')

END SUBROUTINE Hk_triqs_writer

   SUBROUTINE file_subspace_procar_WRITER( subspace )
      !
      !  Purpose:
      !    Write a SUBSPACE file
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    29/10/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_subspace_procar ), INTENT(IN) :: subspace

      INTEGER :: ioerror          	!error code for file IO
      INTEGER :: band, ion, orbital           !loop indices

      !-------------------------------------------------------------------------------------------------------------------------------

      !===========================================================================================================================
      !Format descriptors
      !===========================================================================================================================

      !Format descriptors for the correlated subspace file
2000  FORMAT(1X,'#flag to white-/blacklist band selection'/,1X,'is_whitelist=',1X,L1,/)
2010  FORMAT(1X,'#flag to white-/blacklist band selection'/,1X,'is_whitelist=',A,/)

2001  FORMAT(1X,'#list of selected band indices'/,1X,'bands_list=')
2011  FORMAT(1X,'#list of selected band indices'/,1X,'bands_list=',A,/)

2002  FORMAT(1X,I3)
2003  FORMAT(/)

2004  FORMAT(1X,'#correlated subspace µ(ion) : L(orbital), (1=s, 2=py, 3=pz, 4=px, 5=dxy, 6=dyz, 7=dz², 8=dxz, 9=dx², &
         10=f-3, 11=f-2, 12=f-1, 13=f0, 14=f1, 15=f2, 16=f3)')
2005  FORMAT(1X,I3,1X,':')
2015  FORMAT(1X,A)
2006  FORMAT(1X,'#flag to do T=0 calculations (only relevant for charge self consistency)'/,1X,'is_T0=',1X,L1,/)
2007  FORMAT(1X,'#flag to choose H(k) file format (triqs / wanni)'/,1X,'Hk_fmt=',1X,A,/)

      IF ( rank_0 ) THEN

         WRITE(6,*) '   rank 0: WRITING SUBSPACE ...'

         !check if SUBSPACE exists
         OPEN (UNIT=26,FILE=subspace%filename,STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
         IF ( ioerror /= 0 ) THEN
            WRITE(6,*) 'ERROR: cannot write SUBSPACE'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         WRITE(26,2000) subspace%is_whitelist

         WRITE(26,2006) subspace%is_T0

         WRITE(26,2007) TRIM(ADJUSTL(subspace%hk_fmt))

         WRITE(26,2001,ADVANCE='NO')
         DO band=1,subspace%n_bands
            WRITE(26,2002,ADVANCE='NO') subspace%band_indices(band)
         ENDDO
         WRITE(26,2003)

         WRITE(26,2004)

         DO ion=1,subspace%n_ions
            WRITE(26,2005,ADVANCE='NO') subspace%L(ion)%ion
            DO orbital=1,subspace%L(ion)%n_orbitals-1
               WRITE(26,2002,ADVANCE='NO') subspace%L(ion)%orbital(orbital)
            ENDDO
            WRITE(26,2002) subspace%L(ion)%orbital(subspace%L(ion)%n_orbitals)  !odd last blank row override
         ENDDO

        WRITE(26,*)

        CALL Hk_triqs_writer(subspace, 26)

         CLOSE(26)

         WRITE(6,*) '-'

      ENDIF

   END SUBROUTINE file_subspace_procar_WRITER

   !=================================================================================================================================


SUBROUTINE hk_triqs_reader(subspace,file_id)

    IMPLICIT NONE

    TYPE( file_subspace_procar ), INTENT(INOUT) :: subspace
    INTEGER, INTENT(IN) :: file_id

    INTEGER :: ioerror, allocerror          !error codes for allocation and file IO
    INTEGER :: ion, shell                    !loop indices


    2007  FORMAT(1X,'#H(k) header for triqs'/,1X,'n_shell=',1X,I3)

    2017  FORMAT(1X,9X,I3)

    2019 FORMAT(1X,12X,3X,4X,A)

    2020  FORMAT(1X,13X,1X,I3)

    2021  FORMAT(1X,8X,I3,A)



    IF ( rank_0 ) then
        READ(file_id,*) line_readin
        READ(file_id,2017) subspace%n_shells
    END IF

    ALLOCATE ( subspace%shell_info( subspace%n_shells, 4 ), STAT=allocerror )
     IF ( allocerror > 0 ) THEN
        WRITE(6,*) 'ERROR: Memory for "shell_info" could not be allocated'
        CALL MPI_Abort(MPI_COMM_WORLD, i_error)
        STOP
     ENDIF

    IF ( rank_0 ) then
        DO shell = 1, subspace%n_shells
            READ(file_id,2019) line_readin
            READ(line_readin,*) subspace%shell_info(shell,:)
        ENDDO
    END IF

    IF ( rank_0 ) then
        READ(file_id,2020) subspace%n_corr_shells
    END IF

    ALLOCATE ( subspace%corr_shells_info( subspace%n_corr_shells, 6 ), STAT=allocerror )
     IF ( allocerror > 0 ) THEN
        WRITE(6,*) 'ERROR: Memory for "shell_info" could not be allocated'
        CALL MPI_Abort(MPI_COMM_WORLD, i_error)
        STOP
     ENDIF

    IF ( rank_0 ) then
        DO shell = 1, subspace%n_corr_shells
            READ(file_id,2019) line_readin
            READ(line_readin,*) subspace%corr_shells_info(shell,:)
        ENDDO
    END IF
    WRITE(6,*) '   checking SUBSPACE'
    IF ( subspace%hk_fmt == 'triqs' ) THEN
        IF ( SUM(subspace%shell_info(:,4)) /= subspace%n_bands ) then
            WRITE(6,*) 'ERROR: total number of shell dimensions has to be equal to number of bands!'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
        ENDIF
        IF ( subspace%n_bands > subspace%n_orbitals ) then
            IF ( SUM(subspace%corr_shells_info(:,4)) > SUM(subspace%shell_info(:subspace%n_shells-1,4)) ) then
                WRITE(6,*) 'ERROR: total number of corr shell dimensions can not be larger '
                WRITE(6,*) 'than total number of orbitals defined in correlated subspace!'
                CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                STOP
            ENDIF
        else
            IF ( SUM(subspace%corr_shells_info(:,4)) > SUM(subspace%shell_info(:,4)) ) then
                WRITE(6,*) 'ERROR: total number of corr shell dimensions can not be larger '
                WRITE(6,*) 'than total number of orbitals defined in correlated subspace!'
                CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                STOP
            ENDIF
        END IF


    ENDIF


    IF ( rank_0 ) then
        READ(file_id,2021) subspace%n_irreps, line_readin
    END IF

    ALLOCATE ( subspace%irrep_info( subspace%n_irreps ), STAT=allocerror )
     IF ( allocerror > 0 ) THEN
        WRITE(6,*) 'ERROR: Memory for "n_irreps" could not be allocated'
        CALL MPI_Abort(MPI_COMM_WORLD, i_error)
        STOP
     ENDIF

    IF ( rank_0 ) then
        READ(line_readin,*) subspace%irrep_info(:)
    END IF

    srcnt = 1
    CALL MPI_BCAST(subspace%n_shells,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
    CALL check_mpi_success

    srcnt = 1
    CALL MPI_BCAST(subspace%n_corr_shells,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
    CALL check_mpi_success

    srcnt = 1
    CALL MPI_BCAST(subspace%n_irreps,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
    CALL check_mpi_success

    srcnt = subspace%n_shells*4
    CALL MPI_BCAST(subspace%shell_info,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
    CALL check_mpi_success

    srcnt = subspace%n_corr_shells*6
    CALL MPI_BCAST(subspace%corr_shells_info,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
    CALL check_mpi_success

    srcnt = subspace%n_irreps
    CALL MPI_BCAST(subspace%irrep_info,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
    CALL check_mpi_success


 END SUBROUTINE hk_triqs_reader


   SUBROUTINE file_subspace_procar_SET_FROM_FILE( subspace, procar )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    29/10/15     S. Barthel	Original code
      !

      USE type_file_procar  !this forces the data structure to be non-private

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_subspace_procar ), INTENT(INOUT) :: subspace
      TYPE( file_procar ), INTENT(IN) :: procar

      INTEGER :: ioerror, allocerror          !error codes for allocation and file IO
      INTEGER :: ion                    !loop indices

      !===============================================================================================================================
      !Format descriptors
      !===============================================================================================================================

      !Format descriptors for the correlated subspace file
2000  FORMAT(1X,'#flag to white-/blacklist band selection'/,1X,'is_whitelist=',1X,L1,/)
      !2010 FORMAT(1X,'#flag to white-/blacklist band selection'/,1X,'is_whitelist=',A,/)
2010  FORMAT(1X,40X/,1X,13X,A,/)

2001  FORMAT(1X,'#list of selected band indices'/,1X,'bands_list=')
      !2011 FORMAT(1X,'#list of selected band indices'/,1X,'bands_list=',A,/)
2011  FORMAT(1X,30X/,1X,11X,A,/)

2002  FORMAT(1X,I3)
2003  FORMAT(/)

2004  FORMAT(1X,'#correlated subspace µ(ion) : L(orbital), (1=s, 2=py, 3=pz, 4=px, 5=dxy, 6=dyz, 7=dz², 8=dxz, 9=dx², &
         10=f-3, 11=f-2, 12=f-1, 13=f0, 14=f1, 15=f2, 16=f3)')
2005  FORMAT(1X,I3,1X,':')
2015  FORMAT(1X,A)

2006  FORMAT(1X,'#flag to do T=0 calculations (only relevant for charge self consistency)'/,1X,'is_T0=',1X,L1,/)
2016  FORMAT(1X,72X/,1X,6X,A,/)

2007  FORMAT(1X,'#flag to choose H(k) file format (triqs / wanni)'/,1X,'Hk_fmt=',1X,A5,/)
2017  FORMAT(1X,38X/,1X,7X,A,/)


      IF ( rank_0 ) THEN

         OPEN (UNIT=26,FILE=subspace%filename, STATUS='OLD', ACTION='READ', IOSTAT=ioerror)

         IF ( ioerror /= 0 ) THEN
            WRITE(6,*) 'ERROR: cannot read SUBSPACE'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         READ(26,2010) line_readin !read a whole line
         READ(line_readin,*,IOSTAT=ioerror) subspace%is_whitelist  !attempt type conversion of first entry, the rest is ignored
         IF (ioerror /= 0) THEN                                    !check for correctness, else print error
            WRITE(6,*) 'ERROR: "is_whitelist=" has to be of type logical.'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
         !WRITE(6,*) subspace%is_whitelist
         READ(26,2016) line_readin !read a whole line
         READ(line_readin,*,IOSTAT=ioerror) subspace%is_T0  !attempt type conversion of first entry, the rest is ignored
         IF (ioerror /= 0) THEN                                    !check for correctness, else print error
            WRITE(6,*) 'ERROR: "is_T0=" has to be of type logical.'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

          READ(26,2017) line_readin !read a whole line
         READ(line_readin,*,IOSTAT=ioerror) subspace%hk_fmt  !attempt type conversion of first entry, the rest is ignored
         subspace%hk_fmt  = TRIM(ADJUSTL(subspace%hk_fmt ))
         IF (ioerror /= 0) THEN                                    !check for correctness, else print error
            WRITE(6,*) 'ERROR: "Hk=" has to be of type character.'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
         IF (subspace%hk_fmt == 'triqs') THEN
            WRITE(6,*) '   H(k) output file format is triqs'
         ELSEIF (subspace%hk_fmt == 'wannier') THEN
            WRITE(6,*) '   H(k) output file format is wannier'
        ELSE
            WRITE(6,*) 'ERROR: " Hk_fmt=" has to be triqs or wannier.'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
        ENDIF

      ENDIF

      !broadcast header
      srcnt = 1
      CALL MPI_BCAST(subspace%is_whitelist,  srcnt, MPI_LOGICAL, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      CALL MPI_BCAST(subspace%is_T0,  srcnt, MPI_LOGICAL, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      CALL MPI_BCAST(subspace%hk_fmt,  srcnt, MPI_LOGICAL, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      !---

      IF ( rank_0 ) THEN
         READ(26,2011) line_readin !read a whole line
         subspace%n_bands = numel_string(line_readin, procar%n_bands) !maximum of n_bands
         !WRITE(6,*) subspace%n_bands
      ENDIF

      !broadcast header
      srcnt = 1
      CALL MPI_BCAST(subspace%n_bands,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      !---

      IF ( (0 < subspace%n_bands) .AND. (subspace%n_bands <= procar%n_bands) ) THEN
         !allocate
         ALLOCATE ( subspace%band_indices( subspace%n_bands ), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "band_indices" could not be allocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ELSE
         WRITE(6,*) 'ERROR: "0 < band_indices <= bands" not fulfilled.'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ENDIF

      IF ( rank_0 ) THEN
         READ(line_readin,*) subspace%band_indices
         IF ( (ANY(subspace%band_indices<1) .OR. ANY(subspace%band_indices>procar%n_bands)) ) THEN
            WRITE(6,*) '  ERROR: "band_indices=" has to be a non-empty list with elements in [1 bands].'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !broadcast
      srcnt = subspace%n_bands
      CALL MPI_BCAST(subspace%band_indices,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      IF ( rank_0 ) THEN

         !WRITE(6,*) subspace%band_indices

         READ(26,*) !skip header

         !Read correlated subspace until end of file (temporary read in)
         subspace%n_ions = 0
         DO
            READ(26,2015,IOSTAT=ioerror) line_readin
            IF ( ioerror /= 0 ) THEN
               !WRITE(6,*) 'End of file.'
               EXIT
            ENDIF
            ! if the line does not contain a colon, then it is not a 'correlated subspace line'
            ! and we do not count it
            IF ( INDEX(line_readin, ':') == 0 ) THEN
                EXIT
            END IF
            subspace%n_ions = subspace%n_ions + 1
         ENDDO
         REWIND(26) !rewind file
         !WRITE(6,*) subspace%n_ions
      ENDIF

      !broadcast
      srcnt = 1
      CALL MPI_BCAST(subspace%n_ions,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      IF ( (0 < subspace%n_ions) .AND. (subspace%n_ions <= procar%n_ions) ) THEN

         !Allocate correlated subspace
         ALLOCATE ( subspace%L(subspace%n_ions), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "L" could not be allocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

      ELSE
         WRITE(6,*) 'ERROR: "0 < n_ions <= ions" not fulfilled.'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ENDIF

      IF ( rank_0 ) THEN
         !skip lines (ugly)
         READ(26,*)  !bands whitelist header (*, because if we use a format, it would require line_readin)
         READ(26,*)  !bands whitelist
         READ(26,*)
         READ(26,*)  !T=0 header
         READ(26,*)  !T=0
         READ(26,*)
         READ(26,*)  !hk format header
         READ(26,*)  !hk format
         READ(26,*)
         READ(26,*)  !list of band indices header
         READ(26,*)  !list of band indices
         READ(26,*)
         READ(26,*)  !correlated subspace header
      ENDIF

      subspace%n_orbitals = 0

      DO ion=1,subspace%n_ions

         IF ( rank_0 ) THEN
            READ(26,2015) line_readin ! integer, integer array
            ! position of seperating character (:) to mask the input string
            idx_seperator = INDEX(line_readin, ':')
            IF (idx_seperator == 0) THEN
               WRITE(6,*) 'ERROR: Missing seperator ":" in correlated subspace selection.'
               WRITE(6,*) 'ERROR: ', line_readin
               CALL MPI_Abort(MPI_COMM_WORLD, i_error)
               STOP
            ENDIF

            !check ion
            READ(line_readin(1:(idx_seperator-1)),*,IOSTAT=ioerror) subspace%L(ion)%ion
            IF ((ioerror /= 0) .OR. (0 > subspace%L(ion)%ion) .OR. (subspace%L(ion)%ion > procar%n_ions)) THEN !check for correctness, else print error
               WRITE(6,*) 'ERROR: "µ(ion)" has to be a single integer taken from [1 n_ions].'
               WRITE(6,*) 'ERROR: ', line_readin
               CALL MPI_Abort(MPI_COMM_WORLD, i_error)
               STOP
            ENDIF

            !how many orbitals do we have per ion ? allocate memory correspondingly !
            subspace%L(ion)%n_orbitals = numel_string(line_readin((idx_seperator+1):255), 16) !maximum of 16 orbitals, s1p3d5f7

            IF ( .NOT. (0 < subspace%L(ion)%n_orbitals) .AND. (subspace%L(ion)%n_orbitals <= 16) ) THEN
               WRITE(6,*) 'ERROR: "0 < size of L(orbital) <= 16" not fulfilled.'
               WRITE(6,*) 'ERROR: ', subspace%L(ion)%n_orbitals
               WRITE(6,*) 'ERROR: ', line_readin
               CALL MPI_Abort(MPI_COMM_WORLD, i_error)
               STOP
            ENDIF

         ENDIF

         !WRITE(6,*) ion, subspace%L(ion)%ion, subspace%L(ion)%n_orbitals

         !broadcast
         srcnt = 1
         CALL MPI_BCAST(subspace%L(ion)%ion,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
         CALL check_mpi_success
         CALL MPI_BCAST(subspace%L(ion)%n_orbitals,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
         CALL check_mpi_success

         !allocate memory for correlated orbitals
         ALLOCATE( subspace%L(ion)%orbital(subspace%L(ion)%n_orbitals), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "L(ion)%orbital" could not be allocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         IF ( rank_0 ) THEN

            READ(line_readin((idx_seperator+1):255),*) subspace%L(ion)%orbital

            IF ( (ANY(subspace%L(ion)%orbital<1) .OR. ANY(subspace%L(ion)%orbital>16)) ) THEN
               WRITE(6,*) 'ERROR: "L(orbital)" has to be a non-empty list whith elements in [1 16].'
               WRITE(6,*) 'ERROR: ', line_readin
               CALL MPI_Abort(MPI_COMM_WORLD, i_error)
               STOP
            ENDIF

            !WRITE(6,*) subspace%L(ion)%orbital

         ENDIF

         !broadcast
         srcnt = subspace%L(ion)%n_orbitals
         CALL MPI_BCAST(subspace%L(ion)%orbital,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
         CALL check_mpi_success

      ENDDO

    CALL hk_triqs_reader(subspace,26)

    CLOSE(26)

      !no broadcast needed
      subspace%n_orbitals = SUM(subspace%L(:)%n_orbitals)


      IF ( rank_0 ) THEN
         WRITE(6,*) '   SUBSPACE set from FILE'
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE file_subspace_procar_SET_FROM_FILE

END MODULE type_file_subspace_procar
