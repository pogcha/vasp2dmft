MODULE type_file_subspace_locproj
!
!  Purpose:
!    Import external input
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    01/12/15     S. Barthel	Original code
!

   USE parallel
   USE fileio

   IMPLICIT NONE

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units

!===================================================================================================================================

   TYPE :: file_subspace_locproj !data container for SUBSPACE

      !PRIVATE                                                   !no access granted to member variables

      CHARACTER(16) :: filename = 'SUBSPACE'                    !default filename
      CHARACTER(7) :: hk_fmt
      LOGICAL :: is_whitelist = .TRUE.                          !flag to white-/blacklist band selection
      LOGICAL :: is_T0 = .TRUE.                                 !flag to do T=0 calculation or do finite T calculation


      INTEGER :: n_bands                                        !number of selected bands in band-subspace
      INTEGER, ALLOCATABLE, DIMENSION(:) :: band_indices        !list of selected band indices in band-subspace

      INTEGER :: n_orbitals
      INTEGER, ALLOCATABLE, DIMENSION(:) :: orbital_indices		!list of orbital indices (VASP labeling)

      INTEGER :: n_shells                                       ! number of atomic shells
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: shell_info		! information about shells (which atom, type, L, dimension)

      INTEGER :: n_corr_shells                                  ! number of correlated atomic shells
      INTEGER, ALLOCATABLE, DIMENSION(:,:) :: corr_shells_info	! information about shells (which atom, type, L, dimension, SO, interactions)

      INTEGER :: n_irreps
      INTEGER, ALLOCATABLE, DIMENSION(:) :: irrep_info		    ! information about irreps (number of irreducible representations, and the dimensions of the irreps)

   END TYPE file_subspace_locproj

!===================================================================================================================================

CONTAINS

   SUBROUTINE file_subspace_locproj_SET_FILENAME( subspace, filename )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    30/10/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_subspace_locproj ), INTENT(INOUT) :: subspace
      CHARACTER(16), INTENT(IN) :: filename

      !-------------------------------------------------------------------------------------------------------------------------------

      subspace%filename = TRIM(ADJUSTL(filename))

   END SUBROUTINE file_subspace_locproj_SET_FILENAME

   !=================================================================================================================================

   SUBROUTINE file_subspace_locproj_INIT( subspace, locproj )
      !
      !  Purpose:
      !    Read a SUBSPACE file
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    01/12/15     S. Barthel	Original code
      !

      USE type_file_locproj  !this forces the data structure to be non-private

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_subspace_locproj ), INTENT(INOUT) :: subspace
      TYPE( file_locproj ), INTENT(IN) :: locproj

      INTEGER :: ioerror          !error code for file IO

      !-------------------------------------------------------------------------------------------------------------------------------

      IF ( rank_0 ) THEN
         OPEN (UNIT=26,FILE=subspace%filename, STATUS='OLD', ACTION='READ', IOSTAT=ioerror)
         CLOSE(26)
      ENDIF

      srcnt = 1
      CALL MPI_BCAST(ioerror,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      IF ( ioerror /= 0 ) THEN

         IF (rank_0) THEN
            WRITE(6,*) '   rank 0: Creating SUBSPACE ...'
         ENDIF

         CALL file_subspace_locproj_SET( subspace, locproj )

         if ( rank_0 ) then
            CALL file_subspace_locproj_WRITER( subspace )
         end if
         CALL MPI_BARRIER(MPI_COMM_WORLD, i_error)

         WRITE(6,*) '   rank 0: Generated default SUBSPACE. Check and rerun! Exiting...'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP

      ELSE

         IF (rank_0) THEN
            WRITE(6,*) '   rank 0: Reading SUBSPACE ...'
         ENDIF

         CALL file_subspace_locproj_READER( subspace, locproj )
      ENDIF

   END SUBROUTINE file_subspace_locproj_INIT

   !=================================================================================================================================


 FUNCTION l_from_orb_type(orbital_type)
        implicit NONE
        INTEGER :: orbital_type
        INTEGER :: l_from_orb_type
        IF (orbital_type == 1) then
            l_from_orb_type = 0
        elseif ( ( orbital_type > 1 ) .AND. (orbital_type < 5) ) then
            l_from_orb_type = 1
        elseif ( ( orbital_type > 4 ) .AND. (orbital_type < 10) ) then
            l_from_orb_type = 2
        elseif ( orbital_type > 9 ) then
            l_from_orb_type = 3
        END IF

        RETURN
    end function




    SUBROUTINE guess_triqs_header(subspace, locproj)
      
        USE type_file_locproj

        TYPE( file_subspace_locproj ), INTENT(INOUT) :: subspace
        TYPE( file_locproj ), INTENT(IN) :: locproj

        INTEGER :: band, orbital, shell, corr_shell       !loop indices
        INTEGER, DIMENSION(2) :: current, new

        INTEGER :: allocerror          !error code for allocation

        ! go through all orbitals and check to how many shells they belong
        ! starting with first orb, save atom and l
        current(1) = locproj%ion_type(1)
        current(2) = l_from_orb_type(locproj%orbital_type(1))
        subspace%n_shells = 1
        subspace%n_corr_shells = 1

        DO orbital = 1, locproj%n_orbitals
            ! check if the next orbital is the atom and has the same l
            ! if it does not, we have a new shell
            new(1) = locproj%ion_type(orbital)
            new(2) = l_from_orb_type(locproj%orbital_type(orbital))

            IF ( ( current(1) == new(1) ) .AND. (current(2) == new(2) ) ) then
            else
                subspace%n_shells = subspace%n_shells + 1
                IF ( new(2) > 1 ) then
                    subspace%n_corr_shells = subspace%n_corr_shells + 1
                END IF

                current = new

            END IF


        ENDDO
        ! if we have spare bands. we dump then into a dummy shell. Thus we add one to the number of shells

        IF (locproj%n_orbitals < locproj%n_bands) THEN
            subspace%n_shells = subspace%n_shells + 1
        ENDIF

        ALLOCATE ( subspace%shell_info( subspace%n_shells, 4 ), STAT=allocerror )
        IF ( allocerror > 0 ) THEN
             WRITE(6,*) 'ERROR: Memory for "shell_info" could not be allocated'
             CALL MPI_Abort(MPI_COMM_WORLD, i_error)
             STOP
        ENDIF

        ALLOCATE ( subspace%corr_shells_info( subspace%n_corr_shells, 6 ), STAT=allocerror )
        IF ( allocerror > 0 ) THEN
             WRITE(6,*) 'ERROR: Memory for "n_corr_shells" could not be allocated'
             CALL MPI_Abort(MPI_COMM_WORLD, i_error)
             STOP
        ENDIF
        subspace%corr_shells_info(:,:) = 0

        ! assume all shells with d or f electrons are correlated
        ! go through all shells and assign atom type, type, l and dim
        ! starting with first orb, save atom and l
        ! assume all shells with d electrons are correlated
        subspace%shell_info(:,:) = 0
        shell = 1
        subspace%shell_info(shell,1) = locproj%ion_type(1)
        subspace%shell_info(shell,2) = 1
        subspace%shell_info(shell,3) = l_from_orb_type(locproj%orbital_type(1))
        subspace%shell_info(shell,4) = 0
        corr_shell = 1
        IF ( subspace%shell_info(shell,3) > 1 ) then
            subspace%corr_shells_info(corr_shell,:4) = subspace%shell_info(1,:)
        END IF

        current(1) = locproj%ion_type(1)
        current(2) = l_from_orb_type(locproj%orbital_type(1))
        DO orbital = 1, locproj%n_orbitals
            ! check if the next orbital is the atom and has the same l
            ! if it does not, we have a new shell
            new(1) = locproj%ion_type(orbital)
            new(2) = l_from_orb_type(locproj%orbital_type(orbital))

            IF ( ( current(1) == new(1) ) .AND. (current(2) == new(2) ) ) then
            else
                shell = shell + 1
                subspace%shell_info(shell,1) = locproj%ion_type(orbital)
                subspace%shell_info(shell,2) = shell
                subspace%shell_info(shell,3) = l_from_orb_type(locproj%orbital_type(orbital))
                subspace%shell_info(shell-1,4) = ( ( orbital - 1 ) - SUM(subspace%shell_info(:,4)) )
                IF ( subspace%shell_info(shell-1,3) > 1) then
                    corr_shell = corr_shell + 1
                    subspace%corr_shells_info(corr_shell-1,:4) = subspace%shell_info(shell-1,:)
                END IF
                current = new
            END IF

        ENDDO
        subspace%shell_info(shell,4) = locproj%n_orbitals -  SUM(subspace%shell_info(:,4))
        IF ( subspace%shell_info(shell,3) > 1) then
                    corr_shell = corr_shell + 1
                    subspace%corr_shells_info(corr_shell-1,:4) = subspace%shell_info(shell,:)
                END IF
        IF (locproj%n_orbitals < locproj%n_bands) THEN
            subspace%shell_info(subspace%n_shells,1) = subspace%shell_info(subspace%n_shells-1,1)+1
            subspace%shell_info(subspace%n_shells,2) = subspace%shell_info(subspace%n_shells-1,2)+1
            subspace%shell_info(subspace%n_shells,3) = -1
            subspace%shell_info(subspace%n_shells,4) = locproj%n_bands- locproj%n_orbitals
        END IF

        subspace%n_irreps = 1

        ALLOCATE ( subspace%irrep_info( subspace%n_irreps ), STAT=allocerror )
        IF ( allocerror > 0 ) THEN
             WRITE(6,*) 'ERROR: Memory for "n_corr_shells" could not be allocated'
             CALL MPI_Abort(MPI_COMM_WORLD, i_error)
             STOP
        ENDIF
        subspace%irrep_info(1) = SUM( subspace%corr_shells_info(:,4) )

    END SUBROUTINE guess_triqs_header


   SUBROUTINE file_subspace_locproj_SET( subspace, locproj )
      !
      !  Purpose:
      !    init subspace from locproj
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    01/12/15     S. Barthel	Original code
      !

      USE type_file_locproj  !this forces the data structure to be non-private

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_subspace_locproj ), INTENT(INOUT) :: subspace
      TYPE( file_locproj ), INTENT(IN) :: locproj

      INTEGER :: allocerror          !error code for allocation
      INTEGER :: band, orbital, shell, corr_shell       !loop indices
      INTEGER, DIMENSION(2) :: current, new
      !-------------------------------------------------------------------------------------------------------------------------------

      !init number of selected bands in band-subspace from PROCAR
      subspace%n_bands = locproj%n_bands

      !deallocate possibly
      IF ( ALLOCATED(subspace%band_indices) ) THEN
         DEALLOCATE(subspace%band_indices, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "band_indices" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !allocate
      ALLOCATE ( subspace%band_indices( subspace%n_bands ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "band_indices" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         !init list of selected band indices in band-subspace from LOCPROJ
         subspace%band_indices = (/(band, band=1,subspace%n_bands)/)
      ENDIF

      !-------------------------------------------------------------------------------------------------------------------------------

      !init number of orbitals from LOCPROJ
      subspace%n_orbitals = locproj%n_orbitals

       ! init h(k) format to triqs
      subspace%hk_fmt = 'triqs'


      CALL guess_triqs_header(subspace, locproj)




      !deallocate possibly
      IF ( ALLOCATED(subspace%orbital_indices) ) THEN
         DEALLOCATE(subspace%orbital_indices, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "orbital_indices" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !allocate
      ALLOCATE ( subspace%orbital_indices( subspace%n_orbitals ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "band_indices" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         !init list of orbital indices from LOCPROJ
         subspace%orbital_indices = (/(orbital, orbital=1,subspace%n_orbitals)/)
         !(1=s, 2=py, 3=pz, 4=px, 5=dxy, 6=dyz, 7=dz², 8=dxz, 9=dx², 10=f-3, 11=f-2, 12=f-1, 13=f0, 14=f1, 15=f2, 16=f3)
      ENDIF


      IF ( rank_0 ) THEN
         WRITE(6,*) '   SUBSPACE set from LOCPROJ'
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE file_subspace_locproj_SET

   !=================================================================================================================================





   SUBROUTINE file_subspace_locproj_WRITER( subspace )
      !
      !  Purpose:
      !    Write a SUBSPACE file
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    29/10/15     S. Barthel	Original code
      !

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_subspace_locproj ), INTENT(IN) :: subspace

      INTEGER :: ioerror			!error code for file IO
      INTEGER :: band, orbital, shell		!loop indices

      !-------------------------------------------------------------------------------------------------------------------------------

      !===========================================================================================================================
      !Format descriptors
      !===========================================================================================================================

      !Format descriptors for the correlated subspace file
2000  FORMAT(1X,'#flag to white-/blacklist band selection'/,1X,'is_whitelist=',1X,L1,/)

2001  FORMAT(1X,'#list of selected band indices'/,1X,'bands_list=')

2002  FORMAT(1X,I3)

2003  FORMAT(/)


2004  FORMAT(1X,'#list of orbital characters'/,1X,'orbitals_list=')

2005  FORMAT(1X,'#flag to do T=0 calculations (only relevant for charge self consistency)'/,1X,'is_T0=',1X,L1,/)

2006  FORMAT(1X,'#flag to choose H(k) file format (triqs / wanni)'/,1X,'Hk_fmt=',1X,A,/)

2007  FORMAT(1X,'#H(k) header for triqs'/,1X,'n_shell=',1X,I3)

2009  FORMAT(1X,'shell_type (',I3,'  )=',1X)

2010  FORMAT(1X,'n_corr_shell=',1X,I3)

2011  FORMAT(1X,'irreps=')
      IF ( rank_0 ) THEN

         WRITE(6,*) '   rank 0: WRITING SUBSPACE ...'

         !check if SUBSPACE exists
         OPEN (UNIT=26,FILE=subspace%filename,STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
         IF ( ioerror /= 0 ) THEN
            WRITE(6,*) 'ERROR: cannot write SUBSPACE'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         WRITE(26,2000) subspace%is_whitelist

         WRITE(26,2005) subspace%is_T0

         WRITE(26,2006) TRIM(ADJUSTL(subspace%hk_fmt))

         !band-indices
         WRITE(26,2001,ADVANCE='NO')
         DO band=1,subspace%n_bands
            WRITE(26,2002,ADVANCE='NO') subspace%band_indices(band)
         ENDDO
         WRITE(26,2003)

         !orbital-indices
         WRITE(26,2004,ADVANCE='NO')
         DO orbital=1,subspace%n_orbitals
            WRITE(26,2002,ADVANCE='NO') subspace%orbital_indices(orbital)
         ENDDO
         WRITE(26,2003)





         WRITE(26,2007) subspace%n_shells

         DO shell = 1, subspace%n_shells
            WRITE(26,2009,ADVANCE='NO') shell
            DO band = 1, 4
                WRITE(26,2002,ADVANCE='NO') subspace%shell_info(shell,band)
            END DO
            WRITE(26,2003,ADVANCE='NO')
         END DO

         WRITE(26,2010) subspace%n_corr_shells

         DO shell = 1, subspace%n_corr_shells
            WRITE(26,2009,ADVANCE='NO') shell

            DO band = 1, 6
                WRITE(26,2002,ADVANCE='NO') subspace%corr_shells_info(shell,band)
            END DO
            WRITE(26,2003,ADVANCE='NO')
         END DO

         WRITE(26,2011,ADVANCE='NO')
         WRITE(26,2002,ADVANCE='NO') subspace%n_irreps
         DO band = 1, subspace%n_irreps
            WRITE(26,2002,ADVANCE='NO') subspace%irrep_info(band)
         END DO
         WRITE(26,2003,ADVANCE='NO')

         CLOSE(26)

         WRITE(6,*) '-'

      ENDIF

   END SUBROUTINE file_subspace_locproj_WRITER

   !=================================================================================================================================


   SUBROUTINE file_subspace_locproj_READER( subspace, locproj )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    29/10/15     S. Barthel	Original code
      !

      USE type_file_locproj  !this forces the data structure to be non-private

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_subspace_locproj ), INTENT(INOUT) :: subspace
      TYPE( file_locproj ), INTENT(IN) :: locproj


      INTEGER :: ioerror, allocerror          !error codes for allocation and file IO
      INTEGER :: ion, shell                    !loop indices

      !===============================================================================================================================
      !Format descriptors
      !===============================================================================================================================

!     !Format descriptors for the correlated subspace file
!     2000 FORMAT(1X,'#flag to white-/blacklist band selection'/,1X,'is_whitelist=',1X,L1,/)
!     !2010 FORMAT(1X,'#flag to white-/blacklist band selection'/,1X,'is_whitelist=',A,/)
!     2010 FORMAT(1X,40X/,1X,13X,A,/)
!
!     2001 FORMAT(1X,'#list of selected band indices'/,1X,'bands_list=')
!     !2011 FORMAT(1X,'#list of selected band indices'/,1X,'bands_list=',A,/)
!     2011 FORMAT(1X,30X/,1X,11X,A,/)
!
!     2002 FORMAT(1X,I3)
!     2003 FORMAT(/)
!
!     2004 FORMAT(1X,'#correlated subspace µ(ion) : L(orbital), (1=s, 2=py, 3=pz, 4=px, 5=dxy, 6=dyz, 7=dz², 8=dxz, 9=dx², &
!       10=f-3, 11=f-2, 12=f-1, 13=f0, 14=f1, 15=f2, 16=f3)')
!     2005 FORMAT(1X,I3,1X,':')
!     2015 FORMAT(1X,A)


      !Format descriptors for the subspace file
2000  FORMAT(1X,'#flag to white-/blacklist band selection'/,1X,'is_whitelist=',1X,L1,/)
2010  FORMAT(1X,40X/,1X,13X,A,/)

2001  FORMAT(1X,'#list of selected band indices'/,1X,'bands_list=')
2011  FORMAT(1X,30X/,1X,11X,A,/)

2002  FORMAT(1X,I3)
2003  FORMAT(/)

2004  FORMAT(1X,'#list of orbital characters'/,1X,'orbitals_list=')
2014  FORMAT(1X,27X/,1X,14X,A,/)

2005  FORMAT(1X,'#flag to do T=0 calculations (only relevant for charge self consistency)'/,1X,'is_T0=',1X,L1,/)
2015  FORMAT(1X,72X/,1X,6X,A,/)

2006  FORMAT(1X,'#flag to choose H(k) file format (triqs / wanni)'/,1X,'Hk_fmt=',1X,A5,/)
2016  FORMAT(1X,38X/,1X,7X,A,/)
      
2007  FORMAT(1X,'#H(k) header for triqs'/,1X,'n_shell=',1X,I3)

2017  FORMAT(1X,9X,I3)

2019  FORMAT(1X,12X,3X,4X,A)

2020  FORMAT(1X,13X,1X,I3)

2021  FORMAT(1X,8X,I3,A)



      IF ( rank_0 ) THEN

         OPEN (UNIT=26,FILE=subspace%filename, STATUS='OLD', ACTION='READ', IOSTAT=ioerror)

         IF ( ioerror /= 0 ) THEN
            WRITE(6,*) 'ERROR: cannot read SUBSPACE'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         READ(26,2010) line_readin !read a whole line
         READ(line_readin,*,IOSTAT=ioerror) subspace%is_whitelist  !attempt type conversion of first entry, the rest is ignored
         IF (ioerror /= 0) THEN                                    !check for correctness, else print error
            WRITE(6,*) 'ERROR: "is_whitelist=" has to be of type logical.'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         READ(26,2015) line_readin !read a whole line
         READ(line_readin,*,IOSTAT=ioerror) subspace%is_T0  !attempt type conversion of first entry, the rest is ignored

         IF (ioerror /= 0) THEN                                    !check for correctness, else print error
            WRITE(6,*) 'ERROR: "is_T0=" has to be of type logical.'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
         READ(26,2016) line_readin !read a whole line
         READ(line_readin,*,IOSTAT=ioerror) subspace%hk_fmt  !attempt type conversion of first entry, the rest is ignored
         subspace%hk_fmt  = TRIM(ADJUSTL(subspace%hk_fmt ))
         IF (ioerror /= 0) THEN                                    !check for correctness, else print error
            WRITE(6,*) 'ERROR: "Hk=" has to be of type character.'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
         IF (subspace%hk_fmt == 'triqs') THEN
            WRITE(6,*) '   H(k) output file format is triqs'
         ELSEIF (subspace%hk_fmt == 'wannier') THEN
            WRITE(6,*) '   H(k) output file format is wannier'
        ELSE
            WRITE(6,*) 'ERROR: " Hk_fmt=" has to be triqs or wannier.'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
        ENDIF
         !IF (subspace%is_T0) then
         !   WRITE(6,*) '   Doing T=0 calculation.'
         !else
         !   WRITE(6,*) '   Doing finite T calculation.'
         !END IF


      ENDIF


      !broadcast header
      srcnt = 1
      CALL MPI_BCAST(subspace%is_whitelist,  srcnt, MPI_LOGICAL, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      CALL MPI_BCAST(subspace%is_T0,  srcnt, MPI_LOGICAL, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      srcnt = LEN(subspace%hk_fmt)+1
      CALL MPI_BCAST(subspace%hk_fmt,  srcnt, MPI_LOGICAL, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      !---

      IF ( rank_0 ) THEN
         READ(26,2011) line_readin !read a whole line
         subspace%n_bands = numel_string(line_readin, locproj%n_bands) !maximum of n_bands
         !WRITE(6,*) subspace%n_bands
      ENDIF
      !broadcast header
      srcnt = 1
      CALL MPI_BCAST(subspace%n_bands,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success
      !---

      IF ( (0 < subspace%n_bands) .AND. (subspace%n_bands <= locproj%n_bands) ) THEN
         !allocate
         ALLOCATE ( subspace%band_indices( subspace%n_bands ), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "band_indices" could not be allocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ELSE
         WRITE(6,*) 'ERROR: "0 < band_indices <= bands" not fulfilled.'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ENDIF
      IF ( rank_0 ) THEN
         READ(line_readin,*) subspace%band_indices
         IF ( (ANY(subspace%band_indices<1) .OR. ANY(subspace%band_indices>locproj%n_bands)) ) THEN
            WRITE(6,*) '  ERROR: "band_indices=" has to be a non-empty list whith &
            &elements in [1 bands].'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !broadcast
      srcnt = subspace%n_bands
      CALL MPI_BCAST(subspace%band_indices,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      !---

      IF ( rank_0 ) THEN
         READ(26,2014) line_readin !read a whole line
         subspace%n_orbitals = numel_string(line_readin, locproj%n_orbitals) !maximum of n_orbitals
!       WRITE(6,*) subspace%n_orbitals
!       WRITE(6,*) line_readin
      ENDIF

      !broadcast header
      srcnt = 1
      CALL MPI_BCAST(subspace%n_orbitals,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      IF ( (subspace%n_orbitals == locproj%n_orbitals) ) THEN
         !allocate
         ALLOCATE ( subspace%orbital_indices( subspace%n_orbitals ), STAT=allocerror )
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "orbital_indices" could not be allocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ELSE
         WRITE(6,*) 'ERROR: "orbital_indices == n_orbitals" not fulfilled.'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ENDIF

      IF ( rank_0 ) THEN
         READ(line_readin,*) subspace%orbital_indices
         IF ( (ANY(subspace%orbital_indices<1) .OR. ANY(subspace%orbital_indices>locproj%n_orbitals)) ) THEN
            WRITE(6,*) '  ERROR: "band_indices=" has to be a non-empty list whith elements in [1 orbitals].'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !broadcast
      srcnt = subspace%n_orbitals
      CALL MPI_BCAST(subspace%orbital_indices,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      IF ( subspace%hk_fmt == 'triqs' ) THEN
    IF ( rank_0 ) then
        READ(26,*)
        READ(26,2017) subspace%n_shells
    END IF
    

    srcnt = 1
    CALL MPI_BCAST(subspace%n_shells,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
    CALL check_mpi_success


    ALLOCATE ( subspace%shell_info( subspace%n_shells, 4 ), STAT=allocerror )
     IF ( allocerror > 0 ) THEN
        WRITE(6,*) 'ERROR: Memory for "shell_info" could not be allocated'
        CALL MPI_Abort(MPI_COMM_WORLD, i_error)
        STOP
     ENDIF

     subspace%shell_info = 0


    IF ( rank_0 ) then
        DO shell = 1, subspace%n_shells
            READ(26,2019) line_readin
            READ(line_readin,*) subspace%shell_info(shell,:)
        ENDDO
        READ(26,2020) subspace%n_corr_shells
    END IF

    srcnt = 1

    CALL MPI_BCAST(subspace%n_corr_shells,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)

    ALLOCATE ( subspace%corr_shells_info( subspace%n_corr_shells, 6 ), STAT=allocerror )
     IF ( allocerror > 0 ) THEN
        WRITE(6,*) 'ERROR: Memory for "shell_info" could not be allocated'
        CALL MPI_Abort(MPI_COMM_WORLD, i_error)
        STOP
     ENDIF
    subspace%corr_shells_info = 0
    IF ( rank_0 ) then
        DO shell = 1, subspace%n_corr_shells
            READ(26,2019) line_readin
            READ(line_readin,*) subspace%corr_shells_info(shell,:)
        ENDDO
    END IF

    ! do some sanity checks on the triqs info. only do so if the triqs output is requested

    IF (rank_0 ) then
    IF ( subspace%hk_fmt == 'triqs' ) THEN
        IF ( SUM(subspace%shell_info(:,4)) /= subspace%n_bands ) then
            WRITE(6,*) 'ERROR: total number of shell dimensions has to be equal to number of bands!'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
        ENDIF
        IF ( subspace%n_bands > subspace%n_orbitals ) then
            IF ( SUM(subspace%corr_shells_info(:,4)) > SUM(subspace%shell_info(:subspace%n_shells-1,4)) ) then
                WRITE(6,*) 'ERROR: total number of corr shell dimensions can not be larger '
                WRITE(6,*) 'than total number of orbitals defined in LOCPROJ!'
                CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                STOP
            ENDIF
        else
            IF ( SUM(subspace%corr_shells_info(:,4)) > SUM(subspace%shell_info(:,4)) ) then
                WRITE(6,*) 'ERROR: total number of corr shell dimensions can not be larger '
                WRITE(6,*) 'than total number of orbitals defined in LOCPROJ!'
                CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                STOP
            ENDIF
        END IF


     ENDIF
  ENDIF

    IF ( rank_0 ) then
        READ(26,2021) subspace%n_irreps, line_readin
    END IF
    srcnt = 1
    CALL MPI_BCAST(subspace%n_irreps,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
    CALL check_mpi_success

    ALLOCATE ( subspace%irrep_info( subspace%n_irreps ), STAT=allocerror )
     IF ( allocerror > 0 ) THEN
        WRITE(6,*) 'ERROR: Memory for "n_irreps" could not be allocated'
        CALL MPI_Abort(MPI_COMM_WORLD, i_error)
        STOP
     ENDIF

    IF ( rank_0 ) then
        READ(line_readin,*) subspace%irrep_info(:)
    END IF

    CALL check_mpi_success

    srcnt = subspace%n_shells*4

    CALL MPI_BCAST(subspace%shell_info,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)

    CALL check_mpi_success

    srcnt = subspace%n_corr_shells*6

    CALL MPI_BCAST(subspace%corr_shells_info,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
    CALL check_mpi_success

    srcnt = subspace%n_irreps
    CALL MPI_BCAST(subspace%irrep_info,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
    CALL check_mpi_success
    !CALL EXIT()
    ENDIF

    CLOSE(26)

      IF ( rank_0 ) THEN
         WRITE(6,*) '   SUBSPACE set from FILE'
         WRITE(6,*) '-'
      ENDIF

   END SUBROUTINE file_subspace_locproj_READER

   !=================================================================================================================================

   SUBROUTINE file_subspace_locproj_REINIT_BANDS( subspace_01, subspace_02, n_bands, idx_min, idx_max )
      !
      !  Purpose:
      !    ...
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    20/11/15     S. Barthel	Original code
      !

      USE type_file_locproj  !this forces the data structure to be non-private

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_subspace_locproj ), INTENT(IN) :: subspace_01
      TYPE( file_subspace_locproj ), INTENT(INOUT) :: subspace_02

      INTEGER, INTENT(IN) :: n_bands, idx_min, idx_max

      INTEGER :: ioerror, allocerror	!error codes for allocation and file IO
      INTEGER :: band, ion                !loop indices

      !-------------------------------------------------------------------------------------------------------------------------------

      !simple copy to start with

      subspace_02%filename     = subspace_01%filename

      !-------------------------------------------------------------------------------------------------------------------------------
      !band-stuff, reinit
      !-------------------------------------------------------------------------------------------------------------------------------
      subspace_02%is_whitelist = subspace_01%is_whitelist
      subspace_02%is_T0 = subspace_01%is_T0

      !reinit band-indices
      subspace_02%n_bands = n_bands

      !deallocate possibly
      IF ( ALLOCATED(subspace_02%band_indices) ) THEN
         DEALLOCATE(subspace_02%band_indices, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "subspace_02%band_indices" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !allocate
      ALLOCATE ( subspace_02%band_indices( subspace_02%n_bands ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "band_indices" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ENDIF

      !reinit
      subspace_02%band_indices = subspace_01%band_indices(idx_min:idx_max)	!this way we keep removed bands, i.e. [1 2 6 7]

      !-------------------------------------------------------------------------------------------------------------------------------
      !orbital-stuff, copied
      !-------------------------------------------------------------------------------------------------------------------------------
      subspace_02%n_orbitals = subspace_01%n_orbitals

      !deallocate possibly
      IF ( ALLOCATED(subspace_02%orbital_indices) ) THEN
         DEALLOCATE(subspace_02%orbital_indices, STAT=allocerror)
         IF ( allocerror > 0 ) THEN
            WRITE(6,*) 'ERROR: Memory for "subspace_02%orbital_indices" could not be deallocated'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF
      ENDIF

      !allocate
      ALLOCATE ( subspace_02%orbital_indices( subspace_02%n_orbitals ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "orbital_indices" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ENDIF

      !copied
      subspace_02%orbital_indices = subspace_01%orbital_indices

   END SUBROUTINE file_subspace_locproj_REINIT_BANDS

END MODULE type_file_subspace_locproj
