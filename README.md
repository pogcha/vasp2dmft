This is an interface between VASP and some impurity solver to do lda++

It uses the PAW projectors and builds an orthonormal complete basis in order to get a H(k) in localized basis.

It can also read a self-energy and calculate charge updates which can be read in by VASP (charge self-consistency)



# Documentation:

## Usage
On execution you can choose to do calculations using information from the `LOCPROJ`(default) or `PROCAR` file and to do charge self consistent calculations or non charge self consistent calculations (default). Toggle this by inline arguments.

The arguments have to be `T` or `F`. The first argument toggles the charge selfconsistency and the second one which file to use.

`vasp2dmft T T` performs charge selfconsistent calculations with `PROCAR`. NOT IMPLEMENTED!  
`vasp2dmft T F` performs charge selfconsistent calculations with `LOCPROJ`. Same as `vasp2dmft T`.   
`vasp2dmft F T` performs non charge selfconsistent calculations with `PROCAR`.  
`vasp2dmft F F` performs non charge selfconsistent calculations with `LOCPROJ`. Same as `vasp2dmft` and `vasp2dmft F`.  

MPI should work by executing `mpirun -np N vasp2dmft`, where N is the number of cores.

### Necessary input files

For non charge self consistent calculations using LOCPROJ you need
 - `LOCPROJ` (VASP file)
 - `POSCAR` (VASP file)
 - `OUTCAR` (VASP file)
 - `COORDS` (auto-generated with default values if not existing)
 - `SUBSPACE` (auto-generated with default values if not existing)

For non charge self consistent calculations using PROCAR you need
 - `PROCAR` (VASP file)
 - `POSCAR` (VASP file)
 - `COORDS` (auto-generated with default values if not existing)
 - `SUBSPACE` (auto-generated with default values if not existing)

For charge self consistent calculations you additionally need
 - `SIGMA.DAT`

## Format of non-vasp input files

### SUBSPACE

This file defines the correlated subspace in terms of KS bands and which orbitals to use as localized basis and toggles if we do T=0 or not. The Comments have to be written exactly like here!

Format of `SUBSPACE` when using `LOCPROJ`.
```
 #flag to white-/blacklist band selection
 is_whitelist= toggle_if_whiteList

 #flag to do T=0 calculations (only relevant for charge self consistency)
 is_T0= toggle_if_T=0

 #list of selected band indices
 bands_list=   List of bands for correlated subspace (one line, seperated by space or tab. Count from 1)

 #list of orbital characters
 orbitals_list=   List of orbitals as ordered in LOCPROJ, which are used as localized basis. (one line, seperated by spaces or tabs, Count from 1)
```

Format of `SUBSPACE` when using `PROCAR`. 
```
 #flag to white-/blacklist band selection
 is_whitelist= toggle_if_whiteList

 #flag to do T=0 calculations (only relevant for charge self consistency)
 is_T0= toggle_if_T=0

 #list of selected band indices
 bands_list=   List of bands for correlated subspace (one line, seperated by space or tab. Count from 1)

 #correlated subspace µ(ion) : L(orbital), (1=s, 2=py, 3=pz, 4=px, 5=dxy, 6=dyz, 7=dz², 8=dxz, 9=dx², 10=f-3, 11=f-2, 12=f-1, 13=f0, 14=f1, 15=f2, 16=f3)
   atom_1 :   orb_1^atom1 orb_2^atom1 orb_3^atom1 ...
   atom_2 :   orb_1^atom2 orb_2^atom2 orb_3^atom2 ...
...
```

### SIGMA.DAT

Format of `SIGMA.DAT`. This is the self energy $`\Sigma_{ij\sigma}(\omega_n)`$ which is read in. Everything is free format.

```
beta
number_of_matsubara_frequencies
number_of_spins
number_of_k-points
number_of_orbitals_for_which_sigma_unequals_zero
total_number_of_orbitals
total_electron_number
chemical_potential
Index_of_Matsubara_freq_from_where_on_the_high_freq._tail_is_fitted
orbital_0 orbital_0 spin_0, matsubara_index_0
sigma_real^0000 sigma_imag^0000
orbital_1 orbital_0 spin_0, matsubara_index_0
sigma_real^1000 sigma_imag^1000
orbital_2 orbital_0 spin_0, matsubara_index_0
sigma_real^2000 sigma_imag^2000
...
orbital_0 orbital_1 spin_0, matsubara_index_0
sigma_real^0100 sigma_imag^0100
orbital_1 orbital_1 spin_0, matsubara_index_0
sigma_real^1100 sigma_imag^1100
...
( and so on. First index is fastest (innermost loop) and so on)
```

## Format of output files

### Hk.dat

Format of file `H_fBZ.proj` to which Hamiltonian $`H_{ij}^{k_x^n,k_y^n,k_z^n}`$ in localized basis is written.
```
number_of_spins number_of_kpoints number_of_orbitals number_of_electrons # of spins, # of k-points, # of orbitals, # of electrons. Generated from LOCPROJ/PROCAR.
k_x^0 k_y^0 k_z^0
Re(H_00^0) Im(H_00^0) Re(H_01^0) Im(H_01^0) ...
Re(H_10^0) Im(H_10^0) Re(H_11^0) Im(H_11^0) ...
...
k_x^1 k_y^1 k_z^1
Re(H_00^1) Im(H_00^1) Re(H_01^1) Im(H_01^1) ...
Re(H_10^1) Im(H_10^1) Re(H_11^1) Im(H_11^1) ...
...
```

### GAMMA

Format of file `GAMMA` which contains the density matrix correction $`\Gamma_{ij\sigma}(k)`$ for band indices $`i`$ and $`j`$, k-point $`k`$ and spin $`\sigma`$
```
n_kpoints n_bands ! number of k-points and bands
k_index_1 band_index_start band_index_end ! k-point index, start and end band
Re(Gamma_{1111}) Im(Gamma_{1111})
Re(Gamma_{1211}) Im(Gamma_{1211})
...
Re(Gamma_{2111}) Im(Gamma_{2111})
Re(Gamma_{2211}) Im(Gamma_{2211})
...
k_index_2 band_index_start band_index_end ! k-point index, start and end band
Re(Gamma_{1112}) Im(Gamma_{1112})
Re(Gamma_{1212}) Im(Gamma_{1212})
...
Re(Gamma_{2112}) Im(Gamma_{2112})
Re(Gamma_{2212}) Im(Gamma_{2212})
...
followed by the second spin, if you are doing spin polarized calculations
```
