CC = gfortran
#CFLAGS = -O2 -m64 -march=core-avx-i -fp-model strict -integer-size 64 -real-size 64 -assume byterecl
CFLAGS = -m64 -fdefault-integer-8 -fdefault-real-8 -fopenmp -mtune=core2 -march=core2 -mfpmath=sse -funroll-loops -ftree-vectorizer-verbose=1

LDFLAGS = -lgomp -lm -llapack -lblas

SOURCES = constants.f90 timings.f90 shared_types.f90 shared_data.f90 fileio.f90 math.f90 coulomb.f90 cfbasis.f90 selfconsistency.f90 import.f90 export.f90 projectors.f90 complement.f90 main.f90

OBJECTS = $(SOURCES:.f90=.o)

EXECUTABLE = vasp2w2dyn_178.gnu

INSTALLPATH = ~/bin

all: build clean install

build: $(EXECUTABLE)

clean:
	rm -f *.o
	rm -f *.mod
	rm -f *~
	
install: 
	cp $(EXECUTABLE) $(INSTALLPATH)
	
.SUFFIXES: .o .f90
	
.f90.o:
	$(CC) $(CFLAGS) -c -o $@ $<
	
$(EXECUTABLE): $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS)
	
.PHONY: clean

.PHONY: install