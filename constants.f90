MODULE constants
!
!  Purpose:
!    Declare parameters
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    11/11/13     S. Barthel	Original code
!

   IMPLICIT NONE

! Data dictionary: Declare constants in natural units (hbar=c=1, 4*pi*epsilon_0=1)

   COMPLEX, PARAMETER :: IM = (0.0D0,1.0D0) 	!Imaginary unit
   REAL, PARAMETER :: PI = 4*ATAN(1.0D0) 		!The number Pi (3.14159265358979)
   REAL, PARAMETER :: kB = 8.6173324D-5 		!Boltzmann constant in eV/K

END MODULE constants
