MODULE shared_data
!
!  Purpose:
!    To declare data to share between routines
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    02/12/15     S. Barthel	Original code
!

!PROCAR
   USE type_file_poscar
   USE type_file_coords
   USE type_file_procar
   USE type_file_subspace_procar

!LOCPROJ
   USE type_file_locproj
   USE type_file_subspace_locproj

!OUTCAR
   USE type_file_outcar

! general
   USE type_locproj

   IMPLICIT NONE
   SAVE

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units

! PROCAR
   TYPE( file_poscar ) :: poscar_01
   TYPE( file_coords ) :: coords_01
   TYPE( file_procar ) :: procar_01
   TYPE( file_subspace_procar ) :: subspace_procar_01, subspace_procar_02

! LOCPROJ
   TYPE( file_locproj ) :: locproj_01
   TYPE( file_subspace_locproj ) :: subspace_locproj_01

! PROCAR
   TYPE( file_outcar ) :: outcar_01

! general
   TYPE( class_locproj ) :: proj_01


   LOGICAL :: use_procar
   INTEGER :: vasp_version

END MODULE shared_data
