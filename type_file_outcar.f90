MODULE type_file_outcar
!
!  Purpose:
!    Import external input
!
!  Record of revisions:
!      Date       Programmer	Description of change
!      ====       ==========	=====================
!    27/01/16     S. Barthel	Original code
!

   USE parallel
   USE fileio

   IMPLICIT NONE

! Data dictionary: declare constants

! Data dictionary: declare variable types, definitions, units

!===================================================================================================================================

   TYPE :: file_outcar !data container for OUTCAR

      !PRIVATE                                                 !no access granted to member variables

      INTEGER :: n_kpoints_ibz
      INTEGER :: n_kpoints_ibz_hf

      REAL, ALLOCATABLE, DIMENSION(:,:) :: k_points_ibz		!Coordinates of irreducible k-points k(xyz,k) in units of VASP k-mesh
      REAL, ALLOCATABLE, DIMENSION(:) :: k_weight_ibz		!weight of irreducible k-points (multiplicity)
      INTEGER, ALLOCATABLE, DIMENSION(:) :: idx_ibz_hf		!indices corresponding to 1. BZ

      REAL, ALLOCATABLE, DIMENSION(:,:) :: k_points_ibz_hf      	!Coordinates of k-points k(xyz,k) in units of VASP k-mesh
      REAL, ALLOCATABLE, DIMENSION(:) :: k_weight_ibz_hf        	!weight of k-points

      REAL :: tol_eql = 1.0D-13

   END TYPE file_OUTCAR

!===================================================================================================================================

CONTAINS

   SUBROUTINE file_outcar_READ( outcar )
      !
      !  Purpose:
      !    Load an OUTCAR file
      !
      !  Record of revisions:
      !      Date       Programmer	Description of change
      !      ====       ==========	=====================
      !    27/01/16     S. Barthel	Original code
      !

      USE constants

      IMPLICIT NONE

      ! Data dictionary: declare constants

      ! Data dictionary: declare variable types, definitions, units

      TYPE( file_outcar ), INTENT(INOUT) :: outcar

      INTEGER :: ioerror, allocerror          !error codes for allocation and file IO
      INTEGER :: kpoint, kpoint_              !loop index

      CHARACTER(44) :: str_IBZKPT
      CHARACTER(47) :: str_IBZKPT_HF

      !-------------------------------------------------------------------------------------------------------------------------------

      IF ( rank_0 ) THEN

         WRITE(6,"(3X,'rank 0: READING data of OUTCAR ...')")

         !check if PROCAR exists
         OPEN (UNIT=26,FILE='OUTCAR',STATUS='OLD', ACTION='READ', IOSTAT=ioerror)

         IF ( ioerror /= 0 ) THEN
            WRITE(6,*) 'ERROR: OUTCAR file missing'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         !scan for " Subroutine IBZKPT returns following result:"
         DO
            READ(26,'(A)',IOSTAT=ioerror) str_IBZKPT

            !read until error or the string is found
            IF ( ioerror /= 0) THEN
               CLOSE(26)
               WRITE(6,*) 'ERROR: OUTCAR file cannot be read'
               CALL MPI_Abort(MPI_COMM_WORLD, i_error)
               STOP
            ENDIF

            IF ( INDEX(str_IBZKPT,' Subroutine IBZKPT returns following result:') == 1 ) THEN
               EXIT
            ENDIF

         ENDDO

         !lets read some k-points
         READ(26,*)		!skip one line
         READ(26,*)		!skip one line

         !try reading number of irreducible k-points
         READ(26,'(6X)',ADVANCE='NO')	!skip some chars
         READ(26,*,IOSTAT=ioerror) outcar%n_kpoints_ibz

         IF ( ioerror /= 0) THEN
            CLOSE(26)
            WRITE(6,*) 'ERROR: "outcar%n_kpoints_ibz" cannot be read'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         WRITE(6,*) '  rank 0: Found irreducible k-points ', outcar%n_kpoints_ibz

      ENDIF

      !broadcast data
      srcnt = 1
      CALL MPI_BCAST(outcar%n_kpoints_ibz,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      !allocate
      ALLOCATE( outcar%k_points_ibz( 3, outcar%n_kpoints_ibz ), outcar%k_weight_ibz( outcar%n_kpoints_ibz ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "outcar%k_points_ibz" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         outcar%k_points_ibz = 0.
         outcar%k_weight_ibz = 0.
      ENDIF

      !read on rank 0
      IF ( rank_0 ) THEN

         READ(26,*)		!skip one line
         READ(26,*)		!skip one line
         READ(26,*)		!skip one line

         DO kpoint=1,outcar%n_kpoints_ibz
            READ(26,*,IOSTAT=ioerror) outcar%k_points_ibz(:,kpoint), outcar%k_weight_ibz(kpoint)
            IF ( ioerror /= 0) THEN
               CLOSE(26)
               WRITE(6,*) 'ERROR: "outcar%k_points_ibz, outcar%k_weight_ibz" cannot be read'
               CALL MPI_Abort(MPI_COMM_WORLD, i_error)
               STOP
            ENDIF
         ENDDO

         WRITE(6,*) '  rank 0: irreducible k-points read'

      ENDIF

      !broadcast data
      srcnt = 3*outcar%n_kpoints_ibz
      CALL MPI_BCAST(outcar%k_points_ibz,  srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      srcnt = outcar%n_kpoints_ibz
      CALL MPI_BCAST(outcar%k_weight_ibz,  srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      IF ( rank_0 ) THEN

         !scan for " Subroutine IBZKPT returns following result:"
         DO
            READ(26,'(A)',IOSTAT=ioerror) str_IBZKPT_HF
            !read until error or the string is found
            IF ( ioerror /= 0) THEN
               CLOSE(26)
               WRITE(6,*) 'ERROR: OUTCAR file cannot be read'
               CALL MPI_Abort(MPI_COMM_WORLD, i_error)
               STOP
            ENDIF

            IF ( INDEX(str_IBZKPT_HF,' Subroutine IBZKPT_HF returns following result:') == 1 ) THEN
               EXIT
            ENDIF

         ENDDO

         !lets read some k-points
         READ(26,*)		!skip one line
         READ(26,*)		!skip one line

         !try reading number of irreducible k-points
         READ(26,'(6X)',ADVANCE='NO')	!skip some chars
         READ(26,*,IOSTAT=ioerror) outcar%n_kpoints_ibz_hf
         IF ( ioerror /= 0) THEN
            CLOSE(26)
            WRITE(6,*) 'ERROR: "outcar%n_kpoints_ibz_hf" cannot be read'
            CALL MPI_Abort(MPI_COMM_WORLD, i_error)
            STOP
         ENDIF

         WRITE(6,*) '  rank 0: Found k-points in 1st BZ ', outcar%n_kpoints_ibz_hf

      ENDIF

      !broadcast data
      srcnt = 1
      CALL MPI_BCAST(outcar%n_kpoints_ibz_hf,  srcnt, MPI_INTEGER, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      !allocate
      ALLOCATE( outcar%k_points_ibz_hf( 3, outcar%n_kpoints_ibz_hf ),&
       outcar%k_weight_ibz_hf( outcar%n_kpoints_ibz_hf ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "outcar%k_points_ibz" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         outcar%k_points_ibz_hf = 0.
         outcar%k_weight_ibz_hf = 0.
      ENDIF

      !read on rank 0
      IF ( rank_0 ) THEN

         READ(26,*)		!skip one line
         READ(26,*)		!skip one line
9004     FORMAT(1X,1F9.6,1X,1F9.6,1X,1F9.6,4X,1F10.8)
         DO kpoint=1,outcar%n_kpoints_ibz_hf
            READ(26,9004,IOSTAT=ioerror) outcar%k_points_ibz_hf(:,kpoint), outcar%k_weight_ibz_hf(kpoint)
            IF ( ioerror /= 0) THEN
               CLOSE(26)
               WRITE(6,*) 'ERROR: "outcar%k_points_ibz_hf, outcar%k_weight_ibz_hf" cannot be read. proably an error with fixed'
               WRITE(6,*) ' format... change back to free format for small number of k-points (nk<10000)'
               CALL MPI_Abort(MPI_COMM_WORLD, i_error)
               STOP
            ENDIF
         ENDDO

         outcar%k_weight_ibz_hf = 1.0

         WRITE(6,*) '  rank 0: k-points in 1st BZ read, weights set to 1.0'

         CLOSE(26)

      ENDIF

      !broadcast data
      srcnt = 3*outcar%n_kpoints_ibz_hf
      CALL MPI_BCAST(outcar%k_points_ibz_hf,  srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      srcnt = outcar%n_kpoints_ibz_hf
      CALL MPI_BCAST(outcar%k_weight_ibz_hf,  srcnt, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, i_error)
      CALL check_mpi_success

      !allocate
      ALLOCATE( outcar%idx_ibz_hf( outcar%n_kpoints_ibz ), STAT=allocerror )
      IF ( allocerror > 0 ) THEN
         WRITE(6,*) 'ERROR: Memory for "idx_ibz_hf" could not be allocated'
         CALL MPI_Abort(MPI_COMM_WORLD, i_error)
         STOP
      ELSE
         outcar%idx_ibz_hf = 0

         !index map (apparently the first outcar%n_kpoints_ibz of LOCPROJ are the ones we are looking for already ...)
         DO kpoint=1,outcar%n_kpoints_ibz		!irr. BZ, i.e. GAMMA and PROCAR
            DO kpoint_=1,outcar%n_kpoints_ibz_hf	!1. BZ, i.e. LOCPROJ
               IF ( ALL( ABS( outcar%k_points_ibz(:,kpoint)-outcar%k_points_ibz_hf(:,kpoint_) ) < outcar%tol_eql )  ) THEN
                  outcar%idx_ibz_hf(kpoint) = kpoint_

! 	    IF ( rank_0 ) THEN
! 	      WRITE(6,*) outcar%k_points_ibz(:,kpoint)
! 	      WRITE(6,*) outcar%k_points_ibz_hf(:,kpoint_)
! 	      WRITE(6,*) kpoint, outcar%idx_ibz_hf(kpoint)
! 	    ENDIF

                  !just a check to be sure ...
                  IF ( kpoint /= kpoint_ ) THEN
                     WRITE(6,*) 'ERROR: irreducible k-points are NOT stored in the beginning'
                     CALL MPI_Abort(MPI_COMM_WORLD, i_error)
                     STOP
                  ENDIF

               ENDIF
            ENDDO
         ENDDO

      ENDIF

!      IF ( rank_0 ) THEN
!
!         OPEN (UNIT=26,FILE='./_KPOINTS.outcar',STATUS='REPLACE', ACTION='WRITE', IOSTAT=ioerror)
!         WRITE(26,*) ' index of k-points in irreducible BZ, corresponding index in full BZ.'
!         DO kpoint=1,outcar%n_kpoints_ibz
!            WRITE(26,*) kpoint, outcar%idx_ibz_hf(kpoint)
!         ENDDO
!         CLOSE(26)
!
!         WRITE(6,*) '-'
!      ENDIF

   END SUBROUTINE file_outcar_READ

   !=================================================================================================================================

END MODULE type_file_outcar
